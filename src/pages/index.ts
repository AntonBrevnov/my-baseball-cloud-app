import ForgotPasswordPage from './ForgotPasswordPage';
import SignInPage from './SignInPage';
import SignUpPage from './SignUpPage';
import ProfilePage from './ProfilePage';
import LeaderboardPage from './LeaderboardPage';
import NetworkPage from './NetworkPage';
import OtherProfilePage from './OtherProfilePage';

export const UnauthorizedPages = [
  {
    path: '/login',
    component: SignInPage,
  },
  {
    path: '/registration',
    component: SignUpPage,
  },
  {
    path: '/password',
    component: ForgotPasswordPage,
  },
];

export const AuthorizedPages = [
  {
    path: '/profile',
    component: ProfilePage,
  },
  {
    path: '/otherProfile',
    component: OtherProfilePage,
  },
  {
    path: '/leaderboard',
    component: LeaderboardPage,
  },
  {
    path: '/network',
    component: NetworkPage,
  },
];
