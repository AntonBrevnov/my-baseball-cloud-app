import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Pagination, Toast, LoaderSpinner } from '../../components';
import { NetworkHeader, TableFilters, Table } from './components';
import { fetchNetworkUsersAction } from '../../store/network/actions';
import {
  selectLoadNetworkUsersStatus,
  selectTotalCount,
} from '../../store/network/slice';
import { INetworkFilters } from '../../store/network/types';
import { selectClientData } from '../../store/user/slice';
import {
  selectToastData,
  selectToastVisibleState,
} from '../../store/toast/slice';

const NetworkPage = () => {
  const toastData = useSelector(selectToastData);
  const toastIsVisible = useSelector(selectToastVisibleState);
  const totalProfilesCount = useSelector(selectTotalCount);
  const networkUsersIsLoading = useSelector(selectLoadNetworkUsersStatus);
  const clientData = useSelector(selectClientData);
  const [firstFilters, setFirstFilters] = useState<INetworkFilters>({
    school: '',
    team: '',
    age: '',
    position: '',
    isFavorite: 'All',
    profilesCount: 10,
  });
  const [playerNameValue, setPlayerNameValue] = useState('');
  const [currentPageIndex, setCurrentPageIndex] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      fetchNetworkUsersAction({
        ...clientData,
        ...firstFilters,
        isFavorite: firstFilters.isFavorite === 'Favorite',
        age: Number(firstFilters.age),
        playerName: playerNameValue,
        offset: currentPageIndex * firstFilters.profilesCount,
      }),
    );
  }, [firstFilters, playerNameValue, currentPageIndex]);

  return (
    <PageContainer>
      <InsideContainer>
        <NetworkHeader onChangeFilters={filters => setFirstFilters(filters)} />
        <TableFilters onChangePlayerName={value => setPlayerNameValue(value)} />
        {networkUsersIsLoading ? <LoaderSpinner /> : <Table />}
        <Pagination
          isFixed
          currentPageIndex={currentPageIndex}
          maxPagesCount={Math.round(
            totalProfilesCount / firstFilters.profilesCount,
          )}
          onChangePageIndex={index => setCurrentPageIndex(index)}
        />
      </InsideContainer>
      {toastIsVisible && <Toast {...toastData} />}
    </PageContainer>
  );
};

const PageContainer = styled.div`
  grid-area: content;
  background: #fff;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
`;
const InsideContainer = styled.div`
  width: 100%;
  padding: 0;
  overflow: auto;
  margin-right: auto;
  margin-left: auto;
`;

export default NetworkPage;
