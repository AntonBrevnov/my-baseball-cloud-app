import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { updateFavoriteStatusAction } from '../../../store/network/actions';
import { selectNetworkUsers } from '../../../store/network/slice';
import { setSelectedOtherUserId } from '../../../store/otherUser/slice';
import { selectClientData } from '../../../store/user/slice';
import { namesStringGenerator } from '../../../utils/namesStringGenerator';

const Table = () => {
  const clientData = useSelector(selectClientData);
  const networkUser = useSelector(selectNetworkUsers);
  const dispatch = useDispatch();

  const networkUserTableItemsList = networkUser.map(profile => (
    <TableRow key={Math.round(Math.random() * 10000)}>
      <TableCellStyle1>
        <ProfileLink
          href="/otherProfile"
          onClick={() => dispatch(setSelectedOtherUserId(profile.id))}>
          {profile.name}
        </ProfileLink>
      </TableCellStyle1>
      <TableCellStyle2>-</TableCellStyle2>
      <TableCellStyle3>{profile.school.name || '-'}</TableCellStyle3>
      <TableCellStyle3>
        {profile.teams.length && namesStringGenerator(profile.teams)}
      </TableCellStyle3>
      <TableCellStyle4>{profile.age}</TableCellStyle4>
      <TableCellStyle5>
        <HeartButton
          onClick={() =>
            dispatch(
              updateFavoriteStatusAction({
                ...clientData,
                id: profile.id,
                isFavorite: !profile.isFavorite,
              }),
            )
          }>
          {profile.isFavorite ? (
            <Heart className="blue-icon fa fa-heart" />
          ) : (
            <Heart className="blur-blue-icon fa fa-heart" />
          )}
        </HeartButton>
      </TableCellStyle5>
    </TableRow>
  ));

  return (
    <TableWrapper>
      <TableHeader>
        <TableCellStyle1>Player Name</TableCellStyle1>
        <TableCellStyle2>Sessions</TableCellStyle2>
        <TableCellStyle3>School</TableCellStyle3>
        <TableCellStyle3>Teams</TableCellStyle3>
        <TableCellStyle4>Age</TableCellStyle4>
        <TableCellStyle5>Favorite</TableCellStyle5>
      </TableHeader>
      <TableContent>{networkUserTableItemsList}</TableContent>
    </TableWrapper>
  );
};

const TableWrapper = styled.div`
  padding: 16px;
`;
const TableHeader = styled.div`
  position: sticky;
  top: 0;
  min-height: 44px;
  display: flex;
  align-items: center;
  color: rgb(102, 119, 132);
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  font-weight: 300;
  margin-bottom: 6px;
  background-color: #fff;
`;
const TableContent = styled.div`
  min-height: 436px;
  background: #ffffff;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
  border-radius: 4px;
`;
const TableRow = styled.div`
  margin-bottom: 4px;
  min-height: 44px;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  background-color: #f7f8f9;
  :hover {
    background-color: #ecf8ff;
  }
`;
const ProfileLink = styled.a`
  padding-left: 6px;
  text-decoration: none;
  color: #414f5a;
  :hover {
    color: #48bbff;
    text-decoration: underline;
  }
`;
const TableCellStyle1 = styled.div`
  width: 19.5%;
  flex: 1 0 19.5%;
  min-width: 0;
`;
const TableCellStyle2 = styled.div`
  width: 10%;
  flex: 1 0 10%;
  min-width: 0;
`;
const TableCellStyle3 = styled.div`
  width: 23%;
  flex: 1 0 23%;
  min-width: 0;
`;
const TableCellStyle4 = styled.div`
  width: 15%;
  flex: 1 0 15%;
  min-width: 0;
`;
const TableCellStyle5 = styled.div`
  width: 8%;
  flex: 1 0 8%;
  min-width: 0;
`;
const HeartButton = styled.div`
  cursor: pointer;
  background-color: transparent;
  border-color: transparent;
  outline: none;
`;
const Heart = styled.i`
  cursor: pointer;
`;

export default Table;
