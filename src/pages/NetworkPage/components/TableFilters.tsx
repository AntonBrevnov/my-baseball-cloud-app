import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { selectTotalCount } from '../../../store/network/slice';
import { SearchInput } from '../../../components';

type FiltersProps = {
  onChangePlayerName: (value: string) => void;
};

const TablesFilter = ({ onChangePlayerName }: FiltersProps) => {
  const totalProfilesCount = useSelector(selectTotalCount);
  return (
    <MainContainer>
      <LeftText>Available Players ({totalProfilesCount})</LeftText>
      <SearchInput
        width="103px"
        placeholder="Player Name"
        onChange={value => onChangePlayerName(value)}
      />
    </MainContainer>
  );
};

const MainContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 16px;
`;
const LeftText = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  color: #414f5a;
  font-weight: 400;
  text-align: left;
`;

export default TablesFilter;
