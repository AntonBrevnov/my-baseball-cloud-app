import { Field, Form } from 'react-final-form';
import styled from 'styled-components';
import { AnimatableInput, BlueComboBox } from '../../../components';
import { ILeaderboardFirstFilters } from '../../../store/leaders/types';
import dateConverter from '../../../utils/dateConverter';
import { userPositionConverter } from '../../../utils/userPositionConverter';

type HeaderProps = {
  onChangeFilters: (filters: ILeaderboardFirstFilters) => void;
};

const Header = ({ onChangeFilters }: HeaderProps) => {
  const handleChangeFilters = (filters: ILeaderboardFirstFilters) => {
    onChangeFilters(filters);
  };

  return (
    <LeaderboardHeader>
      <Title>Leaderboard</Title>
      <Form
        onSubmit={handleChangeFilters}
        render={({ handleSubmit }) => (
          <FiltersContainer>
            <Field name="date">
              {props => (
                <BlueComboBox
                  title={value =>
                    `Date ${
                      dateConverter(value) === 'All'
                        ? ''
                        : `(${dateConverter(value)})`
                    }`
                  }
                  initialItemIndex={0}
                  itemsList={['', 'last_week', 'last_month']}
                  onChangeItem={() => handleSubmit()}
                  dataConverter={value => dateConverter(value)}
                  renderProps={props}
                />
              )}
            </Field>
            <FilterWrapper>
              <Field name="school">
                {props => (
                  <AnimatableInput
                    minWidth={70}
                    maxWidth={180}
                    placeHolder="School"
                    submitForm={handleSubmit}
                    renderProps={props}
                  />
                )}
              </Field>
            </FilterWrapper>
            <FilterWrapper>
              <Field name="team">
                {props => (
                  <AnimatableInput
                    minWidth={70}
                    maxWidth={180}
                    placeHolder="Team"
                    submitForm={handleSubmit}
                    renderProps={props}
                  />
                )}
              </Field>
            </FilterWrapper>
            <FilterWrapper>
              <Field name="position">
                {props => (
                  <BlueComboBox
                    title={value =>
                      `Position ${
                        userPositionConverter(value) === 'All'
                          ? ''
                          : `(${userPositionConverter(value)})`
                      }`
                    }
                    initialItemIndex={0}
                    itemsList={[
                      '',
                      'catcher',
                      'first_base',
                      'second_base',
                      'shortstop',
                      'third_base',
                      'outfield',
                      'pitcher',
                    ]}
                    onChangeItem={() => handleSubmit()}
                    dataConverter={value => userPositionConverter(value)}
                    renderProps={props}
                  />
                )}
              </Field>
            </FilterWrapper>
            <FilterWrapper>
              <Field name="age">
                {props => (
                  <AnimatableInput
                    minWidth={50}
                    maxWidth={80}
                    placeHolder="Age"
                    submitForm={handleSubmit}
                    renderProps={props}
                  />
                )}
              </Field>
            </FilterWrapper>
            <FilterWrapper>
              <Field name="isFavorite">
                {props => (
                  <BlueComboBox
                    title={value => value}
                    initialItemIndex={0}
                    itemsList={['All', 'Favorite']}
                    onChangeItem={() => handleSubmit()}
                    dataConverter={value => value}
                    renderProps={props}
                  />
                )}
              </Field>
            </FilterWrapper>
          </FiltersContainer>
        )}
      />
    </LeaderboardHeader>
  );
};

const LeaderboardHeader = styled.div`
  display: flex;
  padding: 16px;
  justify-content: space-between;
`;
const Title = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 24px;
  line-height: 1.25;
  font-weight: 400;
  -webkit-text-align: center;
  text-align: center;
  color: #667784;
`;
const FiltersContainer = styled.form`
  display: flex;
  margin-right: 40px;
`;
const FilterWrapper = styled.div`
  margin-left: 15px;
`;

export default Header;
