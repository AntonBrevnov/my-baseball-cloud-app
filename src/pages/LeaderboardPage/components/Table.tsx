import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { updateFavoriteStatusAction } from '../../../store/leaders/actions';
import {
  selectBattingLeaders,
  selectPitchingLeaders,
} from '../../../store/leaders/slice';
import { setSelectedOtherUserId } from '../../../store/otherUser/slice';
import { selectClientData } from '../../../store/user/slice';
import { namesStringGenerator } from '../../../utils/namesStringGenerator';

type TableProps = {
  type: string;
};

const Table = ({ type }: TableProps) => {
  const clientData = useSelector(selectClientData);
  const battingLeadersList = useSelector(selectBattingLeaders);
  const pitchingLeadersList = useSelector(selectPitchingLeaders);
  const dispatch = useDispatch();

  const battingLeaderboardItemsList = battingLeadersList.map(
    (profile, index) => (
      <TableRow key={Math.round(Math.random() * 10000)}>
        <FirstTypeCell className="leaderboard-table__column-item">
          {index + 1}
        </FirstTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          <ProfileLink
            href="/otherProfile"
            onClick={() => dispatch(setSelectedOtherUserId(profile.id))}>
            {profile.name}
          </ProfileLink>
        </SecondTypeCell>
        <ThirdTypeCell className="leaderboard-table__column-item">
          {profile.age}
        </ThirdTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.school.name}
        </SecondTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.teams.length && namesStringGenerator(profile.teams)}
        </SecondTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.exitVelocity || '-'}
        </SecondTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.launchAngle || '-'}
        </SecondTypeCell>
        <FourthTypeCell className="leaderboard-table__column-item">
          {profile.distance || '-'}
        </FourthTypeCell>
        <FifthTypeCell className="leaderboard-table__column-item">
          <HeartButton
            onClick={() =>
              dispatch(
                updateFavoriteStatusAction({
                  ...clientData,
                  id: profile.id,
                  isFavorite: !profile.favorite,
                  playerType: type,
                }),
              )
            }>
            {profile.favorite ? (
              <Heart className="blue-icon fa fa-heart" />
            ) : (
              <Heart className="blur-blue-icon fa fa-heart" />
            )}
          </HeartButton>
        </FifthTypeCell>
      </TableRow>
    ),
  );

  const pitchingLeaderboardItemsList = pitchingLeadersList.map(
    (profile, index) => (
      <TableRow key={Math.round(Math.random() * 10000)}>
        <FirstTypeCell className="leaderboard-table__column-item">
          {index + 1}
        </FirstTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          <ProfileLink
            href="/otherProfile"
            onClick={() => dispatch(setSelectedOtherUserId(profile.id))}>
            {profile.name}
          </ProfileLink>
        </SecondTypeCell>
        <ThirdTypeCell className="leaderboard-table__column-item">
          {profile.age}
        </ThirdTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.school.name}
        </SecondTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.teams.length && namesStringGenerator(profile.teams)}
        </SecondTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.pitchType || '-'}
        </SecondTypeCell>
        <SecondTypeCell className="leaderboard-table__column-item">
          {profile.velocity || '-'}
        </SecondTypeCell>
        <FourthTypeCell className="leaderboard-table__column-item">
          {profile.spinRate || '-'}
        </FourthTypeCell>
        <FifthTypeCell className="leaderboard-table__column-item">
          <HeartButton
            onClick={() =>
              dispatch(
                updateFavoriteStatusAction({
                  ...clientData,
                  id: profile.id,
                  isFavorite: !profile.favorite,
                  playerType: type,
                }),
              )
            }>
            {profile.favorite ? (
              <Heart className="blue-icon fa fa-heart" />
            ) : (
              <Heart className="blur-blue-icon fa fa-heart" />
            )}
          </HeartButton>
        </FifthTypeCell>
      </TableRow>
    ),
  );

  return (
    <TableWrapper>
      {type === 'batting' && (
        <>
          <TableHeader>
            <FirstTypeCell className="leaderboard-table__column-item">
              Rank
            </FirstTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Better Name
            </SecondTypeCell>
            <ThirdTypeCell className="leaderboard-table__column-item">
              Age
            </ThirdTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              School
            </SecondTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Teams
            </SecondTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Exit Velocity
            </SecondTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Launch Angle
            </SecondTypeCell>
            <FourthTypeCell className="leaderboard-table__column-item">
              Distance
            </FourthTypeCell>
            <FifthTypeCell className="leaderboard-table__column-item">
              Favorite
            </FifthTypeCell>
          </TableHeader>
          {battingLeaderboardItemsList.length ? (
            battingLeaderboardItemsList
          ) : (
            <ErrorText>There's no info yet!</ErrorText>
          )}
        </>
      )}
      {type === 'pitching' && (
        <>
          <TableHeader>
            <FirstTypeCell className="leaderboard-table__column-item">
              Rank
            </FirstTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Better Name
            </SecondTypeCell>
            <ThirdTypeCell className="leaderboard-table__column-item">
              Age
            </ThirdTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              School
            </SecondTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Teams
            </SecondTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Pitch Type
            </SecondTypeCell>
            <SecondTypeCell className="leaderboard-table__column-item">
              Velocity
            </SecondTypeCell>
            <FourthTypeCell className="leaderboard-table__column-item">
              Spin Rate
            </FourthTypeCell>
            <FifthTypeCell className="leaderboard-table__column-item">
              Favorite
            </FifthTypeCell>
          </TableHeader>
          {pitchingLeaderboardItemsList.length ? (
            pitchingLeaderboardItemsList
          ) : (
            <ErrorText>There's no info yet!</ErrorText>
          )}
        </>
      )}
    </TableWrapper>
  );
};

const TableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 21px;
  overflow-x: hidden;
  padding: 16px;
`;
const TableHeader = styled.div`
  min-height: 44px;
  margin-bottom: 6px;
  display: flex;
  width: 100%;
  max-width: 100%;
  padding-right: 0;
  align-items: center;
  background-color: #fff;
`;
const TableRow = styled.div`
  margin-bottom: 4px;
  min-height: 44px;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  background-color: #f7f8f9;
  :hover {
    background-color: #ecf8ff;
  }
`;
const FirstTypeCell = styled.div`
  width: 6%;
  flex: 1 0 6%;
  min-width: 0;
`;
const SecondTypeCell = styled.div`
  width: 14%;
  flex: 1 0 14%;
  min-width: 0;
`;
const ThirdTypeCell = styled.div`
  width: 5%;
  flex: 1 0 5%;
  min-width: 0;
`;
const FourthTypeCell = styled.div`
  width: 10%;
  flex: 1 0 10%;
  min-width: 0;
`;
const FifthTypeCell = styled.div`
  width: 6%;
  flex: 1 0 6%;
  min-width: 0;
`;
const ProfileLink = styled.a`
  text-decoration: none;
  color: #414f5a;
  :hover {
    color: #48bbff;
    text-decoration: underline;
  }
`;
const HeartButton = styled.div`
  cursor: pointer;
  background-color: transparent;
  border-color: transparent;
  outline: none;
`;
const Heart = styled.i`
  cursor: pointer;
`;
const ErrorText = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;

export default Table;
