import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';

type TabsContainerProps = {
  currentTab: string;
  onChangeTab: (tab: string) => void;
  renderProps: FieldRenderProps<string>;
};

const TabsContainer = ({
  currentTab,
  onChangeTab,
  renderProps,
}: TabsContainerProps) => (
  <Container>
    <input {...renderProps.input} type="hidden" />
    <Tab
      isSelected={currentTab === 'batting'}
      onClick={() => {
        onChangeTab('batting');
        renderProps.input.onChange('batting');
      }}>
      Batting
    </Tab>
    <Tab
      isSelected={currentTab === 'pitching'}
      onClick={() => {
        onChangeTab('pitching');
        renderProps.input.onChange('pitching');
      }}>
      Pitching
    </Tab>
  </Container>
);

const Container = styled.ul`
  width: 100%;
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: flex-start;
`;
const Tab = styled.li<{ isSelected: boolean }>`
  padding: 8px;
  margin: 8px;
  border: 2px solid #788b99;
  border-radius: 40px;
  outline: none;
  position: relative;
  list-style: none;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 17px;
  font-weight: 700;
  color: ${props => (props.isSelected ? '#fff' : '#667784')};
  background-color: ${props => (props.isSelected ? '#788b99' : '#fff')};
  cursor: pointer;
`;

export default TabsContainer;
