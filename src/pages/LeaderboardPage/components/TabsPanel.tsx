import { useState } from 'react';
import { Field, Form } from 'react-final-form';
import styled from 'styled-components';
import { BlueComboBox } from '../../../components';
import { ILeaderboardSecondFilters } from '../../../store/leaders/types';
import battingTypeConverter from '../../../utils/battingTypeConverter';
import pitchingTypeConverter from '../../../utils/pitchingTypeConverter';
import TabsContainer from './TabsContainer';

type TabsPanelProps = {
  onChangeFilters: (fitlers: ILeaderboardSecondFilters) => void;
};

const TabsPanel = ({ onChangeFilters }: TabsPanelProps) => {
  const [currentTab, setCurrentTab] = useState('batting');

  const handleChangeFilters = (filters: ILeaderboardSecondFilters) => {
    onChangeFilters(filters);
  };

  return (
    <Form
      onSubmit={handleChangeFilters}
      render={({ handleSubmit }) => (
        <form>
          <Field name="leadersType">
            {props => (
              <TabsContainer
                currentTab={currentTab}
                onChangeTab={tab => {
                  setCurrentTab(tab);
                  handleSubmit();
                }}
                renderProps={props}
              />
            )}
          </Field>
          {currentTab === 'batting' && (
            <ComboBoxPanel>
              <ComboBoxWrapper>
                <Field name="sortFilter" initialValue="exit_velocity">
                  {props => (
                    <BlueComboBox
                      title={value => battingTypeConverter(value) || ''}
                      initialItemIndex={0}
                      itemsList={['exit_velocity', 'carry_distance']}
                      onChangeItem={() => handleSubmit()}
                      renderProps={props}
                      dataConverter={value => battingTypeConverter(value)}
                    />
                  )}
                </Field>
              </ComboBoxWrapper>
            </ComboBoxPanel>
          )}
          {currentTab === 'pitching' && (
            <ComboBoxPanel>
              <ComboBoxWrapper>
                <Field name="sortFilter">
                  {props => (
                    <BlueComboBox
                      title={value => pitchingTypeConverter(value)}
                      initialItemIndex={0}
                      itemsList={['pitch_velocity', 'spin_rate']}
                      onChangeItem={() => handleSubmit()}
                      dataConverter={value => pitchingTypeConverter(value)}
                      renderProps={props}
                    />
                  )}
                </Field>
              </ComboBoxWrapper>
            </ComboBoxPanel>
          )}
        </form>
      )}
    />
  );
};

const ComboBoxPanel = styled.div`
  position: relative;
  display: flex;
  margin-bottom: 23px;
`;
const ComboBoxWrapper = styled.div`
  position: absolute;
  right: 46px;
  top: -45px;
`;

export default TabsPanel;
