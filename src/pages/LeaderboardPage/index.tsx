import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { LoaderSpinner, Toast } from '../../components';
import {
  LeaderboardHeader,
  LeaderboardTable,
  LeaderboardTabsPanel,
} from './components';
import {
  selectToastData,
  selectToastVisibleState,
} from '../../store/toast/slice';
import {
  fetchAllBattingLeadersAction,
  fetchAllPitchingLeadersAction,
} from '../../store/leaders/actions';
import { selectLoadLeadersStatus } from '../../store/leaders/slice';
import {
  ILeaderboardFirstFilters,
  ILeaderboardSecondFilters,
} from '../../store/leaders/types';
import { selectClientData } from '../../store/user/slice';

const LeaderboardPage = () => {
  const toastData = useSelector(selectToastData);
  const toastIsVisible = useSelector(selectToastVisibleState);
  const leaderboardIsLoading = useSelector(selectLoadLeadersStatus);
  const clientData = useSelector(selectClientData);

  const [firstFilters, setFirstFilters] = useState<ILeaderboardFirstFilters>({
    age: 0,
    date: '',
    isFavorite: 'All',
    position: '',
    school: '',
    team: '',
  });
  const [secondFilters, setSecondFilters] = useState<ILeaderboardSecondFilters>(
    {
      leadersType: 'batting',
      sortFilter: 'exit_velocity',
    },
  );
  const dispatch = useDispatch();

  useEffect(() => {
    // eslint-disable-next-line default-case
    switch (secondFilters.leadersType) {
      case 'batting':
        if (
          secondFilters.sortFilter === 'pitch_velocity' ||
          secondFilters.sortFilter === 'spin_rate'
        ) {
          dispatch(
            fetchAllBattingLeadersAction({
              ...clientData,
              ...firstFilters,
              isFavorite: Number(
                firstFilters.isFavorite === 'Favorite' ? 1 : 0,
              ),
              type: 'exit_velocity',
            }),
          );
        } else {
          dispatch(
            fetchAllBattingLeadersAction({
              ...clientData,
              ...firstFilters,
              isFavorite: Number(
                firstFilters.isFavorite === 'Favorite' ? 1 : 0,
              ),
              type: secondFilters.sortFilter,
            }),
          );
        }
        break;
      case 'pitching':
        if (
          secondFilters.sortFilter === 'exit_velocity' ||
          secondFilters.sortFilter === 'carry_distance'
        ) {
          dispatch(
            fetchAllPitchingLeadersAction({
              ...clientData,
              ...firstFilters,
              isFavorite: Number(
                firstFilters.isFavorite === 'Favorite' ? 1 : 0,
              ),
              type: 'pitch_velocity',
            }),
          );
        } else {
          dispatch(
            fetchAllPitchingLeadersAction({
              ...clientData,
              ...firstFilters,
              isFavorite: Number(
                firstFilters.isFavorite === 'Favorite' ? 1 : 0,
              ),
              type: secondFilters.sortFilter,
            }),
          );
        }
        break;
    }
  }, [firstFilters, secondFilters]);

  return (
    <PageContainer>
      <InsideContainer>
        <LeaderboardHeader
          onChangeFilters={filters => setFirstFilters(filters)}
        />
        <LeaderboardTabsPanel
          onChangeFilters={filters => setSecondFilters(filters)}
        />
        {leaderboardIsLoading ? (
          <LoaderSpinner />
        ) : (
          <LeaderboardTable type={secondFilters.leadersType} />
        )}
      </InsideContainer>
      {toastIsVisible && <Toast {...toastData} />}
    </PageContainer>
  );
};

const PageContainer = styled.div`
  grid-area: content;
  background: #fff;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
`;
const InsideContainer = styled.div`
  width: 100%;
  padding: 0;

  overflow: auto;
  margin-right: auto;
  margin-left: auto;
`;

export default LeaderboardPage;
