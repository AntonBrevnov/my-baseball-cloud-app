import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import {
  selectClientData,
  selectUserData,
  selectUserPitchingGraph,
} from '../../../../store/user/slice';
import { fetchUserPitchingGraphAction } from '../../../../store/user/actions';
import { BlueComboBox } from '../../../../components';

const PitchingChartsPanel = () => {
  const clientData = useSelector(selectClientData);
  const { id, firstName, lastName } = useSelector(selectUserData);
  const pitchingGraph = useSelector(selectUserPitchingGraph);
  const [pitchTypeValue, setPitchTypeValue] = useState('None');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      fetchUserPitchingGraphAction({
        ...clientData,
        profileId: id,
        pitchType: pitchTypeValue,
      }),
    );
  }, [pitchTypeValue]);

  const chartOptions: Highcharts.Options = {
    title: {
      text: `Rolling Velocity for ${firstName} ${lastName}`,
    },
    subtitle: {
      text: `Average over last last ${pitchingGraph.length} pitches`,
    },
    yAxis: {
      title: {
        text: 'Velocity',
      },
    },
    series: [
      {
        name: 'Velocity',
        type: 'spline',
        data: pitchingGraph,
      },
    ],
  };

  return (
    <div>
      <PanelHeader>
        <BlueComboBox
          title={value =>
            value === 'None' ? 'PitchType' : `PitchType (${value})`
          }
          initialItemIndex={0}
          itemsList={[
            'None',
            'Four Seam Fastball',
            'Two Seam Fastball',
            'Curveball',
            'Changeup',
            'Slider',
          ]}
          onChangeItem={(_, value) => setPitchTypeValue(value || 'None')}
          dataConverter={value => value}
        />
      </PanelHeader>
      {pitchingGraph.length ? (
        <HighchartsReact highcharts={Highcharts} options={chartOptions} />
      ) : (
        <ErrorText>There's no info yet!</ErrorText>
      )}
    </div>
  );
};

const PanelHeader = styled.div`
  display: flex;
  flex-direction: row-reverse;
`;
const ErrorText = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;

export default PitchingChartsPanel;
