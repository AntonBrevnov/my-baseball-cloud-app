import styled from 'styled-components';

const RecentSessionReports = () => (
    <Card>
      <CardTitle>Recent Session Reports</CardTitle>
      <DataTitle>No data currently linked to this profile</DataTitle>
    </Card>
  );

const Card = styled.div`
  background: #fff;
  margin: 16px;
  padding: 16px;
  border-radius: 8px;
  box-sizing: border-box;
  flex-grow: 1;
  display: flex;
  max-width: 100%;
  min-width: 0;
  flex-direction: column;
  overflow: hidden;
`;
const CardTitle = styled.div`
  font-family: 'Lato', sans-serif;
  font-weight: 900;
  font-size: 18px;
  line-height: 1.25;
  text-align: left;
  color: #414f5a;
`;
const DataTitle = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;

export default RecentSessionReports;
