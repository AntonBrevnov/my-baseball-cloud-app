import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { fetchUserPitchingLogsAction } from '../../../../store/user/actions';
import {
  selectClientData,
  selectLoadLogsStatus,
  selectUserData,
  selectUserPitchingLogs,
  selectUserPitchingLogsCount,
} from '../../../../store/user/slice';
import {
  SearchInput,
  BlueComboBox,
  LoaderSpinner,
  Pagination,
} from '../../../../components';
import PitchingLogsTableItem from './PitchingLogsTableItem';

const PitchingLogsPanel = () => {
  const { id } = useSelector(selectUserData);
  const clientData = useSelector(selectClientData);
  const logsIsLoading = useSelector(selectLoadLogsStatus);
  const pitchingLogs = useSelector(selectUserPitchingLogs);
  const pitchingLogsTotalCount = useSelector(selectUserPitchingLogsCount);
  const [currentPageIndex, setCurrentPageIndex] = useState(0);
  const [pitchTypeValue, setPitchTypeValue] = useState('None');
  const [batterNameInputValue, setBatterNameInputValue] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      fetchUserPitchingLogsAction({
        ...clientData,
        pitchType: pitchTypeValue,
        name: batterNameInputValue,
        count: 10,
        offset: currentPageIndex * 10,
        profileId: id,
      }),
    );
  }, [currentPageIndex, pitchTypeValue, batterNameInputValue]);

  const logsTableItemsList = pitchingLogs.map(log => (
    <PitchingLogsTableItem key={Math.round(Math.random() * 10000)} {...log} />
  ));

  return (
    <div>
      <PanelHeader>
        <SearchInput
          width="171px"
          placeholder="Search"
          onChange={value => setBatterNameInputValue(value)}
        />
        <BlueComboBox
          title={value =>
            value === 'None' ? 'PitchType' : `PitchType (${value})`
          }
          initialItemIndex={0}
          itemsList={[
            'None',
            'Four Seam Fastball',
            'Two Seam Fastball',
            'Curveball',
            'Changeup',
            'Slider',
          ]}
          onChangeItem={(_, value) => setPitchTypeValue(value || 'None')}
          dataConverter={value => value}
        />
      </PanelHeader>
      <PanelTitle>Pitching Logs</PanelTitle>
      {pitchingLogs.length ? (
        <>
          {logsIsLoading ? (
            <LoaderSpinner />
          ) : (
            <Table>
              <TableHeader>
                <TableCell1>Date</TableCell1>
                <TableCell2>Batter Name</TableCell2>
                <TableCell2>Pitch Type</TableCell2>
                <TableCell2>Pitch Call</TableCell2>
                <TableCell3>Velocity</TableCell3>
                <TableCell3>Spin Rate</TableCell3>
                <TableCell4>Spin Axis</TableCell4>
              </TableHeader>
              {logsTableItemsList}
            </Table>
          )}
          <Pagination
            isFixed={false}
            currentPageIndex={currentPageIndex}
            maxPagesCount={Math.round(pitchingLogsTotalCount / 10)}
            onChangePageIndex={index => setCurrentPageIndex(index)}
          />
        </>
      ) : (
        <ErrorText>There's no info yet!</ErrorText>
      )}
    </div>
  );
};

const PanelHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 23px;
`;
const ErrorText = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const PanelTitle = styled.div`
  font-family: 'Lato', sans-serif;
  line-height: 1.25;
  font-size: 18px;
  color: #414f5a;
  font-weight: 400;
  text-align: left;
`;
const Table = styled.div`
  display: flex;
  flex-direction: column;
`;
const TableHeader = styled.div`
  display: flex;
  position: sticky;
  top: 0;
  background: #fff;
  min-height: 44px;
  margin-bottom: 6px;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
`;
const TableCell1 = styled.div`
  display: flex;
  width: 12.1%;
  flex: 1 0 12.1%;
`;
const TableCell2 = styled.div`
  display: flex;
  width: 18.5%;
  flex: 1 0 18.5%;
`;
const TableCell3 = styled.div`
  display: flex;
  width: 11.4%;
  flex: 1 0 11.4%;
`;
const TableCell4 = styled.div`
  display: flex;
  width: 8%;
  flex: 1 0 8%;
`;

export default PitchingLogsPanel;
