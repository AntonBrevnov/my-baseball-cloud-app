import React, { forwardRef, useState } from 'react';
import styled from 'styled-components';
import DatePicker, { CalendarContainer } from 'react-datepicker';
import ComboBox from '../../../../components/BlueComboBox';
import CalendarIcon from '../../../../assets/svg/CalendarIcon';
import ComboBoxMark from '../../../../assets/svg/ComboBoxMark';

const SessionReportsPanel = () => {
  const [selectedDate, setSelectedDate] = useState(new Date());

  const DatePickerInput = forwardRef<
    HTMLButtonElement,
    React.HTMLProps<HTMLButtonElement>
  >(({ value, onClick }, ref) => {
    const [calendarIsOpen, setCalendarState] = useState(false);
    return (
      <InputWrapper
        onClick={e => {
          setCalendarState(!calendarIsOpen);
          if (onClick) {
            onClick(e);
          }
        }}
        ref={ref}>
        <CalendarIconSpan>
          <CalendarIcon />
        </CalendarIconSpan>
        Date ({value})
        <ComboBoxMark isOpened={calendarIsOpen} />
      </InputWrapper>
    );
  });

  return (
    <>
      <SessionPanelHeader>
        <SessionPanelTitle>Sessions</SessionPanelTitle>
        <SessionPanelHeaderRightSide>
          <ClearFiltersButton>Clear Filters</ClearFiltersButton>
          <DatePicker
            selected={selectedDate}
            onChange={(date: Date) => setSelectedDate(date)}
            calendarContainer={CalendarContainer}
            nextMonthButtonLabel=""
            previousMonthButtonLabel=""
            customInput={<DatePickerInput />}
          />
          <ComboBox
            title={value => `Type ${value === 'None' ? '' : `(${value})`}`}
            itemsList={['None', 'Game', 'Practice']}
            dataConverter={value => value}
          />
        </SessionPanelHeaderRightSide>
      </SessionPanelHeader>
      <SessionPanelTableHeader>
        <FirstColumnType>Date</FirstColumnType>
        <SecondColumnType>Type</SecondColumnType>
        <SecondColumnType>Name</SecondColumnType>
        <ThirdColumnType>Purchased</ThirdColumnType>
      </SessionPanelTableHeader>
      <ErrorText>The player haven't had any sessions yet!</ErrorText>
    </>
  );
};

const SessionPanelHeader = styled.div`
  display: flex;
  justify-content: space-between;
  max-height: 23px;
  flex: 0 0 100%;
  margin-bottom: 21px;
`;
const SessionPanelTitle = styled.div`
  line-height: 1.25;
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  color: #414f5a;
  font-weight: 400;
  text-align: left;
`;
const SessionPanelHeaderRightSide = styled.div`
  display: flex;
`;
const ClearFiltersButton = styled.button`
  align-items: flex-start;
  margin-right: 20px;
  padding: 0;
  font-family: 'Lato', sans-serif;
  background-color: transparent;
  border-style: none;
  font-size: 16px;
  font-weight: 400;
  line-height: 1.19;
  color: #48bbff;
  border-radius: 4px;
  box-shadow: none;
  cursor: pointer;
`;
const ErrorText = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const InputWrapper = styled.button`
  padding-right: 16px;
  outline: none;
  border: 0px solid transparent;
  background-color: #fff;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 1.19;
  color: #48bbff;
  white-space: nowrap;
  display: flex;
  align-items: flex-start;
  cursor: pointer;
`;
const CalendarIconSpan = styled.span`
  margin-right: 6px;
  margin-top: 2px;
`;
const SessionPanelTableHeader = styled.div`
  position: sticky;
  top: 0;
  background: #fff;
  min-height: 44px;
  margin-bottom: 6px;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
`;
const FirstColumnType = styled.div`
  width: 35%;
  flex: 1 0 32%;
  min-width: 0;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
`;
const SecondColumnType = styled.div`
  width: 30%;
  flex: 1 0 28%;
  min-width: 0;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
`;
const ThirdColumnType = styled.div`
  width: 10%;
  flex: 1 0 10%;
  min-width: 0;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
`;

export default SessionReportsPanel;
