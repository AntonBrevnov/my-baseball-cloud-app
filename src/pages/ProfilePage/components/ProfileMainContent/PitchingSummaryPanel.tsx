import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { selectUserPitchingSummary } from '../../../../store/user/slice';

const PitchingSummaryPanel = () => {
  const pitchingSummary = useSelector(selectUserPitchingSummary);

  const averageValuesList = pitchingSummary.averageValues.map(value => (
    <TableRow key={Math.round(Math.random() * 100)}>
      <TableCell1>{value.pitchType}</TableCell1>
      <TableCell2>{value.velocity}</TableCell2>
      <TableCell3>{value.spinRate}</TableCell3>
    </TableRow>
  ));

  const topValuesList = pitchingSummary.topValues.map(value => (
    <TableRow key={Math.round(Math.random() * 100)}>
      <TableCell1>{value.pitchType}</TableCell1>
      <TableCell2>{value.velocity}</TableCell2>
      <TableCell3>{value.spinRate}</TableCell3>
    </TableRow>
  ));

  return (
    <div>
      {!averageValuesList.length && !topValuesList.length ? (
        <ErrorText>There's no info yet!</ErrorText>
      ) : (
        <>
          <FirstTable>
            <Title>Top Pitching Values</Title>
            <TableHeader>
              <TableCell1>Pitch Type</TableCell1>
              <TableCell2>Velocity</TableCell2>
              <TableCell3>Spin Rate</TableCell3>
            </TableHeader>
            <TableContent>{topValuesList}</TableContent>
          </FirstTable>
          <SecondTable>
            <Title>Average Pitching Values</Title>
            <TableHeader>
              <TableCell1>Pitch Type</TableCell1>
              <TableCell2>Velocity</TableCell2>
              <TableCell3>Spin Rate</TableCell3>
            </TableHeader>
            <TableContent>{averageValuesList}</TableContent>
          </SecondTable>
        </>
      )}
    </div>
  );
};

const ErrorText = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const FirstTable = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 15px;
`;
const SecondTable = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  margin-top: 30px;
`;
const Title = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  font-weight: 400;
  line-height: 1.25;
  color: #414f5a;
  text-align: left;
`;
const TableHeader = styled.div`
  display: flex;
  position: sticky;
  min-height: 44px;
  margin-bottom: 6px;
  top: 0;
  background: #fff;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
  align-items: center;
`;
const TableContent = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: rgb(65, 79, 90);
`;
const TableCell1 = styled.div`
  display: flex;
  width: 42%;
  flex: 1 0 40%;
  align-items: center;
`;
const TableCell2 = styled.div`
  display: flex;
  width: 42%;
  flex: 1 0 42%;
  align-items: center;
`;
const TableCell3 = styled.div`
  display: flex;
  width: 15%;
  flex: 1 0 15%;
  align-items: center;
`;
const TableRow = styled.div`
  min-height: 44px;
  margin-bottom: 4px;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  background: #f7f8f9;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
  border-radius: 4px;
  :hover {
    background-color: rgb(236, 248, 255);
  }
`;

export default PitchingSummaryPanel;
