import { useState } from 'react';
import styled from 'styled-components';
import { IBattingLog } from '../../../../store/commonTypes';

const BattingLogsTableItem = ({
  date,
  pitcherName,
  pitchType,
  pitchCall,
  direction,
  distance,
  exitVelocity,
  hangTime,
  hitSpinRate,
  launchAngle,
  pitcherHandedness,
}: IBattingLog) => {
  const [isVisibleAdditionData, setVisibleAdditionData] = useState(false);

  return (
    <TableRow
      onClick={() => setVisibleAdditionData(!isVisibleAdditionData)}
      isOpened={isVisibleAdditionData}>
      <ItemRow>
        <TableCell1>{date || '-'}</TableCell1>
        <TableCell1>{pitcherName || '-'}</TableCell1>
        <TableCell2>{pitcherHandedness || '-'}</TableCell2>
        <TableCell2>{pitchType || '-'}</TableCell2>
        <TableCell3>{pitchCall || '-'}</TableCell3>
      </ItemRow>
      {isVisibleAdditionData && (
        <AdditionDataContainer>
          <AdditionDataHeader>
            <TableCell1>Exit Velocity</TableCell1>
            <TableCell1>Launch Angle</TableCell1>
            <TableCell4>Direction</TableCell4>
            <TableCell4>Hit Spin Rate</TableCell4>
            <TableCell4>Distance</TableCell4>
            <TableCell5>Hang Time</TableCell5>
          </AdditionDataHeader>
          <ItemRow>
            <TableCell1>{exitVelocity || '-'}</TableCell1>
            <TableCell1>{launchAngle || '-'}</TableCell1>
            <TableCell4>{direction || '-'}</TableCell4>
            <TableCell4>{hitSpinRate || '-'}</TableCell4>
            <TableCell4>{distance || '-'}</TableCell4>
            <TableCell5>{hangTime || '-'}</TableCell5>
          </ItemRow>
        </AdditionDataContainer>
      )}
    </TableRow>
  );
};

const TableRow = styled.div<{ isOpened: boolean }>`
  display: block;
  border-radius: 4px;
  margin-bottom: 6px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: rgb(65, 79, 90);
  ${props => props.isOpened && 'box-shadow: 0 0 6px 1px rgb(72 187 255 / 63%);'}
`;
const AdditionDataContainer = styled.div`
  padding: 16px;
  display: flex;
  flex-direction: column;
  margin-bottom: 6px;
`;
const ItemRow = styled.div`
  background-color: #f7f8f9;
  min-height: 44px;
  cursor: pointer;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  :hover {
    background-color: #ecf8ff;
  }
`;
const AdditionDataHeader = styled.div`
  display: grid;
  grid-template-columns: repeat(6, minmax(80px, 1fr));
  position: sticky;
  top: 0;
  background: #fff;
  min-height: 44px;
  margin-bottom: 6px;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
`;
const TableCell1 = styled.div`
  display: flex;
  width: 19.5%;
  flex: 1 0 19.5%;
`;
const TableCell2 = styled.div`
  display: flex;
  width: 21%;
  flex: 1 0 21%;
`;
const TableCell3 = styled.div`
  display: flex;
  width: 19%;
  flex: 1 0 19%;
`;
const TableCell4 = styled.div`
  display: flex;
  width: 15%;
  flex: 1 0 15%;
`;
const TableCell5 = styled.div`
  display: flex;
  width: 16%;
  flex: 1 0 16%;
`;

export default BattingLogsTableItem;
