export const firstPosition = [
  'catcher',
  'first_base',
  'second_base',
  'shortstop',
  'third_base',
  'outfield',
  'pitcher',
];
export const secondPosition = [
  '',
  'catcher',
  'first_base',
  'second_base',
  'shortstop',
  'third_base',
  'outfield',
  'pitcher',
];
export const hands = ['r', 'l'];
export const schoolYears = ['freshman', 'sophomore', 'senior', 'junior', 'none'];
