import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { selectUpdateProfileStatus } from '../../../../../store/user/slice';

const SaveButton = () => {
  const isUpdateProfile = useSelector(selectUpdateProfileStatus);
  return (
    <StyledButton isDisabled={isUpdateProfile} type="submit">
      {!isUpdateProfile ? 'Save' : <i className="fa fa-spinner fa-spin" />}
    </StyledButton>
  );
};

const StyledButton = styled.button<{ isDisabled: boolean }>`
  cursor: pointer;
  width: 100%;
  flex: 1 1 auto;
  padding: 7px 19px 10px 18px;
  border-radius: 4px;
  box-shadow: none;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  font-weight: 400;
  line-height: 19px;
  color: #ffffff;
  border: solid 1px transparent;
  box-shadow: 0 0 4px 0 rgb(72 187 255 / 0%);
  background-color: ${props =>
    props.isDisabled ? 'rgb(117, 164, 191)' : '#48bbff'};
  :hover {
    box-shadow: 0 0 4px 0 rgb(72 187 255 / 80%);
  }
`;

export default SaveButton;
