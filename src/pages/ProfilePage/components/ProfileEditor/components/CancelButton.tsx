import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { selectUpdateProfileStatus } from '../../../../../store/user/slice';

type EditorButtonProps = {
  onClick: () => void;
};

const CancelButton = ({ onClick }: EditorButtonProps) => {
  const isUpdateProfile = useSelector(selectUpdateProfileStatus);
  return (
    <StyledButton isDisabled={isUpdateProfile} onClick={onClick}>
      Cancel
    </StyledButton>
  );
};

const StyledButton = styled.button<{ isDisabled: boolean }>`
  cursor: pointer;
  width: 100%;
  flex: 1 1 auto;
  padding: 7px 19px 10px 18px;
  border-radius: 4px;
  box-shadow: none;
  font-family: 'Lato', sans-serif;
  margin-right: 12px;
  font-size: 16px;
  font-weight: 400;
  line-height: 19px;
  color rgb(51, 51, 51);
  border: solid 1px rgb(209, 215, 219);
  box-shadow: 0 2px 25px 0 rgb(0 0 0 / 0%);
  background-color: ${props =>
    props.isDisabled ? 'rgb(117, 164, 191)' : '#ffffff'};
  :hover {
    color: #48bbff;
    text-shadow: 0 0 4px 0 rgb(72 187 255 / 80%);
    box-shadow: 0 0 4px 0 rgb(72 187 255 / 80%);
    border: solid 1px #48bbff;
  }
`;

export default CancelButton;
