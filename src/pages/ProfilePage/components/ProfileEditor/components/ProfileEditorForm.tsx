import { Form, Field } from 'react-final-form';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { IUserEditorFormData } from '../../../../../store/user/types';
import { userPositionConverter } from '../../../../../utils/userPositionConverter';
import Input from './Input';
import ComboBox from './ComboBox';
import {
  selectClientData,
  selectUserData,
} from '../../../../../store/user/slice';
import userSchoolYearConverter from '../../../../../utils/userSchoolYearConverter';
import MultipleComboBox from './MultipleComboBox';
import { updateProfileAction } from '../../../../../store/user/actions';
import { firstPosition, hands, schoolYears, secondPosition } from './Arrays';
import {
  updateProfileValidator,
  firstNameValidator,
  lastNameValidator,
  ageValidator,
  feetValidator,
  weightValidator,
} from '../../../../../utils/updateProfileValidator';
import SaveButton from './SaveButton';
import CancelButton from './CancelButton';
import { fetchSchoolsListAction } from '../../../../../store/schools/actions';
import { fetchTeamsListAction } from '../../../../../store/teams/actions';
import { fetchFacilitiesListAction } from '../../../../../store/facilities/actions';
import {
  selectSchoolsList,
  selectSchoolsLoadStatus,
} from '../../../../../store/schools/slice';
import {
  selectTeamsList,
  selectTeamsLoadStatus,
} from '../../../../../store/teams/slice';
import {
  selectFacilitiesList,
  selectFacilitiesLoadStatus,
} from '../../../../../store/facilities/slice';
import { SmallLoaderSpinner } from '../../../../../components';

type ProfileEditorFormProps = {
  onRedirectToProfile: () => void;
};

const ProfileEditorForm = ({ onRedirectToProfile }: ProfileEditorFormProps) => {
  const user = useSelector(selectUserData);
  const clientData = useSelector(selectClientData);

  const schoolsListIsLoading = useSelector(selectSchoolsLoadStatus);
  const schoolsList = useSelector(selectSchoolsList);
  const teamsListIsLoading = useSelector(selectTeamsLoadStatus);
  const teamsList = useSelector(selectTeamsList);
  const facilitiesListIsLoading = useSelector(selectFacilitiesLoadStatus);
  const facilitiesList = useSelector(selectFacilitiesList);

  const dispatch = useDispatch();

  const handleUpdateSubmit = (values: IUserEditorFormData) => {
    console.log(values);
    dispatch(
      updateProfileAction({
        userData: {
          ...values,
          id: user.id,
          avatar: user.avatar,
          age: Number(values.age),
          feet: Number(values.feet),
          weight: Number(values.weight),
          school: schoolsList.find(
            value => String(value.id) === values.schoolId,
          ) || { id: 0, name: '' },
          teams: values.teams
            ? teamsList.filter(value => values.teams.includes(String(value.id)))
            : user.teams,
          facilities: values.facilities
            ? facilitiesList.filter(value =>
                values.facilities.includes(String(value.id)),
              )
            : user.facilities,
        },
        ...clientData,
        updateCallback: () => onRedirectToProfile(),
      }),
    );
  };

  return (
    <Form
      onSubmit={handleUpdateSubmit}
      validate={updateProfileValidator}
      render={({ handleSubmit, errors, submitFailed }) => (
        <FormContainer onSubmit={handleSubmit}>
          <UserInfoContainer>
            <LineForInitials>
              <Field
                name="firstName"
                initialValue={user.firstName}
                validate={firstNameValidator}>
                {props => (
                  <Input
                    width="30%"
                    type="text"
                    placeHolder="First Name *"
                    renderProps={props}
                  />
                )}
              </Field>
              <Field
                name="lastName"
                initialValue={user.lastName}
                validate={lastNameValidator}>
                {props => (
                  <Input
                    width="30%"
                    type="text"
                    placeHolder="Last Name *"
                    renderProps={props}
                  />
                )}
              </Field>
            </LineForInitials>
            <Field name="firstPosition" initialValue={user.firstPosition}>
              {props => (
                <ComboBox
                  searchable={false}
                  width="100%"
                  itemsList={firstPosition}
                  initialItemIndex={firstPosition.findIndex(
                    value => value === user.firstPosition,
                  )}
                  placeholder="Position in Game *"
                  dataConverter={userPositionConverter}
                  renderProps={props}
                />
              )}
            </Field>
            <Field name="secondPosition" initialValue={user.secondPosition}>
              {props => (
                <ComboBox
                  searchable={false}
                  width="100%"
                  itemsList={secondPosition}
                  initialItemIndex={secondPosition.findIndex(
                    value => value === user.secondPosition,
                  )}
                  placeholder="Secondary Position in Game"
                  dataConverter={userPositionConverter}
                  renderProps={props}
                />
              )}
            </Field>
          </UserInfoContainer>
          <SectionHeader>
            <SectionLine />
            <SectionTitle>Personal Info</SectionTitle>
          </SectionHeader>
          <Field
            name="age"
            initialValue={String(user.age)}
            validate={ageValidator}>
            {props => (
              <Input
                width="92%"
                type="number"
                placeHolder="Age *"
                renderProps={props}
              />
            )}
          </Field>
          <InlineContainer Width="90%">
            <Field
              name="feet"
              initialValue={String(user.feet)}
              validate={feetValidator}>
              {props => (
                <Input
                  width="40%"
                  type="number"
                  placeHolder="Feet *"
                  renderProps={props}
                />
              )}
            </Field>
            <Field name="inches" initialValue={String(user.inches)}>
              {props => (
                <Input
                  width="40%"
                  type="number"
                  placeHolder="Inches"
                  renderProps={props}
                />
              )}
            </Field>
          </InlineContainer>
          <Field
            name="weight"
            initialValue={String(user.weight)}
            validate={weightValidator}>
            {props => (
              <Input
                width="92%"
                type="number"
                placeHolder="Weight *"
                renderProps={props}
              />
            )}
          </Field>
          <InlineContainer Width="100%">
            <Field name="throwsHand" initialValue={user.throwsHand}>
              {props => (
                <ComboBox
                  searchable={false}
                  width="48%"
                  itemsList={hands}
                  initialItemIndex={hands.findIndex(
                    value =>
                      value.toLowerCase() === user.throwsHand.toLowerCase(),
                  )}
                  placeholder="Throws *"
                  dataConverter={value => value.toUpperCase()}
                  renderProps={props}
                />
              )}
            </Field>
            <Field name="batsHand" initialValue={user.batsHand}>
              {props => (
                <ComboBox
                  searchable={false}
                  width="48%"
                  itemsList={hands}
                  initialItemIndex={hands.findIndex(
                    value =>
                      value.toLowerCase() === user.batsHand.toLowerCase(),
                  )}
                  placeholder="Bats *"
                  dataConverter={value => value.toUpperCase()}
                  renderProps={props}
                />
              )}
            </Field>
          </InlineContainer>
          <SectionHeader>
            <SectionLine />
            <SectionTitle>School</SectionTitle>
          </SectionHeader>
          <ComboBoxWrapper>
            <SpinnerWrapper>
              {schoolsListIsLoading && <SmallLoaderSpinner />}
            </SpinnerWrapper>
            <Field name="schoolId" initialValue={String(user.school.id)}>
              {props => (
                <ComboBox
                  searchable
                  width="100%"
                  itemsList={schoolsList.map(value => String(value.id))}
                  initialItemIndex={schoolsList.findIndex(
                    value => value.id === user.school.id,
                  )}
                  placeholder="School"
                  onInputChange={value =>
                    dispatch(
                      fetchSchoolsListAction({
                        ...clientData,
                        searchInputValue: value,
                      }),
                    )
                  }
                  dataConverter={schoolId =>
                    schoolsList.find(value => String(value.id) === schoolId)
                      ?.name || ''
                  }
                  renderProps={props}
                />
              )}
            </Field>
          </ComboBoxWrapper>
          <Field name="schoolYear" initiaslValue={user.schoolYear}>
            {props => (
              <ComboBox
                searchable={false}
                width="100%"
                itemsList={schoolYears}
                initialItemIndex={schoolYears.findIndex(
                  value => value === user.schoolYear,
                )}
                placeholder="School Year"
                dataConverter={userSchoolYearConverter}
                renderProps={props}
              />
            )}
          </Field>
          <ComboBoxWrapper>
            <SpinnerWrapper>
              {teamsListIsLoading && <SmallLoaderSpinner />}
            </SpinnerWrapper>
            <Field name="teams">
              {props => (
                <MultipleComboBox
                  itemsList={teamsList.map(value => String(value.id))}
                  initialItemsList={
                    user.teams ? user.teams.map(value => String(value.id)) : []
                  }
                  placeholder="Team"
                  onInputChange={value =>
                    dispatch(
                      fetchTeamsListAction({
                        ...clientData,
                        searchInputValue: value,
                      }),
                    )
                  }
                  dataConverter={value =>
                    teamsList.find(item => String(item.id) === value)?.name ||
                    ''
                  }
                  renderProps={props}
                />
              )}
            </Field>
          </ComboBoxWrapper>
          <SectionHeader>
            <SectionLine />
            <SectionTitle>Facility</SectionTitle>
          </SectionHeader>
          <ComboBoxWrapper>
            <SpinnerWrapper>
              {facilitiesListIsLoading && <SmallLoaderSpinner />}
            </SpinnerWrapper>
            <Field name="facilities">
              {props => (
                <MultipleComboBox
                  itemsList={facilitiesList.map(value => String(value.id))}
                  initialItemsList={
                    user.facilities
                      ? user.facilities.map(value => String(value.id))
                      : []
                  }
                  placeholder="Facility"
                  onInputChange={value =>
                    dispatch(
                      fetchFacilitiesListAction({
                        ...clientData,
                        searchInputValue: value,
                      }),
                    )
                  }
                  dataConverter={value =>
                    facilitiesList.find(item => String(item.id) === value)
                      ?.name || ''
                  }
                  renderProps={props}
                />
              )}
            </Field>
          </ComboBoxWrapper>
          <SectionHeader>
            <SectionLine />
            <SectionTitle>About</SectionTitle>
          </SectionHeader>
          <Field name="biography" initialValue={user.biography}>
            {props => (
              <AboutTextArea
                {...props.input}
                placeholder="Describe yourself in a few words"
              />
            )}
          </Field>
          {submitFailed && !!errors?.message && (
            <ErrorText>{errors?.message}</ErrorText>
          )}
          <ButtonsContainer>
            <CancelButton onClick={() => onRedirectToProfile()} />
            <SaveButton />
          </ButtonsContainer>
        </FormContainer>
      )}
    />
  );
};

const FormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
`;
const UserInfoContainer = styled.div`
  margin-bottom: 11px;
`;
const LineForInitials = styled.div`
  width: 90%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 9px;
`;
const InlineContainer = styled.div<{ Width: string }>`
  width: ${props => props.Width};
  display: flex;
  justify-content: space-between;
  margin-bottom: 11px;
`;
const AboutTextArea = styled.textarea`
  font-family: 'Lato', sans-serif;
  display: block;
  outline: none;
  width: 86%;
  min-height: 86px;
  resize: none;
  border-radius: 4px;
  background-color: #eff1f3;
  padding: 11px 16px;
  font-size: 16px;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  border: 1px solid transparent;
  margin-bottom: 15px;
`;
const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
`;
const SectionHeader = styled.div`
  display: flex;
  position: relative;
  margin-bottom: 15px;
`;
const SectionTitle = styled.div`
  font-family: 'Lato', sans-serif;
  line-height: 22.5px;
  font-size: 18px;
  font-weight: 900;
  color: #414f5a;
  text-align: left;
  display: inline-block;
  position: relative;
  z-index: 1;
  background-color: #ffffff;
  padding-right: 12px;
`;
const SectionLine = styled.div`
  content: '';
  position: absolute;
  top: 11px;
  left: 0;
  right: 0;
  height: 1px;
  background-color: #e7ebef;
  z-index: 0;
`;

const ErrorText = styled.span`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  margin-top: 4px;
  color: #f05f62;
`;

const SpinnerWrapper = styled.div`
  position: absolute;
  left: 78%;
  top: 17px;
  z-index: 2;
`;
const ComboBoxWrapper = styled.div`
  position: relative;
`;

export default ProfileEditorForm;
