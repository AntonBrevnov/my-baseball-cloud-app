import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';

type InputProps = {
  width: string;
  type: string;
  placeHolder: string;
  renderProps: FieldRenderProps<string>;
};

const Input = ({ width, type, placeHolder, renderProps }: InputProps) => {
  const { input, meta } = renderProps;
  return (
    <InputWrapper Width={width}>
      <StyledInput {...input} type={type} placeholder={placeHolder} />
      <ErrorText>{meta.error}</ErrorText>
    </InputWrapper>
  );
};

const InputWrapper = styled.div<{ Width: string }>`
  width: ${props => props.Width};
  display: flex;
  flex: 0 0 43%;
  flex-direction: column;
`;
const StyledInput = styled.input`
  margin-bottom: 11px;
  width: 100%;
  padding-left: 16px;
  border-radius: 4px;
  background-color: #eff1f3;
  outline: none;
  height: 38px;
  background-color: #eff1f3;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  border: 1px solid transparent;
  :focus {
    border: 1px solid #48bbff;
    background-color: #fff;
  }
`;

const ErrorText = styled.span`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  margin-bottom: 8px;
  color: #f05f62;
`;

export default Input;
