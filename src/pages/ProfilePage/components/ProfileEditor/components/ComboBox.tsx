import { useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';

type ComboBoxProps = {
  initialItemIndex: number;
  width: string;
  searchable: boolean;
  itemsList: string[];
  placeholder: string;
  onInputChange?: (value: string) => void;
  dataConverter: (value: string) => string;
  renderProps: FieldRenderProps<string>;
};

const ComboBox = ({
  width,
  initialItemIndex,
  searchable,
  itemsList,
  placeholder,
  onInputChange,
  dataConverter,
  renderProps,
}: ComboBoxProps) => {
  const [isShowingList, setShowingListState] = useState(false);
  const [selectedItemValue, setSelectedItemValue] = useState(
    itemsList[initialItemIndex >= 0 ? initialItemIndex : 0],
  );
  const [selectedItemIndex, setSelectedItemIndex] = useState(
    initialItemIndex >= 0 ? initialItemIndex : 0,
  );
  const [inputValue, setInputValue] = useState('');

  const { input } = renderProps;

  const list = itemsList.map((value, index) => (
    <ListItem
      key={Math.round(Math.random() * 10000)}
      isSelected={selectedItemIndex === index}
      onClick={() => {
        setShowingListState(false);
        setSelectedItemValue(value);
        setSelectedItemIndex(index);
        input.onChange(value);
        setInputValue('');
      }}>
      <ListItemText>{dataConverter(value) || '-'}</ListItemText>
    </ListItem>
  ));

  return (
    <ComboBoxContainer Width={width}>
      <input {...input} type="hidden" />
      <ComboBoxHeader
        isSelected={isShowingList}
        onClick={() => setShowingListState(!isShowingList)}>
        {!searchable ? (
          <ComboBoxHeaderText>
            {dataConverter(selectedItemValue) || placeholder}
          </ComboBoxHeaderText>
        ) : (
          <>
            <ComboboxInput
              type="text"
              value={inputValue}
              onChange={e => {
                setInputValue(e.currentTarget.value);
                if (onInputChange) {
                  onInputChange(e.currentTarget.value);
                }
              }}
            />
            {!inputValue && (
              <UnfixedComboBoxHeaderText>
                {dataConverter(selectedItemValue) || placeholder}
              </UnfixedComboBoxHeaderText>
            )}
          </>
        )}
        <SelectArrowSpan>
          {isShowingList ? (
            <span className="fa fa-angle-up" />
          ) : (
            <span className="fa fa-angle-down" />
          )}
        </SelectArrowSpan>
      </ComboBoxHeader>
      {isShowingList && <ItemsList>{list}</ItemsList>}
    </ComboBoxContainer>
  );
};

const ComboBoxContainer = styled.div<{ Width: string }>`
  cursor: pointer;
  position: relative;
  width: ${props => props.Width};
  display: flex;
  flex-direction: column;
`;
const ComboBoxHeader = styled.div<{ isSelected: boolean }>`
  position: relative;
  width: 100%;
  height: 38px;
  border-radius: 4px;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  background-color: ${props => (props.isSelected ? '#fff' : '#eff1f3')};
  border: ${props =>
    props.isSelected ? '1px solid #48bbff' : '1px solid transparent'};
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const ComboBoxHeaderText = styled.div`
  padding-left: 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const UnfixedComboBoxHeaderText = styled.div`
  position: absolute;
  padding-left: 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const ComboboxInput = styled.input`
  outline: none;
  color: #667784;
  padding-left: 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  border: 0px solid transparent;
  background-color: transparent;
`;
const ItemsList = styled.div`
  position: absolute;
  z-index: 2;
  top: 47px;
  width: 100%;
  min-height: 40px;
  max-height: 150px;
  overflow-y: scroll;
  border-radius: 5px;
  box-shadow: 0px 0px 5px rgba(61, 61, 61, 0.2);
`;
const ListItem = styled.div<{ isSelected: boolean }>`
  width: 100%;
  height: 38px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  display: flex;
  align-items: center;
  color: #667784;
  background-color: ${props => (!props.isSelected ? '#fff' : '#def')};
`;
const ListItemText = styled.div`
  padding: 0px 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const SelectArrowSpan = styled.span`
  width: 27px;
  text-align: left;
  padding-top: 2px;
`;

export default ComboBox;
