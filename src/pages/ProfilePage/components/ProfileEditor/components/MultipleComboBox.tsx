import { useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';

type ComboBoxProps = {
  itemsList: string[];
  placeholder: string;
  initialItemsList?: string[];
  onInputChange?: (value: string) => void;
  dataConverter: (value: string) => string;
  renderProps: FieldRenderProps<string[]>;
};

const ComboBox = ({
  itemsList,
  placeholder,
  initialItemsList,
  onInputChange,
  dataConverter,
  renderProps,
}: ComboBoxProps) => {
  const [isShowingList, setShowingListState] = useState(false);
  const [selectedItemsList, setSelectedItemsList] = useState<string[]>(
    initialItemsList || [],
  );
  const [inputValue, setInputValue] = useState('');
  const { input } = renderProps;

  const handleKeyDownOnInput = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Backspace' && !inputValue) {
      const arr = [...selectedItemsList];
      arr.splice(arr.length - 1);
      setSelectedItemsList([...arr]);
      input.onChange([...selectedItemsList]);
    }
  };
  const handleDeleteButtonClick = (index: number) => {
    const arr = [...selectedItemsList];
    arr.splice(index, 1);
    setSelectedItemsList([...arr]);
    input.onChange([...selectedItemsList]);
  };

  const list = itemsList
    .filter(
      filterValue =>
        selectedItemsList.findIndex(value => value === filterValue) === -1,
    )
    .map(value => (
      <ListItem
        key={Math.round(Math.random() * 10000)}
        onClick={() => {
          setShowingListState(false);
          setSelectedItemsList([...selectedItemsList, value]);
          input.onChange([...selectedItemsList, value]);
          setInputValue('');
        }}>
        <ListItemText>{dataConverter(value) || '-'}</ListItemText>
      </ListItem>
    ));

  const selectedItems = selectedItemsList.map((value, index) => (
    <ComboBoxSelectedItem key={Math.round(Math.random() * 10000)}>
      <ComboBoxDeleteItemButton onClick={() => handleDeleteButtonClick(index)}>
        ×
      </ComboBoxDeleteItemButton>
      {dataConverter(value) || '-'}
    </ComboBoxSelectedItem>
  ));

  return (
    <ComboBoxContainer>
      <input {...input} type="hidden" />
      <ComboBoxHeader
        isSelected={isShowingList}
        onClick={() => setShowingListState(!isShowingList)}>
        <ComboBoxSelectedItemsWrapper>
          {selectedItems}
          <ComboboxInput
            type="text"
            placeholder={!selectedItemsList.length ? placeholder : ''}
            value={inputValue}
            onClick={() => setShowingListState(!isShowingList)}
            onChange={e => {
              setInputValue(e.currentTarget.value);
              if (onInputChange) {
                onInputChange(e.currentTarget.value);
              }
            }}
            onKeyDown={handleKeyDownOnInput}
          />
        </ComboBoxSelectedItemsWrapper>
        <SelectArrowSpan>
          {isShowingList ? (
            <span className="fa fa-angle-up" />
          ) : (
            <span className="fa fa-angle-down" />
          )}
        </SelectArrowSpan>
        {isShowingList && <ItemsList>{list}</ItemsList>}
      </ComboBoxHeader>
    </ComboBoxContainer>
  );
};

const ComboBoxContainer = styled.div`
  cursor: pointer;
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  background-color: #fff;
`;
const ComboBoxHeader = styled.div<{ isSelected: boolean }>`
  position: relative;
  width: 100%;
  padding-bottom: 5px;
  min-height: 38px;
  border-radius: 4px;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  background-color: ${props => (props.isSelected ? '#fff' : '#eff1f3')};
  border: ${props =>
    props.isSelected ? '1px solid #48bbff' : '1px solid transparent'};
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const ComboBoxSelectedItemsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  height: 100%;
`;
const ComboBoxSelectedItem = styled.div`
  font-family: 'Lato', sans-serif;
  background-color: rgba(0, 126, 255, 0.08);
  border-radius: 2px;
  border: 1px solid rgba(0, 126, 255, 0.24);
  color: #007eff;
  display: inline-block;
  font-size: 0.9em;
  line-height: 1.4;
  padding: 2px 5px;
  padding-left: 0px;
  margin-left: 5px;
  margin-top: 5px;
  vertical-align: top;
`;
const ComboBoxDeleteItemButton = styled.span`
  cursor: pointer;
  border-bottom-left-radius: 2px;
  border-top-left-radius: 2px;
  border-right: 1px solid #c2e0ff;
  border-right: 1px solid rgba(0, 126, 255, 0.24);
  padding: 1px 5px 3px;
`;
const ComboboxInput = styled.input`
  outline: none;
  width: 50px;
  padding-left: 8px;
  max-width: 100%;
  color: #667784;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  border: 0px solid transparent;
  background-color: transparent;
`;
const ItemsList = styled.div`
  position: absolute;
  top: 110%;
  z-index: 2;
  width: 100%;
  min-height: 40px;
  max-height: 150px;
  overflow-y: scroll;
  border-radius: 5px;
  box-shadow: 0px 0px 5px rgba(61, 61, 61, 0.2);
`;
const ListItem = styled.div`
  width: 100%;
  height: 38px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  display: flex;
  align-items: center;
  color: #667784;
  background-color: #fff;
`;
const ListItemText = styled.div`
  padding: 0px 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const SelectArrowSpan = styled.span`
  width: 27px;
  text-align: left;
  padding-top: 2px;
`;

export default ComboBox;
