import styled from 'styled-components';
import ChooseAvatarForm from './components/ChooseAvatarForm';
import ProfileEditorForm from './components/ProfileEditorForm';

type ProfileEditorPanelProps = {
  onRedirectToProfile: () => void;
};

const ProfileEditorPanel = ({
  onRedirectToProfile,
}: ProfileEditorPanelProps) => (
    <MainContainer>
      <ChooseAvatarForm />
      <ProfileEditorForm onRedirectToProfile={onRedirectToProfile} />
    </MainContainer>
  );

const MainContainer = styled.aside`
  background: #fff;
  border-left: 1px solid rgba(0, 0, 0, 0.1);
  width: 264px;
  overflow-x: auto;
  overflow-y: scroll;
  padding: 16px;
`;

export default ProfileEditorPanel;
