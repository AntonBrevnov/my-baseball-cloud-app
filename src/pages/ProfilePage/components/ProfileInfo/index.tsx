import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import {
  AgeIcon,
  EditProfileIcon,
  HeightIcon,
  WeightIcon,
  ThrowsIcon,
  BatsIcon,
} from '../../../../assets/svg';
import {
  selectInitializeStatus,
  selectLoadProfileStatus,
  selectUserData,
} from '../../../../store/user/slice';
import { namesStringGenerator } from '../../../../utils/namesStringGenerator';
import { userPositionConverter } from '../../../../utils/userPositionConverter';
import userSchoolYearConverter from '../../../../utils/userSchoolYearConverter';

type ProfileInfoProps = {
  onEditProfile: () => void;
};

const ProfileInfo = ({ onEditProfile }: ProfileInfoProps) => {
  const {
    firstName,
    lastName,
    firstPosition,
    secondPosition,
    avatar,
    age,
    feet,
    inches,
    weight,
    throwsHand,
    batsHand,
    school,
    schoolYear,
    teams,
    facilities,
    biography,
  } = useSelector(selectUserData);
  const userIsLoaded = useSelector(selectLoadProfileStatus);
  const userIsInitialized = useSelector(selectInitializeStatus);

  useEffect(() => {
    if (!userIsLoaded && !userIsInitialized) {
      onEditProfile();
    }
  }, [userIsInitialized, userIsLoaded]);

  return (
    <MainContainer>
      <UserInfo>
        <EditProfileButton onClick={() => onEditProfile()}>
          <EditProfileIcon />
        </EditProfileButton>
        <AvatarContainer>
          <Avatar imageUrl={avatar || './images/avatar.png'} />
        </AvatarContainer>
        <UserInfoBottom>
          <UserInfoTitle>
            {firstName} {lastName}
          </UserInfoTitle>
          <UserInfoFirstPosition hasSecondPosition={!!secondPosition}>
            {userPositionConverter(firstPosition)}
          </UserInfoFirstPosition>
          <UserInfoSecondPosition>
            {userPositionConverter(secondPosition)}
          </UserInfoSecondPosition>
        </UserInfoBottom>
      </UserInfo>
      <PersonalInfo>
        <PersonalInfoItem>
          <PersonalInfoItemIcon>
            <AgeIcon />
          </PersonalInfoItemIcon>
          <PersonalInfoItemTitle>Age</PersonalInfoItemTitle>
          <PersonalInfoItemValue>{age}</PersonalInfoItemValue>
        </PersonalInfoItem>
        <PersonalInfoItem>
          <PersonalInfoItemIcon>
            <HeightIcon />
          </PersonalInfoItemIcon>
          <PersonalInfoItemTitle>Height</PersonalInfoItemTitle>
          <PersonalInfoItemValue>
            {feet} ft {inches} in
          </PersonalInfoItemValue>
        </PersonalInfoItem>
        <PersonalInfoItem>
          <PersonalInfoItemIcon>
            <WeightIcon />
          </PersonalInfoItemIcon>
          <PersonalInfoItemTitle>Weight</PersonalInfoItemTitle>
          <PersonalInfoItemValue>{weight} lbs</PersonalInfoItemValue>
        </PersonalInfoItem>
        <PersonalInfoItem>
          <PersonalInfoItemIcon>
            <ThrowsIcon />
          </PersonalInfoItemIcon>
          <PersonalInfoItemTitle>Throws</PersonalInfoItemTitle>
          <PersonalInfoItemValue>
            {throwsHand && throwsHand.toUpperCase()}
          </PersonalInfoItemValue>
        </PersonalInfoItem>
        <PersonalInfoItem>
          <PersonalInfoItemIcon>
            <BatsIcon />
          </PersonalInfoItemIcon>
          <PersonalInfoItemTitle>Bats</PersonalInfoItemTitle>
          <PersonalInfoItemValue>
            {batsHand && batsHand.toUpperCase()}
          </PersonalInfoItemValue>
        </PersonalInfoItem>
      </PersonalInfo>
      <SchoolInfo>
        <SchoolInfoItem>
          <SchoolInfoItemTitle>School</SchoolInfoItemTitle>
          <SchoolInfoItemValue>{school && school.name}</SchoolInfoItemValue>
        </SchoolInfoItem>
        <SchoolInfoItem>
          <SchoolInfoItemTitle>School Year</SchoolInfoItemTitle>
          <SchoolInfoItemValue>
            {userSchoolYearConverter(schoolYear)}
          </SchoolInfoItemValue>
        </SchoolInfoItem>
        <SchoolInfoItem>
          <SchoolInfoItemTitle>Team</SchoolInfoItemTitle>
          <SchoolInfoItemValue>
            {teams && teams.length && namesStringGenerator(teams)}
          </SchoolInfoItemValue>
        </SchoolInfoItem>
        <SchoolInfoItem>
          <SchoolInfoItemTitle>Facility</SchoolInfoItemTitle>
          <SchoolInfoItemValue>
            {facilities &&
              facilities.length &&
              namesStringGenerator(facilities)}
          </SchoolInfoItemValue>
        </SchoolInfoItem>
        <SchoolInfoAboutHeader>
          <SchoolInfoAboutLine />
          <SchoolInfoAboutTitle>About</SchoolInfoAboutTitle>
        </SchoolInfoAboutHeader>
        <SchoolInfoAboutBody>{biography}</SchoolInfoAboutBody>
      </SchoolInfo>
    </MainContainer>
  );
};

const MainContainer = styled.aside`
  background: #fff;
  border-left: 1px solid rgba(0, 0, 0, 0.1);
  width: 264px;
  overflow-x: auto;
  overflow-y: scroll;
  padding: 16px;
`;

// user information styles
const UserInfo = styled.div`
  position: relative;
  box-sizing: border-box;
`;
const EditProfileButton = styled.button`
  position: absolute;
  cursor: pointer;
  top: 12px;
  right: 13px;
  background-color: transparent;
  border-style: none;
  margin: 0;
  padding: 0;
`;
const AvatarContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-bottom: 6px;
`;
const Avatar = styled.div<{ imageUrl: string }>`
  width: 100px;
  height: 100px;
  background-image: url(${props => props.imageUrl});
  background-size: cover;
  background-position: 50% 50%;
  border-radius: 50%;
`;
const UserInfoBottom = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const UserInfoTitle = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 20px;
  text-align: center;
  color: #414f5a;
`;
const UserInfoFirstPosition = styled.div<{ hasSecondPosition: boolean }>`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  text-align: center;
  color: #788b99;
  width: 85px;
  border-bottom: ${props =>
    props.hasSecondPosition
      ? '1px solid rgba(120, 139, 153, 0.5)'
      : '0px solid #000'};
`;
const UserInfoSecondPosition = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  text-align: center;
  color: #788b99;
`;

// personal information styles
const PersonalInfo = styled.div`
  display: flex;
  flex-direction: column;
`;
const PersonalInfoItem = styled.div`
  display: flex;
  padding: 16px 0;
  justify-content: space-between;
`;
const PersonalInfoItemIcon = styled.span`
  width: 24px;
  height: 24px;
  margin-right: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const PersonalInfoItemTitle = styled.span`
  width: auto;
  margin-right: auto;
  color: rgb(51, 51, 51);
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const PersonalInfoItemValue = styled.span`
  width: auto;
  margin: 0;
  color: rgb(51, 51, 51);
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;

// school information styles
const SchoolInfo = styled.div`
  display: flex;
  flex-direction: column;
`;
const SchoolInfoItem = styled.div`
  display: flex;
  flex-direction: column;
`;
const SchoolInfoItemTitle = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 24px;
  text-align: left;
  color: #667784;
  font-size: 14px;
  line-height: 17px;
  font-weight: 300;
  margin-bottom: 3px;
`;
const SchoolInfoItemValue = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
  word-wrap: break-word;
  margin-bottom: 11px;
`;
const SchoolInfoAboutHeader = styled.div`
  display: flex;
  position: relative;
`;
const SchoolInfoAboutTitle = styled.div`
  font-family: 'Lato', sans-serif;
  line-height: 1.25;
  font-size: 18px;
  font-weight: 900;
  color: #414f5a;
  text-align: left;
  display: inline-block;
  position: relative;
  z-index: 1;
  background-color: #ffffff;
  padding-right: 12px;
`;
const SchoolInfoAboutLine = styled.div`
  content: '';
  position: absolute;
  top: 11px;
  left: 0;
  right: 0;
  height: 1px;
  background-color: #e7ebef;
  z-index: 0;
`;
const SchoolInfoAboutBody = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #788b99;
  line-height: 1.75;
  word-wrap: break-word;
`;

export default ProfileInfo;
