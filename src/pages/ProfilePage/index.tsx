import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { fetchUserDataAction } from '../../store/user/actions';
import {
  selectClientData,
  selectLoadProfileStatus,
  selectUserData,
} from '../../store/user/slice';

import { LoaderSpinner, Toast } from '../../components';
import {
  ProfileInfo,
  ProfileEditorPanel,
  ProfileMainContent,
} from './components';
import {
  selectToastData,
  selectToastVisibleState,
} from '../../store/toast/slice';

// eslint-disable-next-line 
const ProfilePage = () => {
  const toastData = useSelector(selectToastData);
  const toastIsVisible = useSelector(selectToastVisibleState);
  const userData = useSelector(selectUserData);
  const clientData = useSelector(selectClientData);
  const profileIsLoading = useSelector(selectLoadProfileStatus);
  const dispatch = useDispatch();

  const [isEditProfile, setEditProfileState] = useState(false);

  useEffect(() => {
    if (userData.id === -1) {
      dispatch(fetchUserDataAction({ ...clientData }));
    }
  }, [userData]);

  return (
    <PageContainer>
      {profileIsLoading ? (
        <LoaderSpinner />
      ) : (
        <InsideContainer>
          {isEditProfile ? (
            <ProfileEditorPanel
              onRedirectToProfile={() => setEditProfileState(false)}
            />
          ) : (
            <ProfileInfo onEditProfile={() => setEditProfileState(true)} />
          )}
          <ProfileMainContent />
        </InsideContainer>
      )}
      {toastIsVisible && <Toast {...toastData} />}
    </PageContainer>
  );
};

const PageContainer = styled.div`
  grid-area: content;
  background: #fff;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
  box-sizing: border-box;
`;

const InsideContainer = styled.div`
  display: flex;
  flex: 2;
  overflow: auto;
  width: 100%;
  height: 100%;
`;

export default ProfilePage;
