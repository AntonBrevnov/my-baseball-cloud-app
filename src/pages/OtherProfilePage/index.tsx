import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { OtherProfileInfo, OtherProfileMainContent } from './components';
import { LoaderSpinner, Toast } from '../../components';
import { fetchOtherUserDataAction } from '../../store/otherUser/actions';
import {
  selectOtherProfileLoadStatus,
  selectSelectedOtherUserId,
} from '../../store/otherUser/slice';
import { selectClientData } from '../../store/user/slice';
import {
  selectToastData,
  selectToastVisibleState,
} from '../../store/toast/slice';

const OtherProfilePage = () => {
  const toastData = useSelector(selectToastData);
  const toastIsVisible = useSelector(selectToastVisibleState);
  const clientData = useSelector(selectClientData);
  const profileIsLoading = useSelector(selectOtherProfileLoadStatus);
  const selectedOtherUserId = useSelector(selectSelectedOtherUserId);
  const dispatch = useDispatch();

  useEffect(() => {
    if (selectedOtherUserId !== -1) {
      dispatch(
        fetchOtherUserDataAction({
          ...clientData,
          profileId: selectedOtherUserId,
        }),
      );
    }
  }, [selectedOtherUserId]);

  return (
    <PageContainer>
      {profileIsLoading ? (
        <LoaderSpinner />
      ) : (
        <InsideContainer>
          <OtherProfileInfo />
          <OtherProfileMainContent />
        </InsideContainer>
      )}
      {toastIsVisible && <Toast {...toastData} />}
    </PageContainer>
  );
};

const PageContainer = styled.div`
  grid-area: content;
  background: #fff;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
  box-sizing: border-box;
`;

const InsideContainer = styled.div`
  display: flex;
  flex: 2;
  overflow: auto;
  width: 100%;
  height: 100%;
`;

export default OtherProfilePage;
