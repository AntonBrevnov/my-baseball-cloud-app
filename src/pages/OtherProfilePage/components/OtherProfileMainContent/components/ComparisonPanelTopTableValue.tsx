import {
  IBattingTopValue,
  IPitchingTopValue,
} from '../../../../../store/commonTypes';

type TopTableValueProps = {
  isPitcher: boolean;
  pitchType: string;
  topValueType: string;
  topBattingValues: IBattingTopValue[];
  topPitchingValues: IPitchingTopValue[];
};

const TopTableValue = ({
  isPitcher,
  pitchType,
  topValueType,
  topBattingValues,
  topPitchingValues,
}: TopTableValueProps) => {
  if (isPitcher) {
    const index = topPitchingValues.findIndex(
      value => value.pitchType === pitchType,
    );
    if (index !== -1) {
      if (topValueType === 'Velocity') {
        return <div>{topPitchingValues[index].velocity}</div>;
      }
      if (topValueType === 'Spin Rate') {
        return <div>{topPitchingValues[index].spinRate}</div>;
      }
    }
  } else {
    const index = topBattingValues.findIndex(
      value => value.pitchType === pitchType,
    );
    if (index !== -1) {
      if (topValueType === 'Distance') {
        return <div>{topBattingValues[index].distance}</div>;
      }
      if (topValueType === 'Launch Angle') {
        return <div>{topBattingValues[index].launchAngle}</div>;
      }
      if (topValueType === 'Exit Velocity') {
        return <div>{topBattingValues[index].exitVelocity}</div>;
      }
    }
  }
  return <div>-</div>;
};

export default TopTableValue;
