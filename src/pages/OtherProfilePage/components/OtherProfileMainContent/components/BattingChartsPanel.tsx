import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { fetchBattingGraphAction } from '../../../../../store/otherUser/actions';
import {
  selectOtherUserBattingGraph,
  selectOtherUserData,
} from '../../../../../store/otherUser/slice';
import { selectClientData } from '../../../../../store/user/slice';
import { BlueComboBox } from '../../../../../components';

const BattingChartsPanel = () => {
  const clientData = useSelector(selectClientData);
  const { id, firstName, lastName } = useSelector(selectOtherUserData);
  const battingGraph = useSelector(selectOtherUserBattingGraph);
  const [pitchTypeValue, setPitchTypeValue] = useState('None');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      fetchBattingGraphAction({
        ...clientData,
        profileId: id,
        pitchType: pitchTypeValue,
      }),
    );
  }, [pitchTypeValue]);

  const chartOptions: Highcharts.Options = {
    title: {
      text: `Rolling Velocity for ${firstName} ${lastName}`,
    },
    subtitle: {
      text: `Average over last last ${battingGraph.length} pitches`,
    },
    yAxis: {
      title: {
        text: 'Velocity',
      },
    },
    series: [
      {
        name: 'Velocity',
        type: 'spline',
        data: battingGraph,
      },
    ],
  };

  return (
    <div>
      <PanelHeader>
        <BlueComboBox
          title={value =>
            value === 'None' ? 'PitchType' : `PitchType (${value})`
          }
          initialItemIndex={0}
          itemsList={[
            'None',
            'Four Seam Fastball',
            'Two Seam Fastball',
            'Curveball',
            'Changeup',
            'Slider',
          ]}
          onChangeItem={(_, value) => setPitchTypeValue(value || 'None')}
          dataConverter={value => value}
        />
      </PanelHeader>
      {battingGraph.length ? (
        <HighchartsReact highcharts={Highcharts} options={chartOptions} />
      ) : (
        <ErrorText>There's no info yet!</ErrorText>
      )}
    </div>
  );
};

const PanelHeader = styled.div`
  display: flex;
  flex-direction: row-reverse;
`;
const ErrorText = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;

export default BattingChartsPanel;
