import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { fetchBattingLogsAction } from '../../../../../store/otherUser/actions';
import {
  selectOtherUserLogsLoadStatus,
  selectSelectedOtherUserId,
  selectOtherUserBattingLogsCount,
  selectOtherUserBattingLogs,
} from '../../../../../store/otherUser/slice';
import { selectClientData } from '../../../../../store/user/slice';
import {
  SearchInput,
  Pagination,
  LoaderSpinner,
  BlueComboBox,
} from '../../../../../components';
import BattingLogsTableItem from './BattingLogsTableItem';

const BattingLogsPanel = () => {
  const otherUserId = useSelector(selectSelectedOtherUserId);
  const clientData = useSelector(selectClientData);
  const logsIsLoading = useSelector(selectOtherUserLogsLoadStatus);
  const battingLogs = useSelector(selectOtherUserBattingLogs);
  const battingLogsTotalCount = useSelector(selectOtherUserBattingLogsCount);
  const [currentPageIndex, setCurrentPageIndex] = useState(0);
  const [pitchTypeValue, setPitchTypeValue] = useState('None');
  const [pitcherNameInputValue, setPitcherNameInputValue] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      fetchBattingLogsAction({
        ...clientData,
        pitchType: pitchTypeValue,
        name: pitcherNameInputValue,
        count: 10,
        offset: currentPageIndex * 10,
        profileId: otherUserId,
      }),
    );
  }, [currentPageIndex, pitchTypeValue, pitcherNameInputValue]);

  const logsTableItemsList = battingLogs.map(log => (
    <BattingLogsTableItem key={Math.round(Math.random() * 10000)} {...log} />
  ));

  return (
    <div>
      <PanelHeader>
        <SearchInput
          width="171px"
          placeholder="Search"
          onChange={value => setPitcherNameInputValue(value)}
        />
        <BlueComboBox
          title={value =>
            value === 'None' ? 'PitchType' : `PitchType (${value})`
          }
          initialItemIndex={0}
          itemsList={[
            'None',
            'Four Seam Fastball',
            'Two Seam Fastball',
            'Curveball',
            'Changeup',
            'Slider',
          ]}
          onChangeItem={(_, value) => setPitchTypeValue(value || 'None')}
          dataConverter={value => value}
        />
      </PanelHeader>
      <PanelTitle>Batting Logs</PanelTitle>
      {battingLogs.length ? (
        <>
          {logsIsLoading ? (
            <LoaderSpinner />
          ) : (
            <Table>
              <TableHeader>
                <TableCell1>Date</TableCell1>
                <TableCell1>Pitcher Name</TableCell1>
                <TableCell2>Pitch Handedness</TableCell2>
                <TableCell2>Pitch Type</TableCell2>
                <TableCell3>Pitch Call</TableCell3>
              </TableHeader>
              {logsTableItemsList}
            </Table>
          )}
          <Pagination
            isFixed={false}
            currentPageIndex={currentPageIndex}
            maxPagesCount={Math.round(battingLogsTotalCount / 10)}
            onChangePageIndex={index => setCurrentPageIndex(index)}
          />
        </>
      ) : (
        <ErrorText>There's no info yet!</ErrorText>
      )}
    </div>
  );
};

const PanelHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 23px;
`;
const ErrorText = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  min-height: 420px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const PanelTitle = styled.div`
  font-family: 'Lato', sans-serif;
  line-height: 1.25;
  font-size: 18px;
  color: #414f5a;
  font-weight: 400;
  text-align: left;
`;
const Table = styled.div`
  display: flex;
  flex-direction: column;
`;
const TableHeader = styled.div`
  display: flex;
  position: sticky;
  top: 0;
  background: #fff;
  min-height: 44px;
  margin-bottom: 6px;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
`;
const TableCell1 = styled.div`
  display: flex;
  width: 19.5%;
  flex: 1 0 19.5%;
`;
const TableCell2 = styled.div`
  display: flex;
  width: 21%;
  flex: 1 0 21%;
`;
const TableCell3 = styled.div`
  display: flex;
  width: 19%;
  flex: 1 0 19%;
`;

export default BattingLogsPanel;
