import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { selectOtherUserBattingSummary } from '../../../../../store/otherUser/slice';

const BattingSummaryPanel = () => {
  const battingSummary = useSelector(selectOtherUserBattingSummary);

  const averageValuesList = battingSummary.averageValues.map(value => (
    <>
      {value.pitchType && (
        <TableRow key={Math.round(Math.random() * 100)}>
          <TableCell>{value.pitchType ? value.pitchType : '-'}</TableCell>
          <TableCell>{value.distance ? value.distance : '-'}</TableCell>
          <TableCell>{value.launchAngle ? value.launchAngle : '-'}</TableCell>
          <TableCell>{value.exitVelocity ? value.exitVelocity : '-'}</TableCell>
        </TableRow>
      )}
    </>
  ));

  const topValuesList = battingSummary.topValues.map(value => (
    <>
      {value.pitchType && (
        <TableRow key={Math.round(Math.random() * 100)}>
          <TableCell>{value.pitchType ? value.pitchType : '-'}</TableCell>
          <TableCell>{value.distance ? value.distance : '-'}</TableCell>
          <TableCell>{value.launchAngle ? value.launchAngle : '-'}</TableCell>
          <TableCell>{value.exitVelocity ? value.exitVelocity : '-'}</TableCell>
        </TableRow>
      )}
    </>
  ));

  return (
    <div>
      <FirstTable>
        <Title>Top Batting Values</Title>
        <TableHeader>
          <TableCell>Pitch Type</TableCell>
          <TableCell>Distance</TableCell>
          <TableCell>Launch Angle</TableCell>
          <TableCell>Exit Velocity</TableCell>
        </TableHeader>
        <TableContent>
          {topValuesList.length ? (
            topValuesList
          ) : (
            <ErrorText>There's no info yet!</ErrorText>
          )}
        </TableContent>
      </FirstTable>
      <SecondTable>
        <Title>Average Batting Values</Title>
        <TableHeader>
          <TableCell>Pitch Type</TableCell>
          <TableCell>Distance</TableCell>
          <TableCell>Launch Angle</TableCell>
          <TableCell>Exit Velocity</TableCell>
        </TableHeader>
        <TableContent>
          {averageValuesList.length ? (
            averageValuesList
          ) : (
            <ErrorText>There's no info yet!</ErrorText>
          )}
        </TableContent>
      </SecondTable>
    </div>
  );
};

const ErrorText = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const FirstTable = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 15px;
`;
const SecondTable = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  margin-top: 30px;
`;
const Title = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  font-weight: 400;
  line-height: 1.25;
  color: #414f5a;
  text-align: left;
`;
const TableHeader = styled.div`
  display: flex;
  position: sticky;
  min-height: 44px;
  margin-bottom: 6px;
  top: 0;
  background: #fff;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
  align-items: center;
`;
const TableContent = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: rgb(65, 79, 90);
`;
const TableCell = styled.div`
  display: flex;
  width: 194px;
  flex: 1 0 194px;
  align-items: center;
`;
const TableRow = styled.div`
  min-height: 44px;
  margin-bottom: 4px;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  background: #f7f8f9;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
  border-radius: 4px;
  :hover {
    background-color: rgb(236, 248, 255);
  }
`;

export default BattingSummaryPanel;
