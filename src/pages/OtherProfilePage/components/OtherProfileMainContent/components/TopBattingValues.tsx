import styled from 'styled-components';
import { Line } from 'rc-progress';
import { useSelector } from 'react-redux';
import { selectOtherUserBattingValues } from '../../../../../store/otherUser/slice';

const TopBattingValues = () => {
  const battingTopValues = useSelector(selectOtherUserBattingValues);

  return (
    <Card>
      <CardTitle>Top Batting Values</CardTitle>
      <FlexContainer>
        <DataWrapper>
          <DataBody>
            <DataTitle>Exit Velocity</DataTitle>
            <DataValue>
              {battingTopValues.length
                ? battingTopValues[0].exitVelocity
                : 'N/A'}
            </DataValue>
          </DataBody>
          <ProgressBarWrapper>
            <Line
              strokeColor="#ffd01a"
              percent={
                battingTopValues.length ? battingTopValues[0].exitVelocity : 0
              }
            />
          </ProgressBarWrapper>
        </DataWrapper>
        <DataWrapper>
          <DataBody>
            <DataTitle>Carry Distance</DataTitle>
            <DataValue>
              {battingTopValues.length ? battingTopValues[0].distance : 'N/A'}
            </DataValue>
          </DataBody>
          <ProgressBarWrapper>
            <Line
              strokeColor="#ffd01a"
              percent={
                battingTopValues.length ? battingTopValues[0].distance : 0
              }
            />
          </ProgressBarWrapper>
        </DataWrapper>
        <DataWrapper>
          <DataBody>
            <DataTitle>Launch Angle</DataTitle>
            <DataValue>
              {battingTopValues.length
                ? battingTopValues[0].launchAngle
                : 'N/A'}
            </DataValue>
          </DataBody>
          <ProgressBarWrapper>
            <Line
              strokeColor="#ffd01a"
              percent={
                battingTopValues.length ? battingTopValues[0].launchAngle : 0
              }
            />
          </ProgressBarWrapper>
        </DataWrapper>
      </FlexContainer>
    </Card>
  );
};

const Card = styled.div`
  background: #fff;
  margin: 16px;
  padding: 16px;
  border-radius: 8px;
  box-sizing: border-box;
  flex-grow: 1;
  display: flex;
  max-width: 100%;
  min-width: 0;
  flex-direction: column;
`;
const CardTitle = styled.div`
  font-family: 'Lato', sans-serif;
  font-weight: 900;
  font-size: 18px;
  line-height: 1.25;
  text-align: left;
  color: #414f5a;
`;
const FlexContainer = styled.div`
  display: flex;
`;
const DataWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px 24px 0 0;
  position: relative;
`;
const DataBody = styled.div`
  width: 300px;
  height: 22px;
  display: flex;
  justify-content: space-between;
  margin-bottom: 8px;
`;
const DataTitle = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const DataValue = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  font-weight: 700;
  color: #667784;
`;
const ProgressBarWrapper = styled.div`
  position: relative;
  top: -10px;
  max-width: 100%;
  height: 4px;
`;

export default TopBattingValues;
