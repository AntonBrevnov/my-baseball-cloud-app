import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import SearchIcon from '../../../../../assets/svg/SearchIcon';
import {
  fetchProfileDataAction,
  fetchProfileNamesAction,
} from '../../../../../store/comparison/action';
import {
  selectLoadNamesStatus,
  selectLoadProfileDataStatus,
  selectProfileNames,
  selectSelectedProfileData,
} from '../../../../../store/comparison/slice';
import {
  selectOtherUserBattingValues,
  selectOtherUserData,
  selectOtherUserPitchingValues,
} from '../../../../../store/otherUser/slice';
import { selectClientData } from '../../../../../store/user/slice';
import { BlueComboBox, SmallLoaderSpinner } from '../../../../../components';
import TopTableValue from './ComparisonPanelTopTableValue';

const ComparisonPanel = () => {
  const {
    avatar,
    firstPosition,
    firstName,
    lastName,
    age,
    feet,
    inches,
    weight,
  } = useSelector(selectOtherUserData);
  const topBattingValues = useSelector(selectOtherUserBattingValues);
  const topPitchingValues = useSelector(selectOtherUserPitchingValues);
  const clientData = useSelector(selectClientData);
  const isLoadProfilesNames = useSelector(selectLoadNamesStatus);
  const isLoadProfile = useSelector(selectLoadProfileDataStatus);
  const profileNamesList = useSelector(selectProfileNames);
  const selectedProfileData = useSelector(selectSelectedProfileData);
  const dispatch = useDispatch();

  const [searchInputValue, setSearchInputValue] = useState('');
  const [searchInputOnFocus, setSearchInputFocusState] = useState(false);
  const [selectedTopValueType, setSelectedTopValueType] = useState('');

  useEffect(() => {
    dispatch(
      fetchProfileNamesAction({
        ...clientData,
        playerName: searchInputValue,
        position: firstPosition,
      }),
    );
  }, [searchInputValue]);

  const profileNamesItemsList = profileNamesList.map(value => (
    <SearchInputNamesListItem
      key={Math.round(Math.random() * 10000)}
      onClick={() => {
        setSearchInputValue(value.playerName);
        dispatch(
          fetchProfileDataAction({ ...clientData, profileId: value.id }),
        );
      }}>
      {value.playerName}
    </SearchInputNamesListItem>
  ));

  return (
    <div>
      <ComparisonHeader>
        <UserLeftTitle>
          <AvatarWrapper>
            <Avatar AvatarURL={avatar || './images/avatar.png'} />
          </AvatarWrapper>
          <UserInitials>{`${firstName} ${lastName}`}</UserInitials>
        </UserLeftTitle>
        <UserRightTitle>
          {(isLoadProfilesNames || isLoadProfile) && (
            <LoaderSpinnerWrapper>
              <SmallLoaderSpinner />
            </LoaderSpinnerWrapper>
          )}
          <AvatarWrapper>
            <Avatar
              AvatarURL={
                selectedProfileData.avatar
                  ? selectedProfileData.avatar
                  : './images/avatar.png'
              }
            />
          </AvatarWrapper>
          <SearchInputWrapper Width={searchInputOnFocus ? '180px' : '135px'}>
            <SearchInput
              placeholder="Enter player name"
              onFocus={() => setSearchInputFocusState(true)}
              onBlur={() =>
                setTimeout(() => setSearchInputFocusState(false), 300)
              }
              onChange={e => setSearchInputValue(e.currentTarget.value)}
              value={searchInputValue}
            />
            <SearchIconContainer>
              <SearchIcon />
            </SearchIconContainer>
            {profileNamesList.length && searchInputOnFocus && (
              <SearchInputNamesList>
                {profileNamesItemsList}
              </SearchInputNamesList>
            )}
          </SearchInputWrapper>
        </UserRightTitle>
      </ComparisonHeader>
      <ComparisonTableTopRow>
        <div>Age: {age || ' -'}</div>
        <div>Age: {selectedProfileData.age || ' -'}</div>
      </ComparisonTableTopRow>
      <ComparisonTableRow>
        <div>Height: {feet ? `${feet} ft ${inches} in` : ' -'}</div>
        <div>
          Height:
          {selectedProfileData.feet
            ? `${selectedProfileData.feet} ft ${selectedProfileData.inches} in`
            : ' -'}
        </div>
      </ComparisonTableRow>
      <ComparisonTableRow>
        <div>Weight: {weight ? `${weight} lbs` : ' -'}</div>
        <div>
          Weight:{' '}
          {selectedProfileData.weight
            ? `${selectedProfileData.weight} lbs`
            : ' -'}
        </div>
      </ComparisonTableRow>
      <TopValuesTableHeader>
        <BlueComboBox
          title={value =>
            firstPosition === 'pitcher'
              ? `Top Pitching Values - ${value}`
              : `Top Batting Values - ${value}`
          }
          itemsList={
            firstPosition === 'pitcher'
              ? ['Velocity', 'Spin Rate']
              : ['Distance', 'Launch Angle', 'Exit Velocity']
          }
          onChangeItem={(_, value) => setSelectedTopValueType(value || '')}
          dataConverter={value => value}
        />
      </TopValuesTableHeader>
      <TopValuesTable>
        <TopValuesTableRow>
          <div>Fastball</div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Fastball"
              topValueType={selectedTopValueType}
              topBattingValues={topBattingValues}
              topPitchingValues={topPitchingValues}
            />
          </div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Fastball"
              topValueType={selectedTopValueType}
              topBattingValues={selectedProfileData.battingTopValues}
              topPitchingValues={selectedProfileData.pitchingTopValues}
            />
          </div>
        </TopValuesTableRow>
        <TopValuesTableRow>
          <div>Curveball</div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Curveball"
              topValueType={selectedTopValueType}
              topBattingValues={topBattingValues}
              topPitchingValues={topPitchingValues}
            />
          </div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Curveball"
              topValueType={selectedTopValueType}
              topBattingValues={selectedProfileData.battingTopValues}
              topPitchingValues={selectedProfileData.pitchingTopValues}
            />
          </div>
        </TopValuesTableRow>
        <TopValuesTableRow>
          <div>Changeup</div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Changeup"
              topValueType={selectedTopValueType}
              topBattingValues={topBattingValues}
              topPitchingValues={topPitchingValues}
            />
          </div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Changeup"
              topValueType={selectedTopValueType}
              topBattingValues={selectedProfileData.battingTopValues}
              topPitchingValues={selectedProfileData.pitchingTopValues}
            />
          </div>
        </TopValuesTableRow>
        <TopValuesTableRow>
          <div>Slider</div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="ChaSliderngeup"
              topValueType={selectedTopValueType}
              topBattingValues={topBattingValues}
              topPitchingValues={topPitchingValues}
            />
          </div>
          <div>
            <TopTableValue
              isPitcher={firstPosition === 'pitcher'}
              pitchType="Slider"
              topValueType={selectedTopValueType}
              topBattingValues={selectedProfileData.battingTopValues}
              topPitchingValues={selectedProfileData.pitchingTopValues}
            />
          </div>
        </TopValuesTableRow>
      </TopValuesTable>
    </div>
  );
};

const ComparisonHeader = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;
const LoaderSpinnerWrapper = styled.div`
  position: absolute;
  left: -26px;
  top: 16px;
`;
const AvatarWrapper = styled.div`
  display: block;
  flex: 0 0 40px;
  width: 40px;
  height: 40px;
  margin-right: 8px;
  overflow: hidden;
  border-radius: 50%;
`;
const Avatar = styled.div<{ AvatarURL: string }>`
  background-image: url(${props => props.AvatarURL});
  width: 40px;
  height: 40px;
  background-size: cover;
  background-position: 50% 50%;
`;
const UserLeftTitle = styled.div`
  display: flex;
`;
const UserRightTitle = styled.div`
  position: relative;
  display: flex;
  overflow: visible !important;
`;
const UserInitials = styled.div`
  display: flex;
  height: 40px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 19px;
  color: #414f5a;
  align-items: flex-start;
`;
const SearchInputWrapper = styled.div<{ Width: string }>`
  position: relative;
  display: flex;
  width: ${props => props.Width};
  transition: width 0.5s;
`;
const SearchInput = styled.input`
  outline: none;
  display: block;
  width: 135px;
  padding: 5px 5px 7px 0;
  font-size: 16px;
  line-height: 19px;
  min-height: 26px;
  font-weight: 400;
  color: #48bbff;
  border: 0px solid transparent;
  transition: width 0.5s;
  ::placeholder {
    color: #48bbff;
  }
  :focus {
    width: 180px;
    border-bottom: 1px solid #48bbff;
    color: #788b99;
    ::placeholder {
      color: #788b99;
    }
  }
`;
const SearchIconContainer = styled.div`
  border: 0px solid transparent;
  background-color: transparent;
  outline: none;
  padding: 0;
  display: flex;
  align-items: center;
`;
const SearchInputNamesList = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  width: 100%;
  top: 100%;
  right: 0;
  margin-top: 12px;
  padding: 8px 0;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 0 3px 8px 0 rgb(0 0 0 / 15%);
  border: solid 1px #ebebeb;
  z-index: 100;
`;
const SearchInputNamesListItem = styled.a`
  padding: 8px 16px;
  background: #fff;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 1;
  font-weight: 400;
  cursor: pointer;
  background-color: #fff;
  text-decoration: none;
  color: #788b99;
  :hover {
    background-color: rgba(72, 187, 255, 0.1);
  }
`;
const ComparisonTableTopRow = styled.div`
  margin-top: 15px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  min-height: 44px;
  margin-bottom: 6px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: rgb(51, 51, 51);
`;
const ComparisonTableRow = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  min-height: 44px;
  margin-bottom: 6px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: rgb(51, 51, 51);
`;
const TopValuesTableHeader = styled.div`
  width: 25%;
  margin-bottom: 21px;
`;
const TopValuesTable = styled.div`
  display: flex;
  flex-direction: column;
`;
const TopValuesTableRow = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  font-family: 'Lato', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 1;
  color: #414f5a;
  margin-bottom: 4px;
  min-height: 44px;
`;

export default ComparisonPanel;
