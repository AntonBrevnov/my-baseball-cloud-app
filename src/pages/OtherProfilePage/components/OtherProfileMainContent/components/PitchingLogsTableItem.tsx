import { useState } from 'react';
import styled from 'styled-components';
import { IPitchingLog } from '../../../../../store/commonTypes';

const PitchingLogsTableItem = ({
  date,
  batterName,
  pitchType,
  pitchCall,
  velocity,
  spinRate,
  spinAxis,
  verticalBreak,
  horizontalBreak,
  heightAtPlate,
  releaseHeight,
  extension,
  releaseSide,
  tilt,
}: IPitchingLog) => {
  const [isVisibleAdditionData, setVisibleAdditionData] = useState(false);

  return (
    <TableRow
      onClick={() => setVisibleAdditionData(!isVisibleAdditionData)}
      isOpened={isVisibleAdditionData}>
      <ItemRow>
        <TableCell1>{date || '-'}</TableCell1>
        <TableCell2>{batterName || '-'}</TableCell2>
        <TableCell2>{pitchType || '-'}</TableCell2>
        <TableCell2>{pitchCall || '-'}</TableCell2>
        <TableCell3>{velocity || '-'}</TableCell3>
        <TableCell3>{spinRate || '-'}</TableCell3>
        <TableCell4>{spinAxis || '-'}</TableCell4>
      </ItemRow>
      {isVisibleAdditionData && (<>
      <SecondContentHeader>
        <TableCell1>Vertical Break</TableCell1>
        <TableCell2>Horizontal Break</TableCell2>
        <TableCell2>Height at Plate</TableCell2>
        <TableCell2>Release Height</TableCell2>
        <TableCell3>Extension</TableCell3>
        <TableCell3>Release Side</TableCell3>
        <TableCell4>Tilt</TableCell4>
      </SecondContentHeader>
      <ItemRow>
        <TableCell1>{verticalBreak || '-'}</TableCell1>
        <TableCell2>{horizontalBreak || '-'}</TableCell2>
        <TableCell2>{heightAtPlate || '-'}</TableCell2>
        <TableCell2>{releaseHeight || '-'}</TableCell2>
        <TableCell3>{extension || '-'}</TableCell3>
        <TableCell3>{releaseSide || '-'}</TableCell3>
        <TableCell4>{tilt || '-'}</TableCell4>
      </ItemRow></>)}
    </TableRow>
  );
};

const TableRow = styled.div<{ isOpened: boolean }>`
  display: block;
  border-radius: 4px;
  margin-bottom: 6px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: rgb(65, 79, 90);
  ${props => props.isOpened && 'box-shadow: 0 0 6px 1px rgb(72 187 255 / 63%);'}
`;
const ItemRow = styled.div`
  background-color: #f7f8f9;
  min-height: 44px;
  cursor: pointer;
  display: flex;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  :hover {
    background-color: #ecf8ff;
  }
`;
const SecondContentHeader = styled.div`
  display: flex;
  background: #fff;
  min-height: 44px;
  margin-bottom: 6px;
  width: 100%;
  flex: 0 0 100%;
  align-items: center;
  border-radius: 4px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 1;
  font-weight: 300;
  color: #667784;
`;
const TableCell1 = styled.div`
  display: flex;
  width: 12.1%;
  flex: 1 0 12.1%;
`;
const TableCell2 = styled.div`
  display: flex;
  width: 18.5%;
  flex: 1 0 18.5%;
`;
const TableCell3 = styled.div`
  display: flex;
  width: 11.4%;
  flex: 1 0 11.4%;
`;
const TableCell4 = styled.div`
  display: flex;
  width: 8%;
  flex: 1 0 8%;
`;

export default PitchingLogsTableItem;
