import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import {
  fetchBattingSummaryAction,
  fetchPitchingSummaryAction,
} from '../../../../store/otherUser/actions';
import { selectOtherUserData } from '../../../../store/otherUser/slice';
import { selectClientData } from '../../../../store/user/slice';
import BattingChartsPanel from './components/BattingChartsPanel';
import BattingLogsPanel from './components/BattingLogsPanel';
import BattingSummaryPanel from './components/BattingSummaryPanel';
import ComparisonPanel from './components/ComparisonPanel';
import PitchingChartsPanel from './components/PitchingChartsPanel';
import PitchingLogsPanel from './components/PitchingLogsPanel';
import PitchingSummaryPanel from './components/PitchingSummaryPanel';
import TopBattingValues from './components/TopBattingValues';
import TopPitchingValues from './components/TopPitchingValues';

const OtherProfileMainContent = () => {
  const clientData = useSelector(selectClientData);
  const { id, firstPosition } = useSelector(selectOtherUserData);
  const [selectedTabIndex, setSelectedTabIndex] = useState(
    firstPosition === 'pitcher' ? 0 : 1,
  );
  const [isShowingPitching, setShowingPitchingState] = useState(false);
  const [selectedPitchingIndex, setSelectedPitchingIndex] = useState(0);
  const [isShowingBatting, setShowingBattingState] = useState(false);
  const [selectedBattingIndex, setSelectedBattingIndex] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    if (selectedTabIndex === 0 && selectedPitchingIndex === 0) {
      dispatch(
        fetchPitchingSummaryAction({
          ...clientData,
          profileId: id,
        }),
      );
    }
    if (selectedTabIndex === 1 && selectedBattingIndex === 0) {
      dispatch(
        fetchBattingSummaryAction({
          ...clientData,
          profileId: id,
        }),
      );
    }
  }, [selectedTabIndex, selectedPitchingIndex, selectedBattingIndex]);

  const handlePitchingDropDownItemClick = (index: number) => {
    setSelectedPitchingIndex(index);
    setShowingPitchingState(false);
  };
  const handleBattingDropDownItemClick = (index: number) => {
    setSelectedBattingIndex(index);
    setShowingBattingState(false);
  };

  return (
    <MainContainer>
      <Header>
        {firstPosition === 'pitcher' && <TopPitchingValues />}
        <TopBattingValues />
      </Header>
      <Content>
        <TabsList>
          {firstPosition === 'pitcher' && (
            <Tab
              isSelected={selectedTabIndex === 0}
              onClick={() => setSelectedTabIndex(0)}
              onMouseEnter={() => setShowingPitchingState(true)}>
              Pitching
              {isShowingPitching && (
                <DropDownList
                  onMouseLeave={() => setShowingPitchingState(false)}>
                  <DropDownText
                    onClick={() => handlePitchingDropDownItemClick(0)}>
                    Summary
                  </DropDownText>
                  <DropDownText
                    onClick={() => handlePitchingDropDownItemClick(1)}>
                    Charts
                  </DropDownText>
                  <DropDownText
                    onClick={() => handlePitchingDropDownItemClick(2)}>
                    Log
                  </DropDownText>
                </DropDownList>
              )}
            </Tab>
          )}
          <Tab
            isSelected={selectedTabIndex === 1}
            onClick={() => setSelectedTabIndex(1)}
            onMouseEnter={() => setShowingBattingState(true)}>
            Batting
            {isShowingBatting && (
              <DropDownList onMouseLeave={() => setShowingBattingState(false)}>
                <DropDownText onClick={() => handleBattingDropDownItemClick(0)}>
                  Summary
                </DropDownText>
                <DropDownText onClick={() => handleBattingDropDownItemClick(1)}>
                  Charts
                </DropDownText>
                <DropDownText onClick={() => handleBattingDropDownItemClick(2)}>
                  Log
                </DropDownText>
              </DropDownList>
            )}
          </Tab>
          <Tab
            isSelected={selectedTabIndex === 2}
            onClick={() => setSelectedTabIndex(2)}>
            Comparison
          </Tab>
        </TabsList>
        {selectedTabIndex === 0 && selectedPitchingIndex === 0 && (
          <PitchingSummaryPanel />
        )}
        {selectedTabIndex === 0 && selectedPitchingIndex === 1 && (
          <PitchingChartsPanel />
        )}
        {selectedTabIndex === 0 && selectedPitchingIndex === 2 && (
          <PitchingLogsPanel />
        )}
        {selectedTabIndex === 1 && selectedBattingIndex === 0 && (
          <BattingSummaryPanel />
        )}
        {selectedTabIndex === 1 && selectedBattingIndex === 1 && (
          <BattingChartsPanel />
        )}
        {selectedTabIndex === 1 && selectedBattingIndex === 2 && (
          <BattingLogsPanel />
        )}
        {selectedTabIndex === 2 && <ComparisonPanel />}
      </Content>
    </MainContainer>
  );
};

const MainContainer = styled.main`
  background: #788b99;
  flex: 2;
  overflow-y: scroll;
  overflow-x: hidden;
  width: calc(100vw - 220px);
`;
const Header = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
const Content = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  margin: 16px;
  padding: 16px;
  border-radius: 8px;
  box-sizing: border-box;
  flex-grow: 1;
`;
const TabsList = styled.ul`
  margin: 0;
  padding: 0;
  box-shadow: unset;
  display: flex;
  justify-content: flex-start;
`;
const Tab = styled.li<{ isSelected: boolean }>`
  position: relative;
  list-style: none;
  padding: 8px;
  margin: 8px;
  border: 2px solid #788b99;
  border-radius: 40px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  line-height: 17px;
  font-weight: 700;
  background-color: ${props => (props.isSelected ? '#788b99' : '#fff')};
  color: ${props => (props.isSelected ? '#fff' : '#667784')};
  cursor: pointer;
  :hover {
    background-color: rgba(120, 139, 153, 0.4);
  }
`;
const DropDownList = styled.div`
  display: block;
  width: 178px;
  position: absolute;
  top: auto;
  left: -15px;
  margin-top: 12px;
  padding: 8px 0;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 0 3px 8px 0 rgb(0 0 0 / 15%);
  border: solid 1px #ebebeb;
  z-index: 100;
`;
const DropDownText = styled.a`
  font-family: 'Lato', sans-serif;
  display: block;
  padding: 8px 16px;
  line-height: 1;
  color: #788b99;
  font-size: 16px;
  font-weight: 400;
  cursor: pointer;
  background-color: #fff;
  :hover {
    background-color: '#eff1f3';
  }
`;

export default OtherProfileMainContent;
