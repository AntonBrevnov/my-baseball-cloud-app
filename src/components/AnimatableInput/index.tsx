import { useRef, useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';
import ComboBoxMark from '../../assets/svg/ComboBoxMark';

type InputProps = {
  placeHolder: string;
  minWidth: number;
  maxWidth: number;
  submitForm: () => void;
  renderProps: FieldRenderProps<string>;
};

const AnimatableInput = ({
  placeHolder,
  submitForm,
  minWidth,
  maxWidth,
  renderProps,
}: InputProps) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [inputIsFocused, setFocusState] = useState(false);
  const { input } = renderProps;

  return (
    <InputWrapper
      inputIsFocused={inputIsFocused}
      minWidth={minWidth}
      maxWidth={maxWidth}>
      <Input
        minWidth={minWidth}
        maxWidth={maxWidth}
        {...input}
        type="text"
        placeholder={placeHolder}
        onFocus={() => setFocusState(true)}
        onBlur={() => setFocusState(false)}
        onChange={e => {
          input.onChange(e.currentTarget.value);
          submitForm();
        }}
        ref={inputRef}
      />
      <MarkerWrapper onClick={() => inputRef.current?.focus()}>
        <ComboBoxMark isOpened={inputIsFocused} />
      </MarkerWrapper>
    </InputWrapper>
  );
};

const InputWrapper = styled.div<{
  inputIsFocused: boolean;
  minWidth: number;
  maxWidth: number;
}>`
  position: relative;
  transition: width 0.5s;
  width: ${props =>
    props.inputIsFocused ? `${props.maxWidth}px` : `${props.minWidth}px`};
`;
const Input = styled.input<{ minWidth: number; maxWidth: number }>`
  outline: none;
  display: block;
  width: ${props => props.minWidth}px;
  padding: 5px 5px 7px 0;
  font-size: 16px;
  line-height: 19px;
  min-height: 26px;
  font-weight: 400;
  color: #48bbff;
  border: 0px solid transparent;
  transition: width 0.5s;
  ::placeholder {
    color: #48bbff;
  }
  :focus {
    width: ${props => props.maxWidth}px;
    border-bottom: 1px solid #48bbff;
    color: #788b99;
    ::placeholder {
      color: #788b99;
    }
  }
`;
const MarkerWrapper = styled.div`
  position: absolute;
  top: 23%;
  left: auto;
  right: 0;
`;

export default AnimatableInput;
