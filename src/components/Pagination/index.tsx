import { useEffect } from 'react';
import styled from 'styled-components';
import paginationItemsGenerator from '../../utils/paginationItemsGenerator';

type PaginationProps = {
  isFixed: boolean;
  currentPageIndex: number;
  maxPagesCount: number;
  onChangePageIndex: (index: number) => void;
};

// eslint-disable-next-line
const Pagination = ({
  isFixed,
  currentPageIndex,
  maxPagesCount,
  onChangePageIndex,
}: PaginationProps) => {
  useEffect(() => {
    if (currentPageIndex > maxPagesCount) {
      onChangePageIndex(maxPagesCount);
    }
  }, [maxPagesCount, currentPageIndex]);

  const handleClickPaginationItem = (pageIndex: number) => {
    if (pageIndex !== currentPageIndex) {
      onChangePageIndex(pageIndex);
    }
  };

  const paginationItems = paginationItemsGenerator(
    currentPageIndex,
    maxPagesCount,
  );
  const paginationPanelItems = paginationItems.map(({ index, isSelected }) =>
    index !== -1 ? (
      <PaginationItem
        key={Math.round(Math.random() * 100000)}
        isSelected={isSelected}
        onClick={() => handleClickPaginationItem(index)}>
        {index + 1}
      </PaginationItem>
    ) : (
      <DisabledPaginationItem key={Math.round(Math.random() * 100000)}>
        ...
      </DisabledPaginationItem>
    ),
  );

  return (
    <PaginationPanel isFixed={isFixed}>
      <PaginationLeftControl
        isActive={currentPageIndex > 0}
        onClick={() => onChangePageIndex(0)}>
        «
      </PaginationLeftControl>
      {paginationPanelItems}
      <PaginationRightControl
        isActive={currentPageIndex < maxPagesCount}
        onClick={() => onChangePageIndex(maxPagesCount)}>
        »
      </PaginationRightControl>
    </PaginationPanel>
  );
};

const PaginationPanel = styled.div<{ isFixed: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin: 16px 0;
  ${props => props.isFixed && 'position: sticky; bottom: 0;'}
  align-self: flex-end;
  padding-left: 0;
  border: 0;
  border-radius: 4px;
`;
const PaginationItem = styled.div<{ isSelected: boolean }>`
  position: relative;
  float: left;
  padding: 6px 12px;
  font-family: 'Lato', sans-serif;
  line-height: 1.42857143;
  border: none;
  border-radius: 4px;
  margin: 0 2px;
  cursor: ${props => (props.isSelected ? 'default' : 'pointer')};
  color: ${props => (props.isSelected ? '#fff' : '#414f5a')};
  background-color: ${props => (props.isSelected ? '#48bbff;' : '#f7f8f9')};
  :hover {
    ${props =>
      props.isSelected
        ? ''
        : 'color: #23527c; background-color: #eee; border-color: #ddd;'}
  }
`;
const DisabledPaginationItem = styled.div`
  position: relative;
  float: left;
  padding: 6px 12px;
  font-family: 'Lato', sans-serif;
  line-height: 1.42857143;
  border: none;
  border-radius: 4px;
  margin: 0 2px;
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
`;
const PaginationLeftControl = styled.div<{ isActive: boolean }>`
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  margin: 0 2px;
  border-radius: 4px;
  padding: 6px 12px;
  position: relative;
  float: left;
  ${props =>
    !props.isActive
      ? `
      color: #777; 
      cursor: not-allowed; 
      background-color: #fff; 
      border-color: #ddd;`
      : `
      color: #414f5a;
      cursor: pointer; 
      border: none;
      background-color: #f7f8f9;
      `}
`;
const PaginationRightControl = styled.div<{ isActive: boolean }>`
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  margin: 0 2px;
  border-radius: 4px;
  padding: 6px 12px;
  position: relative;
  float: left;
  ${props =>
    !props.isActive
      ? `
      color: #777; 
      cursor: not-allowed; 
      background-color: #fff; 
      border-color: #ddd;`
      : `
      color: #414f5a;
      cursor: pointer; 
      border: none;
      background-color: #f7f8f9;
      `}
`;

export default Pagination;
