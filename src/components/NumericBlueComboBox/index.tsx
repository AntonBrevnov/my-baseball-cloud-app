import { useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';
import ComboBoxMark from '../../assets/svg/ComboBoxMark';

type ComboBoxProps = {
  title: (value: number) => string;
  initialItemIndex?: number;
  itemsList: number[];
  onChangeItem?: (index?: number, value?: number) => void;
  dataConverter: (value: number) => string;
  renderProps?: FieldRenderProps<number>;
};

const ComboBox = ({
  title,
  initialItemIndex,
  itemsList,
  onChangeItem,
  dataConverter,
  renderProps,
}: ComboBoxProps) => {
  const [isShowingList, setShowingListState] = useState(false);
  const [selectedItemValue, setSelectedItemValue] = useState(
    itemsList[initialItemIndex || 0],
  );

  const list = itemsList.map((value, index) => (
    <ListItem
      key={Math.round(Math.random() * 10000)}
      onClick={() => {
        setShowingListState(false);
        setSelectedItemValue(value);
        if (onChangeItem) {
          if (renderProps) {
            renderProps.input.onChange(value);
          }
          onChangeItem(index, value);
        }
      }}>
      <ListItemText>{dataConverter(value)}</ListItemText>
    </ListItem>
  ));

  return (
    <ComboBoxContainer>
      {renderProps && <input {...renderProps.input} type="hidden" />}
      <ComboBoxHeader onClick={() => setShowingListState(!isShowingList)}>
        <ComboBoxHeaderText>{title(selectedItemValue)}</ComboBoxHeaderText>
        <MarkerWrapper>
          <ComboBoxMark isOpened={isShowingList} />
        </MarkerWrapper>
      </ComboBoxHeader>
      {isShowingList && <ItemsList>{list}</ItemsList>}
    </ComboBoxContainer>
  );
};

const ComboBoxContainer = styled.div`
  position: relative;
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
`;
const ComboBoxHeader = styled.div`
  position: relative;
  height: 38px;
  border-radius: 4px;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  background-color: '#fff';
  display: flex;
  align-items: center;
`;
const ComboBoxHeaderText = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #48bbff;s
`;
const ItemsList = styled.div`
  position: absolute;
  z-index: 100;
  right: -25px;
  left: inherit;
  width: 178px;
  top: 100%;
  border-radius: 5px;
  background-color: #ffffff;
  padding-top: 6px;
  padding-bottom: 6px;
  box-shadow: 0 3px 8px 0 rgb(0 0 0 / 15%);
`;
const ListItem = styled.div`
  width: 100%;
  height: 38px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  display: flex;
  align-items: center;
  color: #667784;
  background-color: #fff;
  :hover {
    background-color: #def;
  }
`;
const ListItemText = styled.div`
  padding: 0px 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const MarkerWrapper = styled.div`
  position: relative;
`;

export default ComboBox;
