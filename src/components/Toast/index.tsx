import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import SuccessMark from '../../assets/svg/SuccessMark';
import { hideToast } from '../../store/toast/slice';
import { IToastData } from '../../store/toast/types';

const Toast = ({ title, message }: IToastData) => {
  const dispatch = useDispatch();
  setTimeout(() => dispatch(hideToast()), 2000);

  return (
    <ToastContainer>
      <Mark>
        <SuccessMark />
      </Mark>
      <DataContainer>
        <Title>{title}</Title>
        <div>{message}</div>
      </DataContainer>
      <CloseCross onClick={() => dispatch(hideToast())}>✕</CloseCross>
      <ProgressBar />
    </ToastContainer>
  );
};

const ToastContainer = styled.div`
  position: absolute;
  width: 300px;
  top: 15px;
  left: auto;
  right: 15px;
  z-index: 10;
  border-radius: 10px;
  background-color: #60bb71;
  padding: 10px 5px;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 1em;
  color: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  overflow: hidden;
  animation-name: ascent;
  animation-duration: .4s;
  @keyframes ascent {
    0% {
      opacity: 0;
      transform: scale(1);
    }
    25% {
      opacity: .25;
      transform: scale(1.15);
    }
    50% {
      opacity: .5;
      transform: scale(.85);
    }
    75% {
      opacity: .75;
      transform: scale(1.15);
    }
    100% {
      opacity: 1;
      transform: scale(1);
    }
  }
`;
const Mark = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 70px;
  height: 70px;
`;
const DataContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 65%;
  cursor: default;
`;
const Title = styled.div`
  margin-bottom: 5px;
`;
const CloseCross = styled.div`
  width: 5%;
  display: flex;
  cursor: pointer;
`;
const ProgressBar = styled.div`
  position: absolute;
  top: 95%;
  left: -5px;
  width: 100%;
  height: 5%;
  background-color: #305837;
  animation-name: timingProgressBar;
  animation-duration: 2s;
  @keyframes timingProgressBar {
    from {
      width: 100%;
    }
    to {
      width: 0%;
    }
  }
`;

export default Toast;
