import styled from 'styled-components';

const LoaderSpinner = () => (
  <BouncesWrapper>
    <Bounce1 className="small-bounce bounce__first" />
    <Bounce2 className="small-bounce bounce__second" />
    <Bounce3 className="small-bounce bounce__third" />
  </BouncesWrapper>
);

const BouncesWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Bounce1 = styled.div`
  @keyframes resizing1 {
    0% {
      transform: scale(0);
    }
    20% {
      transform: scale(1);
    }
    40% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
  }
`;
const Bounce2 = styled.div`
  @keyframes resizing2 {
    0% {
      transform: scale(0);
    }
    10% {
      transform: scale(0);
    }
    30% {
      transform: scale(1);
    }
    50% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
  }
`;
const Bounce3 = styled.div`
  @keyframes resizing3 {
    0% {
      transform: scale(0);
    }
    20% {
      transform: scale(0);
    }
    40% {
      transform: scale(1);
    }
    60% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
  }
`;

export default LoaderSpinner;
