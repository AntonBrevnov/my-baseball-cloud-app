import styled from 'styled-components';
import SearchIcon from '../../assets/svg/SearchIcon';

type InputProps = {
  width: string;
  placeholder: string;
  onChange: (value: string) => void;
};

const SearchInput = ({ width, placeholder, onChange }: InputProps) => (
  <InputWrapper>
    <SearchButton>
      <SearchIcon />
    </SearchButton>
    <Input
      inputWidth={width}
      placeholder={placeholder}
      onChange={e => onChange(e.currentTarget.value)}
    />
  </InputWrapper>
);

const InputWrapper = styled.div`
  display: flex;
  position: relative;
`;
const SearchButton = styled.button`
  cursor: pointer;
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  padding: 0;
  background-color: transparent;
  border-color: transparent;
  outline: none;
`;
const Input = styled.input<{ inputWidth: string }>`
  width: ${props => props.inputWidth};
  padding: 5px 5px 5px 24px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 19px;
  font-weight: 400;
  color: #788b99;
  border: 0px solid transparent;
  outline: none;
  border-bottom: 1px solid #48bbff;
  ::placeholder {
    color: #cdcdcd;
  }
`;

export default SearchInput;
