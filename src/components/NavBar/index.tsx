import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { MainLogo } from '../../assets/svg';
import DropDownIcon from '../../assets/svg/DropDownIcon';
import { signOutAction } from '../../store/user/actions';
import { selectClientData, selectUserData } from '../../store/user/slice';

type NavBarProps = {
  isAuthorized: boolean;
};

const NavBar = ({ isAuthorized }: NavBarProps) => {
  const clientData = useSelector(selectClientData);
  const { avatar, firstName, lastName } = useSelector(selectUserData);
  const [isShowingDropDownPanel, setShowingDropDownPanel] = useState(false);
  const dispatch = useDispatch();

  if (isAuthorized) {
    return (
      <NavBarForAuthorized>
        <Link to="/profile">
          <MainLogo />
        </Link>
        <NavBarContentBody>
          <StyledLink href="/leaderboard">
            Leaderboard
            <LinkUnderLine
              isSelected={
                window.location.href.substring(
                  window.location.href.length - 11,
                ) === 'leaderboard'
              }
            />
          </StyledLink>
          <StyledLink href="/network">
            Network
            <LinkUnderLine
              isSelected={
                window.location.href.substring(
                  window.location.href.length - 7,
                ) === 'network'
              }
            />
          </StyledLink>
          <DropDownContainer>
            <DropDownTop>
              <AvatarContainer>
                <a href="/profile">
                  <Avatar AvatarUrl={avatar} />
                </a>
              </AvatarContainer>
              <DropDownTitle
                onClick={() =>
                  setShowingDropDownPanel(!isShowingDropDownPanel)
                }>
                {`${firstName} ${lastName}`}
                <IconContainer>
                  <DropDownIcon />
                </IconContainer>
              </DropDownTitle>
            </DropDownTop>
            <DropDownPanel isVisible={isShowingDropDownPanel}>
              <DropDownPanelText href="/profile">My Profile</DropDownPanelText>
              <DropDownPanelText
                onClick={() => dispatch(signOutAction(clientData))}>
                Log Out
              </DropDownPanelText>
            </DropDownPanel>
          </DropDownContainer>
        </NavBarContentBody>
      </NavBarForAuthorized>
    );
  }
  return (
    <NavBarContainer>
      <Link to="/login">
        <MainLogo />
      </Link>
    </NavBarContainer>
  );
};

const NavBarForAuthorized = styled.header`
  z-index: 10;
  width: 100%;
  height: 39px;
  padding: 8px;
  padding-bottom: 5px;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  grid-area: hd;
  border-bottom: 1px solid rgba(120, 139, 153, 0.5);
`;
const NavBarContainer = styled.header`
  z-index: 10;
  width: 100%;
  padding: 8px;
  padding-bottom: 5px;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  grid-area: hd;
`;
const NavBarContentBody = styled.div`
  display: flex;
  flex-direction: row;
  grid-area: hd;
`;
const StyledLink = styled.a`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  padding: 6px 8px;
  color: #788b99;
  text-decoration: none;
  position: relative;
`;
const LinkUnderLine = styled.div<{ isSelected: boolean }>`
  position: absolute;
  top: 3px;
  border-bottom: 4px solid
    ${props => (props.isSelected ? '#788b99' : 'rgba(0, 0, 0, 0)')};
  width: 85%;
  height: 100%;
  :hover {
    border-bottom: 4px solid rgba(120, 139, 153, 0.4);
  }
`;
const DropDownContainer = styled.div`
  margin-left: 16px;
  margin-right: 16px;
  position: relative;
`;
const DropDownTop = styled.div`
  display: flex;
`;
const AvatarContainer = styled.div`
  display: block;
  flex: 0 0 32px;
  width: 32px;
  height: 32px;
  overflow: hidden;
  border-radius: 50%;
`;
const Avatar = styled.div<{ AvatarUrl: string }>`
  background-image: url(${props => props.AvatarUrl});
  width: 32px;
  height: 32px;
  background-size: cover;
  background-position: 50% 50%;
`;
const DropDownTitle = styled.button`
  padding: 7px 19px 10px 18px;
  border-radius: 4px;
  box-shadow: none;
  font-size: 16px;
  line-height: 19px;
  font-weight: 400;
  display: flex;
  align-items: center;
  background-color: transparent;
  border-style: none;
  font-family: 'Lato', sans-serif;
  cursor: pointer;
  font-size: 16px;
  color: #788b99;
  margin: 0;
  :hover {
    background-color: rgb(239, 239, 239);
  }
`;
const DropDownPanel = styled.div<{ isVisible: boolean }>`
  display: ${props => (props.isVisible ? 'block' : 'none')};
  width: 178px;
  position: absolute;
  top: 100%;
  margin-top: 12px;
  padding: 8px 0;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 0 3px 8px 0 rgb(0 0 0 / 15%);
  border: solid 1px #ebebeb;
  z-index: 100;
  right: -5px;
`;
const DropDownPanelText = styled.a`
  display: block;
  text-decoration: none;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  font-weight: 400;
  line-height: 1;
  color: #788b99;
  padding: 8px 16px;
  background: #fff;
  cursor: pointer;
`;
const IconContainer = styled.span`
  display: flex;
  margin-left: 6px;
`;

export default NavBar;
