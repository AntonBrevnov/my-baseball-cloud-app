interface ComboBoxMarkProps {
  isOpened: boolean;
}

function ComboBoxMark({ isOpened }: ComboBoxMarkProps) {
  if (isOpened) {
    return (
      <svg
        className="combo-box__combo-box-mark"
        width={16}
        height={9}
        viewBox="0 0 16 9"
        xmlns="http://www.w3.org/2000/svg">
        <path
          stroke="#48bbff"
          strokeLinecap="round"
          strokeWidth={2}
          d="M1 8l7-7M8 1l7 7"
        />
      </svg>
    );
  }
  return (
    <svg
      className="combo-box__combo-box-mark"
      width={16}
      height={9}
      viewBox="0 0 16 9"
      xmlns="http://www.w3.org/2000/svg">
      <path
        stroke="#48bbff"
        strokeLinecap="round"
        strokeWidth={2}
        d="M1 1l7 7M15 1L8 8"
      />
    </svg>
  );
}

export default ComboBoxMark;
