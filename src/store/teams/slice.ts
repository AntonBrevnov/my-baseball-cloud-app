import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { ITeam } from '../commonTypes';

const initialState = {
  isLoadTeamsList: false,
  teamsList: [] as ITeam[],
};

const teamsSlice = createSlice({
  name: 'teams',
  initialState,
  reducers: {
    setLoadTeamsStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadTeamsList = action.payload;
    },
    fillTeamsList: (state, action: PayloadAction<ITeam[]>) => {
      state.teamsList = action.payload;
      state.isLoadTeamsList = false;
    },
  },
});

export const teamsReducer = teamsSlice.reducer;
export const { setLoadTeamsStatus, fillTeamsList } = teamsSlice.actions;

export const selectTeamsLoadStatus = createSelector(
  (state: RootState) => state.teams,
  data => data.isLoadTeamsList,
);
export const selectTeamsList = createSelector(
  (state: RootState) => state.teams,
  data => data.teamsList,
);
