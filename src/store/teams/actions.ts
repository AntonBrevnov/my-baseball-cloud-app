import { createAction } from '@reduxjs/toolkit';
import { ISearchable } from '../commonTypes';

export const fetchTeamsListAction =
  createAction<ISearchable>('teams/FETCH_LIST');
