import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import { ISearchable, ITeam } from '../commonTypes';
import { fetchTeamsListAction } from './actions';
import { fillTeamsList, setLoadTeamsStatus } from './slice';
import * as TeamsApi from '../../api/TeamApi';

function* fetchTeamsListSaga(
  action: PayloadAction<ISearchable>,
): Generator<StrictEffect, void, ITeam[]> {
  yield put(setLoadTeamsStatus(true));
  const teams = yield call(TeamsApi.fetchTeamsList, action.payload);
  yield put(fillTeamsList(teams));
}
export function* watchFetchTeamsList() {
  yield takeEvery(fetchTeamsListAction, fetchTeamsListSaga);
}
