import { createAction } from '@reduxjs/toolkit';
import { IUploadGraph, IUploadLogs, IUploadProfileData } from '../commonTypes';
import { IUpdateOtherUsersFavoriteStatusProps } from './types';

export const fetchOtherUserDataAction = createAction<IUploadProfileData>(
  'otherUser/FETCH_DATA',
);
export const updateOtherUserFavoriteStatusAction =
  createAction<IUpdateOtherUsersFavoriteStatusProps>(
    'otherUser/UPDATE_FAVORITE',
  );

export const fetchPitchingSummaryAction = createAction<IUploadProfileData>(
  'otherUser/FETCH_PITCHING_SUMMARY',
);
export const fetchPitchingGraphAction = createAction<IUploadGraph>(
  'otherUser/FETCH_PITCHING_GRAPH',
);
export const fetchPitchingLogsAction = createAction<IUploadLogs>(
  'otherUser/FETCH_PITCHING_LOGS',
);

export const fetchBattingSummaryAction = createAction<IUploadProfileData>(
  'otherUser/FETCH_BATTING_SUMMARY',
);
export const fetchBattingGraphAction = createAction<IUploadGraph>(
  'otherUser/FETCH_BATTING_GRAPH',
);
export const fetchBattingLogsAction = createAction<IUploadLogs>(
  'otherUser/FETCH_BATTING_LOGS',
);
