import { IClientData, IFacility, ISchool, ITeam } from '../commonTypes';

export interface IUpdateOtherUsersFavoriteStatusProps extends IClientData {
  id: number;
  isFavorite: boolean;
}

export interface IOtherUser {
  id: number;
  firstName: string;
  lastName: string;
  avatar: string;
  firstPosition: string;
  secondPosition: string;
  school: ISchool;
  schoolYear: string;
  biography: string;
  teams: ITeam[];
  facilities: IFacility[];
  isFavorite: boolean;
  weight: number;
  feet: number;
  inches: number;
  age: number;
  batsHand: string;
  throwsHand: string;
}
