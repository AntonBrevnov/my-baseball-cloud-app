import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import * as UserApi from '../../api/UserApi';
import { updateNetworkFavoriteStatus } from '../../api/NetworkApi';
import {
  fetchBattingGraphAction,
  fetchBattingLogsAction,
  fetchBattingSummaryAction,
  fetchOtherUserDataAction,
  fetchPitchingGraphAction,
  fetchPitchingLogsAction,
  fetchPitchingSummaryAction,
  updateOtherUserFavoriteStatusAction,
} from './actions';
import {
  fillBattingGraph,
  fillBattingLogs,
  fillBattingSummary,
  fillOtherUserData,
  fillPitchingGraph,
  fillPitchingLogs,
  fillPitchingSummary,
  fillTopBattingValues,
  fillTopPitchingValues,
  setFavoriteStatus,
  setLoadOtherProfileStatus,
  setLoadOtherUserLogsStatus,
} from './slice';
import { IOtherUser, IUpdateOtherUsersFavoriteStatusProps } from './types';
import {
  IBattingLog,
  IBattingSummary,
  IBattingTopValue,
  IFacility,
  IPitchingLog,
  IPitchingSummary,
  IPitchingTopValue,
  IUploadGraph,
  IUploadLogs,
  IUploadProfileData,
} from '../commonTypes';
import { showToast } from '../toast/slice';

function* fetchOtherUserDataSaga(
  action: PayloadAction<IUploadProfileData>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadOtherProfileStatus(true));
  const profile = yield call(UserApi.fetchAllUserData, action.payload);
  const facilities = profile.facilities.map(
    (value: any) =>
      ({
        id: value.id,
        email: value.email,
        name: value.u_name,
      } as IFacility),
  );
  const battingTopValues = profile.batting_top_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        distance: value.distance,
        launchAngle: value.launch_angle,
        exitVelocity: value.exit_velocity,
      } as IBattingTopValue),
  );
  const pitchingTopValues = profile.pitching_top_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchMovement: value.pitch_movement,
      } as IPitchingTopValue),
  );

  const otherUser = {
    id: profile.id,
    firstName: profile.first_name,
    lastName: profile.last_name,
    avatar: profile.avatar,
    age: profile.age,
    batsHand: profile.bats_hand,
    throwsHand: profile.throws_hand,
    teams: profile.teams,
    facilities,
    isFavorite: profile.favorite,
    school: profile.school,
    schoolYear: profile.school_year,
    feet: profile.feet,
    inches: profile.inches,
    weight: profile.weight,
    firstPosition: profile.position,
    secondPosition: profile.position2,
    biography: profile.biography,
  } as IOtherUser;

  yield put(fillOtherUserData(otherUser));
  yield put(fillTopBattingValues(battingTopValues));
  yield put(fillTopPitchingValues(pitchingTopValues));
  yield put(setLoadOtherProfileStatus(false));
}
export function* watchFetchOtherUserData() {
  yield takeEvery(fetchOtherUserDataAction, fetchOtherUserDataSaga);
}

function* updateOtherUserFavoriteStatusSaga(
  action: PayloadAction<IUpdateOtherUsersFavoriteStatusProps>,
): Generator<StrictEffect, void, void> {
  const toastMessage = action.payload.isFavorite
    ? 'This profile added to favorite list successfully'
    : 'This profile removed from favorite list successfully';
  yield put(showToast({ title: 'Success', message: toastMessage }));
  yield put(setFavoriteStatus(action.payload.isFavorite));
  yield call(updateNetworkFavoriteStatus, action.payload);
}
export function* watchUpdateOtherUserFavoriteStatus() {
  yield takeEvery(
    updateOtherUserFavoriteStatusAction,
    updateOtherUserFavoriteStatusSaga,
  );
}

function* fetchOtherUserPitchingSummarySaga(
  action: PayloadAction<IUploadProfileData>,
): Generator<StrictEffect, void, any> {
  const pitchingSummary = yield call(
    UserApi.fetchPitchingSummary,
    action.payload,
  );
  const averageValues = pitchingSummary.average_values.map(
    (value: any) =>
      ({
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchType: value.pitch_type,
        pitchMovement: value.pitch_movement,
      } as IPitchingTopValue),
  );
  const topValues = pitchingSummary.average_values.map(
    (value: any) =>
      ({
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchType: value.pitch_type,
        pitchMovement: value.pitch_movement,
      } as IPitchingTopValue),
  );
  yield put(
    fillPitchingSummary({
      averageValues,
      topValues,
    } as IPitchingSummary),
  );
}
export function* watchFetchOtherUserPitchingSummary() {
  yield takeEvery(
    fetchPitchingSummaryAction,
    fetchOtherUserPitchingSummarySaga,
  );
}

function* fetchOtherUserPitchingGraphSaga(
  action: PayloadAction<IUploadGraph>,
): Generator<StrictEffect, void, any> {
  const pitchingGraph = yield call(UserApi.fetchPitchingGraph, action.payload);
  yield put(fillPitchingGraph(pitchingGraph.graph_rows as number[]));
}
export function* watchFetchOtherUserPitchingGraph() {
  yield takeEvery(fetchPitchingGraphAction, fetchOtherUserPitchingGraphSaga);
}

function* fetchOtherUserPitchingLogsSaga(
  action: PayloadAction<IUploadLogs>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadOtherUserLogsStatus(true));
  const data = yield call(UserApi.fetchPitchingLogs, action.payload);
  const pitchingLogs = data.pitching_log.map(
    (value: any) =>
      ({
        batterId: value.batter_datraks_id,
        batterHandedness: value.batter_handedness,
        batterName: value.batter_name,
        heightAtPlate: value.height_at_plate,
        horizontalBreak: value.horizontal_break,
        verticalBreak: value.vertical_break,
        pitchCall: value.pitch_call,
        pitchType: value.pitch_type,
        releaseHeight: value.release_height,
        releaseSide: value.release_side,
        spinAxis: value.spin_axis,
        spinRate: value.spin_rate,
        ...value,
      } as IPitchingLog),
  );
  yield put(fillPitchingLogs({ pitchingLogs, totalCount: data.total_count }));
}
export function* watchFetchOtherUserPitchingLogs() {
  yield takeEvery(fetchPitchingLogsAction, fetchOtherUserPitchingLogsSaga);
}

function* fetchOtherUserBattingSummarySaga(
  action: PayloadAction<IUploadProfileData>,
): Generator<StrictEffect, void, any> {
  const battingSummary = yield call(
    UserApi.fetchBattingSummary,
    action.payload,
  );
  const averageValues = battingSummary.average_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        exitVelocity: value.exit_velocity,
        distance: value.distance,
        launchAngle: value.launch_angle,
      } as IBattingTopValue),
  );
  const topValues = battingSummary.average_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        exitVelocity: value.exit_velocity,
        distance: value.distance,
        launchAngle: value.launch_angle,
      } as IBattingTopValue),
  );
  yield put(
    fillBattingSummary({
      averageValues,
      topValues,
    } as IBattingSummary),
  );
}
export function* watchFetchOtherUserBattingSummary() {
  yield takeEvery(fetchBattingSummaryAction, fetchOtherUserBattingSummarySaga);
}

function* fetchOtherUserBattingGraphSaga(
  action: PayloadAction<IUploadGraph>,
): Generator<StrictEffect, void, any> {
  const battingGraph = yield call(UserApi.fetchBattingGraph, action.payload);
  yield put(fillBattingGraph(battingGraph.graph_rows as number[]));
}
export function* watchFetchOtherUserBattingGraph() {
  yield takeEvery(fetchBattingGraphAction, fetchOtherUserBattingGraphSaga);
}

function* fetchOtherUserBattingLogsSaga(
  action: PayloadAction<IUploadLogs>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadOtherUserLogsStatus(true));
  const data = yield call(UserApi.fetchBattingLogs, action.payload);
  const battingLogs = data.batting_log.map(
    (value: any) =>
      ({
        pitcherId: value.pitcher_datraks_id,
        pitcherHandedness: value.pitcher_handedness,
        pitcherName: value.pitcher_name,
        exitVelocity: value.exit_velocity,
        hangTime: value.hang_time,
        hitSpinRate: value.hit_spin_rate,
        launchAngle: value.launch_angle,
        pitchCall: value.pitch_call,
        pitchType: value.pitch_type,
        ...value,
      } as IBattingLog),
  );
  yield put(fillBattingLogs({ battingLogs, totalCount: data.total_count }));
}
export function* watchFetchOtherUserBattingLogs() {
  yield takeEvery(fetchBattingLogsAction, fetchOtherUserBattingLogsSaga);
}
