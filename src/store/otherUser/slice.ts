import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { IBattingLog, IBattingLogsData, IBattingSummary, IBattingTopValue, IPitchingLog, IPitchingLogsData, IPitchingSummary, IPitchingTopValue } from '../commonTypes';
import { IOtherUser } from './types';

const initialState = {
  isLoadOtherProfile: false,
  isLoadLogs: false,
  selectedUserId: -1,
  otherUserData: {
    id: -1,
    avatar: '',
    firstName: '',
    lastName: '',
    firstPosition: '',
    secondPosition: '',
    school: { id: 0, name: '' },
    schoolYear: '',
    biography: '',
    teams: [],
    facilities: [],
    isFavorite: false,
    weight: 0,
    feet: 0,
    inches: 0,
    age: 0,
    batsHand: '',
    throwsHand: '',
  } as IOtherUser,
  battingTopValues: [] as IBattingTopValue[],
  pitchingTopValues: [] as IPitchingTopValue[],
  pitchingSummary: {
    averageValues: [],
    topValues: [],
  } as IPitchingSummary,
  pitchingGraph: [] as number[],
  pitchingLogs: [] as IPitchingLog[],
  pitchingLogsCount: 0,
  battingSummary: {
    averageValues: [],
    topValues: [],
  } as IBattingSummary,
  battingGraph: [] as number[],
  battingLogs: [] as IBattingLog[],
  battingLogsCount: 0,
};

const otherUserSlice = createSlice({
  name: 'otherUser',
  initialState,
  reducers: {
    setSelectedOtherUserId: (state, action: PayloadAction<number>) => {
      state.selectedUserId = action.payload;
    },
    setLoadOtherProfileStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadOtherProfile = action.payload;
    },
    setLoadOtherUserLogsStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadLogs = action.payload;
    },
    setFavoriteStatus: (state, action: PayloadAction<boolean>) => {
      state.otherUserData.isFavorite = action.payload;
    },
    fillOtherUserData: (state, action: PayloadAction<IOtherUser>) => {
      state.otherUserData = action.payload;
    },
    fillTopBattingValues: (
      state,
      action: PayloadAction<IBattingTopValue[]>,
    ) => {
      state.battingTopValues = action.payload;
    },
    fillTopPitchingValues: (
      state,
      action: PayloadAction<IPitchingTopValue[]>,
    ) => {
      state.pitchingTopValues = action.payload;
    },
    fillPitchingSummary: (state, action: PayloadAction<IPitchingSummary>) => {
      state.pitchingSummary = action.payload;
    },
    fillPitchingGraph: (state, action: PayloadAction<number[]>) => {
      state.pitchingGraph = action.payload;
    },
    fillPitchingLogs: (state, action: PayloadAction<IPitchingLogsData>) => {
      state.pitchingLogs = action.payload.pitchingLogs;
      state.pitchingLogsCount = action.payload.totalCount;
      state.isLoadLogs = false;
    },
    fillBattingSummary: (state, action: PayloadAction<IBattingSummary>) => {
      state.battingSummary = action.payload;
    },
    fillBattingGraph: (state, action: PayloadAction<number[]>) => {
      state.battingGraph = action.payload;
    },
    fillBattingLogs: (state, action: PayloadAction<IBattingLogsData>) => {
      state.battingLogs = action.payload.battingLogs;
      state.battingLogsCount = action.payload.totalCount;
      state.isLoadLogs = false;
    },
  },
});

export const otherUserReducer = otherUserSlice.reducer;
export const {
  setSelectedOtherUserId,
  setLoadOtherProfileStatus,
  setLoadOtherUserLogsStatus,
  fillOtherUserData,
  fillTopPitchingValues,
  fillTopBattingValues,
  setFavoriteStatus,
  fillPitchingSummary,
  fillPitchingGraph,
  fillPitchingLogs,
  fillBattingSummary,
  fillBattingGraph,
  fillBattingLogs,
} = otherUserSlice.actions;

export const selectSelectedOtherUserId = createSelector(
  (state: RootState) => state.otherUser,
  data => data.selectedUserId,
);

export const selectOtherProfileLoadStatus = createSelector(
  (state: RootState) => state.otherUser,
  data => data.isLoadOtherProfile,
);
export const selectOtherUserLogsLoadStatus = createSelector(
  (state: RootState) => state.otherUser,
  data => data.isLoadLogs,
);

export const selectOtherUserData = createSelector(
  (state: RootState) => state.otherUser,
  data => data.otherUserData,
);

export const selectOtherUserBattingValues = createSelector(
  (state: RootState) => state.otherUser,
  data => data.battingTopValues,
);
export const selectOtherUserPitchingValues = createSelector(
  (state: RootState) => state.otherUser,
  data => data.pitchingTopValues,
);

export const selectOtherUserPitchingSummary = createSelector(
  (state: RootState) => state.otherUser,
  data => data.pitchingSummary,
);
export const selectOtherUserPitchingGraph = createSelector(
  (state: RootState) => state.otherUser,
  data => data.pitchingGraph,
);
export const selectOtherUserPitchingLogs = createSelector(
  (state: RootState) => state.otherUser,
  data => data.pitchingLogs,
);
export const selectOtherUserPitchingLogsCount = createSelector(
  (state: RootState) => state.otherUser,
  data => data.pitchingLogsCount,
);

export const selectOtherUserBattingSummary = createSelector(
  (state: RootState) => state.otherUser,
  data => data.battingSummary,
);
export const selectOtherUserBattingGraph = createSelector(
  (state: RootState) => state.otherUser,
  data => data.battingGraph,
);
export const selectOtherUserBattingLogs = createSelector(
  (state: RootState) => state.otherUser,
  data => data.battingLogs,
);
export const selectOtherUserBattingLogsCount = createSelector(
  (state: RootState) => state.otherUser,
  data => data.battingLogsCount,
);
