import { createAction } from '@reduxjs/toolkit';
import { ISearchable } from '../commonTypes';

export const fetchSchoolsListAction =
  createAction<ISearchable>('schools/FETCH_LIST');
