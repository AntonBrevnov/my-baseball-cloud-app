import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import { fillSchoolsList, setLoadSchoolsStatus } from './slice';
import * as SchoolsApi from '../../api/SchoolApi';
import { ISchool, ISearchable } from '../commonTypes';
import { fetchSchoolsListAction } from './actions';

function* fetchSchoolsListSaga(
  action: PayloadAction<ISearchable>,
): Generator<StrictEffect, void, ISchool[]> {
  yield put(setLoadSchoolsStatus(true));
  const schools = yield call(SchoolsApi.fetchSchoolsList, action.payload);
  yield put(fillSchoolsList(schools));
}
export function* watchFetchSchoolsList() {
  yield takeEvery(fetchSchoolsListAction, fetchSchoolsListSaga);
}
