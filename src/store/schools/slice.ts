import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { ISchool } from '../commonTypes';

const initialState = {
  isLoadSchoolsList: false,
  schoolsList: [] as ISchool[],
};

const schoolsSlice = createSlice({
  name: 'schools',
  initialState,
  reducers: {
    setLoadSchoolsStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadSchoolsList = action.payload;
    },
    fillSchoolsList: (state, action: PayloadAction<ISchool[]>) => {
      state.schoolsList = action.payload;
      state.isLoadSchoolsList = false;
    },
  },
});

export const schoolsReducer = schoolsSlice.reducer;
export const { setLoadSchoolsStatus, fillSchoolsList } = schoolsSlice.actions;

export const selectSchoolsLoadStatus = createSelector(
  (state: RootState) => state.schools,
  data => data.isLoadSchoolsList,
);
export const selectSchoolsList = createSelector(
  (state: RootState) => state.schools,
  data => data.schoolsList,
);
