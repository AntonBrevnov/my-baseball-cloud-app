import { IClientData, ISchool, ITeam } from '../commonTypes';

export interface INetworkFilters {
  school: string;
  team: string;
  position: string;
  age: string;
  isFavorite: string;
  profilesCount: number;
}

export interface IUploadUsersProps extends IClientData {
  playerName?: string;
  school?: string;
  team?: string;
  position: string;
  age?: number;
  isFavorite: boolean;
  profilesCount: number;
  offset: number;
}

export interface IUpdateNetworkFavoriteStatusProps extends IClientData {
  id: number;
  isFavorite: boolean;
}

export interface ITableUserData {
  id: number;
  name: string;
  school: ISchool;
  teams: ITeam[];
  age: string;
  isFavorite: boolean;
}

export interface IFillNetworkUsersArrayProps {
  profiles: ITableUserData[];
  totalProfilesCount: number;
}
