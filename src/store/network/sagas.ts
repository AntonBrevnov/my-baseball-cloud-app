import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import {
  fillProfilesArray,
  setNetworkLoadingStatus,
  updateFavoriteStatus,
} from './slice';
import {
  IFillNetworkUsersArrayProps,
  ITableUserData,
  IUpdateNetworkFavoriteStatusProps,
  IUploadUsersProps,
} from './types';
import * as NetworkApi from '../../api/NetworkApi';
import { fetchNetworkUsersAction, updateFavoriteStatusAction } from './actions';
import { showToast } from '../toast/slice';

function* fetchNetworkUsersSaga(
  action: PayloadAction<IUploadUsersProps>,
): Generator<StrictEffect, void, any> {
  yield put(setNetworkLoadingStatus(true));
  const profiles = yield call(NetworkApi.getNetworkUsers, action.payload);
  const passProfilesList = [] as ITableUserData[];
  profiles.profiles.forEach((item: any) =>
    passProfilesList.push({
      id: item.id,
      name: `${item.first_name  } ${  item.last_name}`,
      age: item.age,
      school: { ...item.school },
      teams: [...item.teams],
      isFavorite: item.favorite,
    }),
  );
  const passData = {
    profiles: passProfilesList,
    totalProfilesCount: profiles.total_count,
  } as IFillNetworkUsersArrayProps;
  yield put(fillProfilesArray(passData));
}

export function* watchFetchNetworkUsers() {
  yield takeEvery(fetchNetworkUsersAction, fetchNetworkUsersSaga);
}

function* updateNetworkFavoriteStatusSaga(
  action: PayloadAction<IUpdateNetworkFavoriteStatusProps>,
): Generator<StrictEffect, void, void> {
  const toastMessage = action.payload.isFavorite
    ? 'This profile added to favorite list successfully'
    : 'This profile removed from favorite list successfully';
  yield put(showToast({ title: 'Success', message: toastMessage }));
  yield put(updateFavoriteStatus(action.payload));
  yield call(NetworkApi.updateNetworkFavoriteStatus, action.payload);
}
export function* watchUpdateNetworkFavoriteStatus() {
  yield takeEvery(updateFavoriteStatusAction, updateNetworkFavoriteStatusSaga);
}
