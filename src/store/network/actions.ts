import { createAction } from '@reduxjs/toolkit';
import { IUpdateNetworkFavoriteStatusProps, IUploadUsersProps } from './types';

export const fetchNetworkUsersAction =
  createAction<IUploadUsersProps>('network/FETCH_ALL');
export const updateFavoriteStatusAction =
  createAction<IUpdateNetworkFavoriteStatusProps>('leaders/UPDATE_FAVORITE');
