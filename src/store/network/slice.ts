import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import {
  IFillNetworkUsersArrayProps,
  ITableUserData,
  IUpdateNetworkFavoriteStatusProps,
} from './types';

const initialState = {
  isLoadingStatus: false,
  totalCount: 0,
  profiles: [] as ITableUserData[],
};

const networkSlice = createSlice({
  name: 'network',
  initialState,
  reducers: {
    setNetworkLoadingStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadingStatus = action.payload;
    },
    fillProfilesArray: (
      state,
      action: PayloadAction<IFillNetworkUsersArrayProps>,
    ) => {
      state.profiles = action.payload.profiles;
      state.totalCount = action.payload.totalProfilesCount;
      state.isLoadingStatus = false;
    },
    updateFavoriteStatus: (
      state,
      action: PayloadAction<IUpdateNetworkFavoriteStatusProps>,
    ) => {
      const { id, isFavorite } = action.payload;
      const index = state.profiles.findIndex(value => value.id === id);
      if (index >= 0) {
        state.profiles[index].isFavorite = isFavorite;
      }
    },
  },
});

export const {
  setNetworkLoadingStatus,
  fillProfilesArray,
  updateFavoriteStatus,
} = networkSlice.actions;
export const networkReducer = networkSlice.reducer;

export const selectLoadNetworkUsersStatus = createSelector(
  (state: RootState) => state.network,
  data => data.isLoadingStatus,
);
export const selectNetworkUsers = createSelector(
  (state: RootState) => state.network,
  data => data.profiles,
);
export const selectTotalCount = createSelector(
  (state: RootState) => state.network,
  data => data.totalCount,
);
