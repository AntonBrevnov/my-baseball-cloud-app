import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { IFacility } from '../commonTypes';

const initialState = {
  isLoadFacilitiesList: false,
  facilitiesList: [] as IFacility[],
};

const facilitiesSlice = createSlice({
  name: 'schools',
  initialState,
  reducers: {
    setLoadFacilitiesStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadFacilitiesList = action.payload;
    },
    fillFacilitiesList: (state, action: PayloadAction<IFacility[]>) => {
      state.facilitiesList = action.payload;
      state.isLoadFacilitiesList = false;
    },
  },
});

export const facilitiesReducer = facilitiesSlice.reducer;
export const { setLoadFacilitiesStatus, fillFacilitiesList } =
  facilitiesSlice.actions;

export const selectFacilitiesLoadStatus = createSelector(
  (state: RootState) => state.facilities,
  data => data.isLoadFacilitiesList,
);
export const selectFacilitiesList = createSelector(
  (state: RootState) => state.facilities,
  data => data.facilitiesList,
);
