import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import { fillFacilitiesList, setLoadFacilitiesStatus } from './slice';
import * as FacilitiesApi from '../../api/FacilityApi';
import { IFacility, ISearchable } from '../commonTypes';
import { fetchFacilitiesListAction } from './actions';

function* fetchFacilitiesListSaga(
  action: PayloadAction<ISearchable>,
): Generator<StrictEffect, void, IFacility[]> {
  yield put(setLoadFacilitiesStatus(true));
  const data = yield call(FacilitiesApi.fetchFacilitiesList, action.payload);
  const facilities = data.map((value: any) => ({
    id: value.id,
    email: value.email,
    name: value.u_name,
  }) as IFacility);
  yield put(fillFacilitiesList(facilities));
}
export function* watchFetchFacilitiesList() {
  yield takeEvery(fetchFacilitiesListAction, fetchFacilitiesListSaga);
}
