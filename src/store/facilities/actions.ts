import { createAction } from '@reduxjs/toolkit';
import { ISearchable } from '../commonTypes';

export const fetchFacilitiesListAction = createAction<ISearchable>(
  'facilities/FETCH_LIST',
);
