export interface ISchool {
  id: number;
  name: string;
}

export interface ITeam {
  id: number;
  name: string;
}

export interface IFacility {
  id: number;
  email: string;
  name: string;
}

export interface IClientData {
  accessToken: string;
  client: string;
  uid: string;
}

export interface IUploadProfileData extends IClientData {
  profileId: number;
}

export interface IBattingTopValue {
  pitchType: string;
  distance: number;
  launchAngle: number;
  exitVelocity: number;
}

export interface IPitchingTopValue {
  pitchType: string;
  velocity: number;
  spinRate: number;
  pitchMovement: number;
}


export interface IUploadGraph extends IClientData {
  profileId: number;
  pitchType: string;
}
export interface IUploadLogs extends IClientData {
  profileId: number;
  pitchType: string;
  name: string;
  count: number;
  offset: number;
}

export interface IPitchingSummary {
  averageValues: IPitchingTopValue[];
  topValues: IPitchingTopValue[];
}
export interface IPitchingLog extends IPitchingTopValue {
  batterId: number;
  batterHandedness: string;
  batterName: string;
  date: string;
  extension: number;
  heightAtPlate: string;
  horizontalBreak: string;
  verticalBreak: string;
  pitchCall: string;
  releaseHeight: number;
  releaseSide: number;
  spinAxis: number;
  tilt: number;
}
export interface IPitchingLogsData {
  pitchingLogs: IPitchingLog[];
  totalCount: number;
}

export interface IBattingSummary {
  averageValues: IBattingTopValue[];
  topValues: IBattingTopValue[];
}
export interface IBattingLog extends IBattingTopValue {
  date: string;
  pitcherId: number;
  pitcherName: string;
  pitcherHandedness: string;
  pitchCall: string;
  direction: string;
  hitSpinRate: number;
  hangTime: number;
}
export interface IBattingLogsData {
  battingLogs: IBattingLog[];
  totalCount: number;
}

export interface ISearchable extends IClientData {
  searchInputValue: string;
}