export interface IToastData {
  title: string;
  message: string;
}
