import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { IToastData } from './types';

const initialState = {
  isVisible: false,
  title: 'title',
  message: 'message',
};

const toastSlice = createSlice({
  name: 'toast',
  initialState,
  reducers: {
    showToast: (state, action: PayloadAction<IToastData>) => {
      state.isVisible = true;
      state.title = action.payload.title;
      state.message = action.payload.message;
    },
    hideToast: state => {
      state.isVisible = false;
    },
  },
});

export const toastReducer = toastSlice.reducer;
export const { showToast, hideToast } = toastSlice.actions;

export const selectToastVisibleState = createSelector(
  (state: RootState) => state.toast,
  data => data.isVisible,
);
export const selectToastData = createSelector(
  (state: RootState) => state.toast,
  data => ({ title: data.title, message: data.message } as IToastData),
);
