import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import {
  fetchAllBattingLeadersAction,
  fetchAllPitchingLeadersAction,
  updateFavoriteStatusAction,
} from './actions';
import {
  fillBattingLeaders,
  fillPitchingLeaders,
  setUploadLeadersStatus,
  updateFavoriteStatus,
} from './slice';
import {
  IBattingLeader,
  IPitchingLeader,
  IUpdateFavoriteStatusProps,
  IUploadLeadersProps,
} from './types';
import * as LeadersApi from '../../api/LeadersApi';
import { showToast } from '../toast/slice';

function* fetchAllBattingLeadersSaga(
  action: PayloadAction<IUploadLeadersProps>,
): Generator<StrictEffect, void, any> {
  yield put(setUploadLeadersStatus(true));
  const response = yield call(LeadersApi.getAllBattingLeaders, action.payload);
  const leadersList = [] as IBattingLeader[];
  for (let i = 0; i < response.length; i++) {
    leadersList.push({
      id: response[i].batter_datraks_id,
      name: response[i].batter_name,
      age: response[i].age,
      distance: response[i].distance,
      exitVelocity: response[i].exit_velocity,
      launchAngle: response[i].launch_angle,
      school: response[i].school,
      teams: response[i].teams,
      favorite: response[i].favorite,
    } as IBattingLeader);
  }
  yield put(fillBattingLeaders([...leadersList]));
}
export function* watchFetchAllBattingLeaders() {
  yield takeEvery(fetchAllBattingLeadersAction, fetchAllBattingLeadersSaga);
}

function* fetchAllPitchingLeadersSaga(
  action: PayloadAction<IUploadLeadersProps>,
): Generator<StrictEffect, void, any> {
  yield put(setUploadLeadersStatus(true));
  const response = yield call(LeadersApi.getAllPitchingLeaders, action.payload);
  const leadersList = [] as IPitchingLeader[];
  for (let i = 0; i < response.length; i++) {
    leadersList.push({
      id: response[i].pitcher_datraks_id,
      name: response[i].pitcher_name,
      age: response[i].age,
      pitchType: response[i].pitch_type,
      velocity: response[i].velocity,
      spinRate: response[i].spin_rate,
      school: response[i].school,
      teams: response[i].teams,
      favorite: response[i].favorite,
    } as IPitchingLeader);
  }
  yield put(fillPitchingLeaders([...leadersList]));
}
export function* watchFetchAllPitchingLeaders() {
  yield takeEvery(fetchAllPitchingLeadersAction, fetchAllPitchingLeadersSaga);
}

function* updateFavoriteStatusSaga(
  action: PayloadAction<IUpdateFavoriteStatusProps>,
): Generator<StrictEffect, void, void> {
  const toastMessage = action.payload.isFavorite
    ? 'This profile added to favorite list successfully'
    : 'This profile removed from favorite list successfully';
  yield put(showToast({ title: 'Success', message: toastMessage }));
  yield put(updateFavoriteStatus(action.payload));
  yield call(LeadersApi.updateFavoriteStatus, action.payload);
}
export function* watchUpdateFavoriteStatus() {
  yield takeEvery(updateFavoriteStatusAction, updateFavoriteStatusSaga);
}
