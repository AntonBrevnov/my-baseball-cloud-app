import { createAction } from '@reduxjs/toolkit';
import { IUpdateFavoriteStatusProps, IUploadLeadersProps } from './types';

export const fetchAllBattingLeadersAction = createAction<IUploadLeadersProps>(
  'leaders/FETCH_ALL_BATTING',
);
export const fetchAllPitchingLeadersAction = createAction<IUploadLeadersProps>(
  'leaders/FETCH_ALL_PITCHING',
);
export const updateFavoriteStatusAction =
  createAction<IUpdateFavoriteStatusProps>('leaders/UPDATE_FAVORITE');
