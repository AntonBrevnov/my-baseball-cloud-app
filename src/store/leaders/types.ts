import { IClientData, ISchool, ITeam } from "../commonTypes";

export interface ILeaderboardFirstFilters {
  date: string;
  school: string;
  team: string;
  position: string;
  age: number;
  isFavorite: string;
}

export interface ILeaderboardSecondFilters {
  sortFilter: string;
  leadersType: string;
}

export interface IUploadLeadersProps extends IClientData {
  type: string;
  date: string;
  school?: string;
  team?: string;
  position: string;
  age?: number;
  isFavorite: number;
}

export interface IUpdateFavoriteStatusProps extends IClientData {
  id: number;
  playerType: string;
  isFavorite: boolean;
}

export interface IBattingLeader {
  id: number;
  name: string;
  age: number;
  school: ISchool;
  teams: ITeam[];
  distance: number;
  exitVelocity: number;
  launchAngle: number;
  favorite: boolean;
}

export interface IPitchingLeader {
  id: number;
  name: string;
  age: number;
  school: ISchool;
  teams: ITeam[];
  pitchType: string;
  velocity: number;
  spinRate: number;
  favorite: boolean;
}
