import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import {
  IBattingLeader,
  IPitchingLeader,
  IUpdateFavoriteStatusProps,
} from './types';

const initialState = {
  isUploadAllLeaders: false,
  battingLeaders: [] as IBattingLeader[],
  pitchingLeaders: [] as IPitchingLeader[],
};

const leaderboardSlice = createSlice({
  name: 'leaders',
  initialState,
  reducers: {
    setUploadLeadersStatus: (state, action: PayloadAction<boolean>) => {
      state.isUploadAllLeaders = action.payload;
    },
    fillBattingLeaders: (state, action: PayloadAction<IBattingLeader[]>) => {
      state.battingLeaders = action.payload;
      state.isUploadAllLeaders = false;
    },
    fillPitchingLeaders: (state, action: PayloadAction<IPitchingLeader[]>) => {
      state.pitchingLeaders = action.payload;
      state.isUploadAllLeaders = false;
    },
    updateFavoriteStatus: (
      state,
      action: PayloadAction<IUpdateFavoriteStatusProps>,
    ) => {
      const { id, playerType, isFavorite } = action.payload;
      let index = -1;
      // eslint-disable-next-line default-case
      switch (playerType) {
        case 'batting':
          index = state.battingLeaders.findIndex(value => value.id === id);
          if (index !== -1) {
            state.battingLeaders[index].favorite = isFavorite;
          }
          break;
        case 'pitching':
          index = state.pitchingLeaders.findIndex(value => value.id === id);
          if (index !== -1) {
            state.pitchingLeaders[index].favorite = isFavorite;
          }
          break;
      }
    },
  },
});

export const {
  setUploadLeadersStatus,
  fillBattingLeaders,
  fillPitchingLeaders,
  updateFavoriteStatus,
} = leaderboardSlice.actions;

export const leadersReducer = leaderboardSlice.reducer;

export const selectLoadLeadersStatus = createSelector(
  (state: RootState) => state.leaders,
  data => data.isUploadAllLeaders,
);
export const selectBattingLeaders = createSelector(
  (state: RootState) => state.leaders,
  data => data.battingLeaders,
);
export const selectPitchingLeaders = createSelector(
  (state: RootState) => state.leaders,
  data => data.pitchingLeaders,
);
