import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';
import {
  persistStore,
  persistReducer,
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';
import { userReducer } from './user/slice';
import { leadersReducer } from './leaders/slice';
import { networkReducer } from './network/slice';
import { otherUserReducer } from './otherUser/slice';
import { comparisonReducer } from './comparison/slice';
import { toastReducer } from './toast/slice';
import { schoolsReducer } from './schools/slice';
import { watchFetchSchoolsList } from './schools/sagas';
import { teamsReducer } from './teams/slice';
import {
  watchFetchBattingGraph,
  watchFetchBattingLogs,
  watchFetchBattingSummary,
  watchFetchPitchingGraph,
  watchFetchPitchingLogs,
  watchFetchPitchingSummary,
  watchFetchUserData,
  watchSignIn,
  watchSignOut,
  watchSignUp,
  watchUpdateProfile,
  watchUploadAvatar,
} from './user/sagas';
import {
  watchFetchAllBattingLeaders,
  watchFetchAllPitchingLeaders,
  watchUpdateFavoriteStatus,
} from './leaders/sagas';
import {
  watchFetchNetworkUsers,
  watchUpdateNetworkFavoriteStatus,
} from './network/sagas';
import {
  watchFetchOtherUserBattingGraph,
  watchFetchOtherUserBattingLogs,
  watchFetchOtherUserBattingSummary,
  watchFetchOtherUserData,
  watchFetchOtherUserPitchingGraph,
  watchFetchOtherUserPitchingLogs,
  watchFetchOtherUserPitchingSummary,
  watchUpdateOtherUserFavoriteStatus,
} from './otherUser/sagas';
import {
  watchFetchProfileData,
  watchFetchProfileNames,
} from './comparison/sagas';
import { watchFetchTeamsList } from './teams/sagas';
import { facilitiesReducer } from './facilities/slice';
import { watchFetchFacilitiesList } from './facilities/sagas';

const rootReducer = combineReducers({
  user: userReducer,
  otherUser: otherUserReducer,
  leaders: leadersReducer,
  network: networkReducer,
  comparison: comparisonReducer,
  toast: toastReducer,
  schools: schoolsReducer,
  teams: teamsReducer,
  facilities: facilitiesReducer,
});
export type RootState = ReturnType<typeof rootReducer>;

const persistConfig = {
  key: 'localData',
  version: 1,
  storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

function* rootSaga() {
  yield all([
    // user
    watchSignUp(),
    watchSignIn(),
    watchSignOut(),
    watchFetchUserData(),
    watchUploadAvatar(),
    watchUpdateProfile(),
    watchFetchPitchingSummary(),
    watchFetchPitchingGraph(),
    watchFetchPitchingLogs(),
    watchFetchBattingSummary(),
    watchFetchBattingGraph(),
    watchFetchBattingLogs(),
    // leaderboard
    watchFetchAllBattingLeaders(),
    watchFetchAllPitchingLeaders(),
    watchUpdateFavoriteStatus(),
    // network
    watchFetchNetworkUsers(),
    watchUpdateNetworkFavoriteStatus(),
    // other user
    watchFetchOtherUserData(),
    watchUpdateOtherUserFavoriteStatus(),
    watchFetchOtherUserPitchingSummary(),
    watchFetchOtherUserPitchingGraph(),
    watchFetchOtherUserPitchingLogs(),
    watchFetchOtherUserBattingSummary(),
    watchFetchOtherUserBattingGraph(),
    watchFetchOtherUserBattingLogs(),
    // comparison
    watchFetchProfileNames(),
    watchFetchProfileData(),
    // schools list
    watchFetchSchoolsList(),
    // teams list
    watchFetchTeamsList(),
    // facilities list
    watchFetchFacilitiesList(),
  ]);
}

const sagaMiddleware = createSagaMiddleware();
export const store = configureStore({
  reducer: persistedReducer,
  middleware: [
    ...getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
    sagaMiddleware,
  ],
});
sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);
