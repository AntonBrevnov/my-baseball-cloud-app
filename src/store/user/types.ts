import { IFacility, ISchool, ITeam, IClientData } from '../commonTypes';

export interface IUserAuthorizationProps {
  email: string;
  password: string;
}
export interface IUserRegistrationProps extends IUserAuthorizationProps {
  confirmPassword: string;
  role: string;
}

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  avatar: string;
  firstPosition: string;
  secondPosition: string;
  school: ISchool;
  schoolYear: string;
  biography: string;
  teams: ITeam[];
  facilities: IFacility[];
  weight: number;
  feet: number;
  inches: number;
  age: number;
  batsHand: string;
  throwsHand: string;
}

export interface IUserEditorFormData {
  firstName: string;
  lastName: string;
  firstPosition: string;
  secondPosition: string;
  schoolId: string;
  schoolYear: string;
  biography: string;
  teams: string[];
  facilities: string[];
  weight: number;
  feet: number;
  inches: number;
  age: number;
  batsHand: string;
  throwsHand: string;  
}

export interface IUploadAvatarProps extends IClientData {
  fileName: string;
}

export interface IUpdateProfileProps extends IClientData {
  userData: IUser;
  updateCallback: () => void;
}
