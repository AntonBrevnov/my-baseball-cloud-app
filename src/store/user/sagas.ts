import { put, call, StrictEffect, takeEvery } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';

import * as UserApi from '../../api/UserApi';
import {
  fillBattingGraph,
  fillBattingLogs,
  fillBattingSummary,
  fillPitchingGraph,
  fillPitchingLogs,
  fillPitchingSummary,
  fillTopBattingValues,
  fillTopPitchingValues,
  initializeAccessData,
  initializeUserData,
  setInitializeStatus,
  setLoadLogsStatus,
  setLoadProfileStatus,
  setUpdateProfileStatus,
  setUploadAvatarStatus,
  signOut,
  uploadPhoto,
} from './slice';
import {
  fetchUserBattingGraphAction,
  fetchUserBattingLogsAction,
  fetchUserBattingSummaryAction,
  fetchUserDataAction,
  fetchUserPitchingGraphAction,
  fetchUserPitchingLogsAction,
  fetchUserPitchingSummaryAction,
  signInAction,
  signOutAction,
  signUpAction,
  updateProfileAction,
  uploadAvatarAction,
} from './actions';
import {
  IUpdateProfileProps,
  IUploadAvatarProps,
  IUser,
  IUserAuthorizationProps,
  IUserRegistrationProps,
} from './types';
import {
  IBattingLog,
  IBattingSummary,
  IBattingTopValue,
  IClientData,
  IFacility,
  IPitchingLog,
  IPitchingSummary,
  IPitchingTopValue,
  IUploadGraph,
  IUploadLogs,
  IUploadProfileData,
} from '../commonTypes';
import { showToast } from '../toast/slice';

function* signInSaga(
  action: PayloadAction<IUserAuthorizationProps>,
): Generator<StrictEffect, void, IClientData> {
  yield put(setLoadProfileStatus(true));
  const clientData = yield call(UserApi.SignIn, action.payload);
  yield put(initializeAccessData({ ...clientData }));
}
export function* watchSignIn() {
  yield takeEvery(signInAction, signInSaga);
}

function* signUpSaga(
  action: PayloadAction<IUserRegistrationProps>,
): Generator<StrictEffect, void, IClientData> {
  yield put(setLoadProfileStatus(true));
  const clientData = yield call(UserApi.SignUp, action.payload);
  yield put(initializeAccessData({ ...clientData }));
}
export function* watchSignUp() {
  yield takeEvery(signUpAction, signUpSaga);
}

function* signOutSaga(
  action: PayloadAction<IClientData>,
): Generator<StrictEffect, void, void> {
  yield call(UserApi.SignOut, action.payload);
  yield put(signOut());
}
export function* watchSignOut() {
  yield takeEvery(signOutAction, signOutSaga);
}

function* fetchUserDataSaga(
  action: PayloadAction<IClientData>,
): Generator<StrictEffect, void, any> {
  const data = yield call(UserApi.fetchUserData, action.payload);
  const currentProfile = { ...data.data.current_profile };
  const profile = yield call(UserApi.fetchAllUserData, {
    ...action.payload,
    profileId: currentProfile.id,
  });

  const facilities = profile.facilities.map(
    (value: any) =>
      ({
        id: value.id,
        email: value.email,
        name: value.u_name,
      } as IFacility),
  );
  const battingTopValues = profile.batting_top_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        distance: value.distance,
        launchAngle: value.launch_angle,
        exitVelocity: value.exit_velocity,
      } as IBattingTopValue),
  );
  const pitchingTopValues = profile.pitching_top_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchMovement: value.pitch_movement,
      } as IPitchingTopValue),
  );

  if (!profile.first_name || !profile.last_name) {
    yield put(setInitializeStatus(false));
  } else {
    yield put(setInitializeStatus(true));
  }

  const user = {
    id: profile.id,
    firstName: profile.first_name,
    lastName: profile.last_name,
    avatar: profile.avatar,
    age: profile.age,
    batsHand: profile.bats_hand,
    throwsHand: profile.throws_hand,
    teams: profile.teams,
    facilities,
    school: profile.school,
    schoolYear: profile.school_year,
    feet: profile.feet,
    inches: profile.inches,
    weight: profile.weight,
    firstPosition: profile.position,
    secondPosition: profile.position2,
    biography: profile.biography,
  } as IUser;

  yield put(initializeUserData(user));
  yield put(fillTopBattingValues(battingTopValues));
  yield put(fillTopPitchingValues(pitchingTopValues));
}
export function* watchFetchUserData() {
  yield takeEvery(fetchUserDataAction, fetchUserDataSaga);
}

function* uploadAvatarSaga(
  action: PayloadAction<IUploadAvatarProps>,
): Generator<StrictEffect, void, string> {
  yield put(setUploadAvatarStatus(true));
  const imageUrl = yield call(UserApi.UploadAvatar, action.payload);
  yield put(uploadPhoto(imageUrl));
}
export function* watchUploadAvatar() {
  yield takeEvery(uploadAvatarAction, uploadAvatarSaga);
}

function* updateProfileSaga(
  action: PayloadAction<IUpdateProfileProps>,
): Generator<StrictEffect, void, any> {
  yield put(setUpdateProfileStatus(true));
  const data = yield call(UserApi.UpdateProfile, action.payload);
  const user = {
    id: data.id,
    firstName: data.first_name,
    lastName: data.last_name,
    avatar: data.avatar,
    age: data.age,
    batsHand: data.bats_hand,
    throwsHand: data.throws_hand,
    teams: data.teams,
    facilities: data.facilities,
    school: data.school,
    schoolYear: data.school_year,
    feet: data.feet,
    inches: data.inches,
    weight: data.weight,
    firstPosition: data.position,
    secondPosition: data.position2,
    biography: data.biography,
  } as IUser;
  yield put(setInitializeStatus(true)); 
  yield put(initializeUserData({ ...user }));
  yield put(
    showToast({
      title: 'Success',
      message: 'Profile has been updated successfully',
    }),
  );
  action.payload.updateCallback();
}
export function* watchUpdateProfile() {
  yield takeEvery(updateProfileAction, updateProfileSaga);
}
function* fetchPitchingSummarySaga(
  action: PayloadAction<IUploadProfileData>,
): Generator<StrictEffect, void, any> {
  const pitchingSummary = yield call(
    UserApi.fetchPitchingSummary,
    action.payload,
  );
  const averageValues = pitchingSummary.average_values.map(
    (value: any) =>
      ({
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchType: value.pitch_type,
        pitchMovement: value.pitch_movement,
      } as IPitchingTopValue),
  );
  const topValues = pitchingSummary.average_values.map(
    (value: any) =>
      ({
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchType: value.pitch_type,
        pitchMovement: value.pitch_movement,
      } as IPitchingTopValue),
  );
  yield put(
    fillPitchingSummary({
      averageValues,
      topValues,
    } as IPitchingSummary),
  );
}
export function* watchFetchPitchingSummary() {
  yield takeEvery(fetchUserPitchingSummaryAction, fetchPitchingSummarySaga);
}

function* fetchPitchingGraphSaga(
  action: PayloadAction<IUploadGraph>,
): Generator<StrictEffect, void, any> {
  const pitchingGraph = yield call(UserApi.fetchPitchingGraph, action.payload);
  yield put(fillPitchingGraph(pitchingGraph.graph_rows as number[]));
}
export function* watchFetchPitchingGraph() {
  yield takeEvery(fetchUserPitchingGraphAction, fetchPitchingGraphSaga);
}

function* fetchPitchingLogsSaga(
  action: PayloadAction<IUploadLogs>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadLogsStatus(true));
  const data = yield call(UserApi.fetchPitchingLogs, action.payload);
  const pitchingLogs = data.pitching_log.map(
    (value: any) =>
      ({
        batterId: value.batter_datraks_id,
        batterHandedness: value.batter_handedness,
        batterName: value.batter_name,
        heightAtPlate: value.height_at_plate,
        horizontalBreak: value.horizontal_break,
        verticalBreak: value.vertical_break,
        pitchCall: value.pitch_call,
        pitchType: value.pitch_type,
        releaseHeight: value.release_height,
        releaseSide: value.release_side,
        spinAxis: value.spin_axis,
        spinRate: value.spin_rate,
        ...value,
      } as IPitchingLog),
  );
  yield put(fillPitchingLogs({ pitchingLogs, totalCount: data.total_count }));
}
export function* watchFetchPitchingLogs() {
  yield takeEvery(fetchUserPitchingLogsAction, fetchPitchingLogsSaga);
}

function* fetchBattingSummarySaga(
  action: PayloadAction<IUploadProfileData>,
): Generator<StrictEffect, void, any> {
  const battingSummary = yield call(
    UserApi.fetchBattingSummary,
    action.payload,
  );
  const averageValues = battingSummary.average_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        exitVelocity: value.exit_velocity,
        distance: value.distance,
        launchAngle: value.launch_angle,
      } as IBattingTopValue),
  );
  const topValues = battingSummary.average_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        exitVelocity: value.exit_velocity,
        distance: value.distance,
        launchAngle: value.launch_angle,
      } as IBattingTopValue),
  );
  yield put(
    fillBattingSummary({
      averageValues,
      topValues,
    } as IBattingSummary),
  );
}
export function* watchFetchBattingSummary() {
  yield takeEvery(fetchUserBattingSummaryAction, fetchBattingSummarySaga);
}

function* fetchBattingGraphSaga(
  action: PayloadAction<IUploadGraph>,
): Generator<StrictEffect, void, any> {
  const battingGraph = yield call(UserApi.fetchBattingGraph, action.payload);
  yield put(fillBattingGraph(battingGraph.graph_rows as number[]));
}
export function* watchFetchBattingGraph() {
  yield takeEvery(fetchUserBattingGraphAction, fetchBattingGraphSaga);
}

function* fetchBattingLogsSaga(
  action: PayloadAction<IUploadLogs>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadLogsStatus(true));
  const data = yield call(UserApi.fetchBattingLogs, action.payload);
  const battingLogs = data.batting_log.map(
    (value: any) =>
      ({
        pitcherId: value.pitcher_datraks_id,
        pitcherHandedness: value.pitcher_handedness,
        pitcherName: value.pitcher_name,
        exitVelocity: value.exit_velocity,
        hangTime: value.hang_time,
        hitSpinRate: value.hit_spin_rate,
        launchAngle: value.launch_angle,
        pitchCall: value.pitch_call,
        pitchType: value.pitch_type,
        ...value,
      } as IBattingLog),
  );
  yield put(fillBattingLogs({ battingLogs, totalCount: data.total_count }));
}
export function* watchFetchBattingLogs() {
  yield takeEvery(fetchUserBattingLogsAction, fetchBattingLogsSaga);
}
