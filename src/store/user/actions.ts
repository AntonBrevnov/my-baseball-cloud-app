import { createAction } from '@reduxjs/toolkit';
import { IClientData, IUploadGraph, IUploadLogs, IUploadProfileData } from '../commonTypes';
import {
  IUpdateProfileProps,
  IUploadAvatarProps,
  IUserAuthorizationProps,
  IUserRegistrationProps,
} from './types';

export const signInAction =
  createAction<IUserAuthorizationProps>('user/SIGN_IN');
export const signUpAction =
  createAction<IUserRegistrationProps>('user/SIGN_UP');
export const signOutAction = createAction<IClientData>('user/SIGN_OUT');
export const fetchUserDataAction = createAction<IClientData>('user/FETCH_DATA');
export const uploadAvatarAction =
  createAction<IUploadAvatarProps>('user/UPLOAD_IMAGE');
export const updateProfileAction =
  createAction<IUpdateProfileProps>('user/UPDATE');

export const fetchUserPitchingSummaryAction = createAction<IUploadProfileData>(
  'user/FETCH_PITCHING_SUMMARY',
);
export const fetchUserPitchingGraphAction = createAction<IUploadGraph>(
  'user/FETCH_PITCHING_GRAPH',
);
export const fetchUserPitchingLogsAction = createAction<IUploadLogs>(
  'user/FETCH_PITCHING_LOGS',
);

export const fetchUserBattingSummaryAction = createAction<IUploadProfileData>(
  'user/FETCH_BATTING_SUMMARY',
);
export const fetchUserBattingGraphAction = createAction<IUploadGraph>(
  'otheruserUser/FETCH_BATTING_GRAPH',
);
export const fetchUserBattingLogsAction = createAction<IUploadLogs>(
  'user/FETCH_BATTING_LOGS',
);
