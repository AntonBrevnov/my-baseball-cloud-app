import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import {
  IBattingLog,
  IBattingLogsData,
  IBattingSummary,
  IBattingTopValue,
  IClientData,
  IPitchingLog,
  IPitchingLogsData,
  IPitchingSummary,
  IPitchingTopValue,
} from '../commonTypes';
import { IUser } from './types';

const initialState = {
  isInitialized: false,
  isUploadAvatar: false,
  isLoadProfile: false,
  isUpdateProfile: false,
  isLoadLogs: false,
  clientData: {
    accessToken: '',
    client: '',
    uid: '',
  } as IClientData,
  userData: {
    id: -1,
    avatar: '',
    firstName: '',
    lastName: '',
    firstPosition: '',
    secondPosition: '',
    school: { id: 0, name: '' },
    schoolYear: '',
    biography: '',
    teams: [],
    facilities: [],
    weight: 0,
    feet: 0,
    inches: 0,
    age: 0,
    batsHand: '',
    throwsHand: '',
  } as IUser,
  battingTopValues: [] as IBattingTopValue[],
  pitchingTopValues: [] as IPitchingTopValue[],
  pitchingSummary: {
    averageValues: [],
    topValues: [],
  } as IPitchingSummary,
  pitchingGraph: [] as number[],
  pitchingLogs: [] as IPitchingLog[],
  pitchingLogsCount: 0,
  battingSummary: {
    averageValues: [],
    topValues: [],
  } as IBattingSummary,
  battingGraph: [] as number[],
  battingLogs: [] as IBattingLog[],
  battingLogsCount: 0,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setInitializeStatus: (state, action: PayloadAction<boolean>) => {
      state.isInitialized = action.payload;
    },
    initializeAccessData: (state, action: PayloadAction<IClientData>) => {
      state.clientData = action.payload;
    },
    initializeUserData: (state, action: PayloadAction<IUser>) => {
      state.userData = action.payload;
      state.isLoadProfile = false;
      state.isUpdateProfile = false;
    },
    fillTopBattingValues: (
      state,
      action: PayloadAction<IBattingTopValue[]>,
    ) => {
      state.battingTopValues = action.payload;
    },
    fillTopPitchingValues: (
      state,
      action: PayloadAction<IPitchingTopValue[]>,
    ) => {
      state.pitchingTopValues = action.payload;
    },
    fillPitchingSummary: (state, action: PayloadAction<IPitchingSummary>) => {
      state.pitchingSummary = action.payload;
    },
    fillPitchingGraph: (state, action: PayloadAction<number[]>) => {
      state.pitchingGraph = action.payload;
    },
    fillPitchingLogs: (state, action: PayloadAction<IPitchingLogsData>) => {
      state.pitchingLogs = action.payload.pitchingLogs;
      state.pitchingLogsCount = action.payload.totalCount;
      state.isLoadLogs = false;
    },
    fillBattingSummary: (state, action: PayloadAction<IBattingSummary>) => {
      state.battingSummary = action.payload;
    },
    fillBattingGraph: (state, action: PayloadAction<number[]>) => {
      state.battingGraph = action.payload;
    },
    fillBattingLogs: (state, action: PayloadAction<IBattingLogsData>) => {
      state.battingLogs = action.payload.battingLogs;
      state.battingLogsCount = action.payload.totalCount;
      state.isLoadLogs = false;
    },
    setUploadAvatarStatus: (state, action: PayloadAction<boolean>) => {
      state.isUploadAvatar = action.payload;
    },
    setLoadProfileStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadProfile = action.payload;
    },
    setUpdateProfileStatus: (state, action: PayloadAction<boolean>) => {
      state.isUpdateProfile = action.payload;
    },
    setLoadLogsStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadLogs = action.payload;
    },
    uploadPhoto: (state, action: PayloadAction<string>) => {
      state.userData.avatar = action.payload;
      state.isUploadAvatar = false;
    },
    signOut: state => {
      state.clientData = {
        accessToken: '',
        client: '',
        uid: '',
      };
      state.userData = {
        id: -1,
        avatar: '',
        firstName: '',
        lastName: '',
        firstPosition: '',
        secondPosition: '',
        school: { id: 0, name: '' },
        schoolYear: '',
        biography: '',
        teams: [],
        facilities: [],
        weight: 0,
        feet: 0,
        inches: 0,
        age: 0,
        batsHand: '',
        throwsHand: '',
      };
      state.battingSummary.averageValues.splice(
        1,
        state.battingSummary.averageValues.length,
      );
      state.battingSummary.topValues.splice(
        1,
        state.battingSummary.topValues.length,
      );
      state.battingTopValues.splice(1, state.battingTopValues.length);
      state.battingGraph.splice(1, state.battingGraph.length);
      state.battingLogs.splice(1, state.battingLogs.length);
      state.battingLogsCount = 0;
      state.pitchingSummary.averageValues.splice(
        1,
        state.pitchingSummary.averageValues.length,
      );
      state.pitchingSummary.topValues.splice(
        1,
        state.pitchingSummary.topValues.length,
      );
      state.pitchingTopValues.splice(1, state.pitchingTopValues.length);
      state.pitchingGraph.splice(1, state.pitchingGraph.length);
      state.pitchingLogs.splice(1, state.pitchingLogs.length);
      state.pitchingLogsCount = 0;
    },
  },
});

export const userReducer = userSlice.reducer;
export const {
  initializeAccessData,
  initializeUserData,
  fillTopBattingValues,
  fillTopPitchingValues,
  signOut,
  uploadPhoto,
  setInitializeStatus,
  setUploadAvatarStatus,
  setLoadProfileStatus,
  setUpdateProfileStatus,
  setLoadLogsStatus,
  fillBattingGraph,
  fillBattingLogs,
  fillBattingSummary,
  fillPitchingGraph,
  fillPitchingLogs,
  fillPitchingSummary,
} = userSlice.actions;

export const selectUserData = createSelector(
  (state: RootState) => state.user,
  data => data.userData,
);

export const selectTopBattingValues = createSelector(
  (state: RootState) => state.user,
  data => data.battingTopValues,
);
export const selectTopPitchingValues = createSelector(
  (state: RootState) => state.user,
  data => data.pitchingTopValues,
);

export const selectUserPitchingSummary = createSelector(
  (state: RootState) => state.user,
  data => data.pitchingSummary,
);
export const selectUserPitchingGraph = createSelector(
  (state: RootState) => state.user,
  data => data.pitchingGraph,
);
export const selectUserPitchingLogs = createSelector(
  (state: RootState) => state.user,
  data => data.pitchingLogs,
);
export const selectUserPitchingLogsCount = createSelector(
  (state: RootState) => state.user,
  data => data.pitchingLogsCount,
);

export const selectUserBattingSummary = createSelector(
  (state: RootState) => state.user,
  data => data.battingSummary,
);
export const selectUserBattingGraph = createSelector(
  (state: RootState) => state.user,
  data => data.battingGraph,
);
export const selectUserBattingLogs = createSelector(
  (state: RootState) => state.user,
  data => data.battingLogs,
);
export const selectUserBattingLogsCount = createSelector(
  (state: RootState) => state.user,
  data => data.battingLogsCount,
);

export const selectClientData = createSelector(
  (state: RootState) => state.user,
  data => data.clientData,
);

export const selectInitializeStatus = createSelector(
  (state: RootState) => state.user,
  data => data.isInitialized,
);
export const selectAvatarLoadingStatus = createSelector(
  (state: RootState) => state.user,
  data => data.isUploadAvatar,
);
export const selectLoadProfileStatus = createSelector(
  (state: RootState) => state.user,
  data => data.isLoadProfile,
);
export const selectUpdateProfileStatus = createSelector(
  (state: RootState) => state.user,
  data => data.isUpdateProfile,
);
export const selectLoadLogsStatus = createSelector(
  (state: RootState) => state.user,
  data => data.isLoadLogs,
);
