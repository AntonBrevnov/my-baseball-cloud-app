import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { IBattingTopValue, IPitchingTopValue } from '../commonTypes';
import { IComparisonUserData, IProfileNamesListItemData } from './types';

const initialState = {
  isLoadNames: false,
  isLoadProfileData: false,
  profileNames: [] as IProfileNamesListItemData[],
  selectedUserData: {
    avatar: '',
    weight: 0,
    feet: 0,
    inches: 0,
    age: 0,
    battingTopValues: [] as IBattingTopValue[],
    pitchingTopValues: [] as IPitchingTopValue[],
  } as IComparisonUserData,
};

const comparisonSlice = createSlice({
  name: 'comparison',
  initialState,
  reducers: {
    setLoadNamesStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadNames = action.payload;
    },
    setLoadProfileDataStatus: (state, action: PayloadAction<boolean>) => {
      state.isLoadProfileData = action.payload;
    },
    fillProfileNames: (
      state,
      action: PayloadAction<IProfileNamesListItemData[]>,
    ) => {
      state.profileNames = action.payload;
      state.isLoadNames = false;
    },
    setSelectedProfileData: (
      state,
      action: PayloadAction<IComparisonUserData>,
    ) => {
      state.selectedUserData = action.payload;
      state.isLoadProfileData = false;
    },
  },
});

export const comparisonReducer = comparisonSlice.reducer;
export const {
  setLoadNamesStatus,
  setLoadProfileDataStatus,
  fillProfileNames,
  setSelectedProfileData,
} = comparisonSlice.actions;

export const selectLoadNamesStatus = createSelector(
  (state: RootState) => state.comparison,
  data => data.isLoadNames,
);
export const selectLoadProfileDataStatus = createSelector(
  (state: RootState) => state.comparison,
  data => data.isLoadProfileData,
);
export const selectProfileNames = createSelector(
  (state: RootState) => state.comparison,
  data => data.profileNames,
);
export const selectSelectedProfileData = createSelector(
  (state: RootState) => state.comparison,
  data => data.selectedUserData,
);
