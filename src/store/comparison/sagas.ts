import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, StrictEffect, takeEvery } from 'redux-saga/effects';
import {
  fillProfileNames,
  setLoadNamesStatus,
  setLoadProfileDataStatus,
  setSelectedProfileData,
} from './slice';
import {
  IComparisonUserData,
  IProfileNamesListItemData,
  IUploadProfileNames,
} from './types';
import * as ComparisonApi from '../../api/ComparisonApi';
import { fetchProfileDataAction, fetchProfileNamesAction } from './action';
import {
  IBattingTopValue,
  IPitchingTopValue,
  IUploadProfileData,
} from '../commonTypes';

function* fetchProfileNamesSaga(
  action: PayloadAction<IUploadProfileNames>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadNamesStatus(true));
  const data = yield call(ComparisonApi.fetchUserNames, action.payload);
  const namesList = data.map(
    (value: any) =>
      ({
        id: value.id,
        playerName: `${value.first_name} ${value.last_name}`,
      } as IProfileNamesListItemData),
  );
  yield put(fillProfileNames(namesList));
}
export function* watchFetchProfileNames() {
  yield takeEvery(fetchProfileNamesAction, fetchProfileNamesSaga);
}

function* fetchProfileDataSaga(
  action: PayloadAction<IUploadProfileData>,
): Generator<StrictEffect, void, any> {
  yield put(setLoadProfileDataStatus(true));
  const data = yield call(ComparisonApi.fetchProfileData, action.payload);

  const topBattingValues = data.batting_top_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        distance: value.distance,
        launchAngle: value.launch_angle,
        exitVelocity: value.exit_velocity,
      } as IBattingTopValue),
  );

  const topPitchingValues = data.pitching_top_values.map(
    (value: any) =>
      ({
        pitchType: value.pitch_type,
        velocity: value.velocity,
        spinRate: value.spin_rate,
        pitchMovement: 0,
      } as IPitchingTopValue),
  );

  const profile = {
    avatar: data.avatar,
    age: data.age,
    feet: data.feet,
    inches: data.inches,
    weight: data.weight,
    battingTopValues: topBattingValues,
    pitchingTopValues: topPitchingValues,
  } as IComparisonUserData;
  yield put(setSelectedProfileData(profile));
}
export function* watchFetchProfileData() {
  yield takeEvery(fetchProfileDataAction, fetchProfileDataSaga);
}
