import {
  IBattingTopValue,
  IClientData,
  IPitchingTopValue,
} from '../commonTypes';

export interface IUploadProfileNames extends IClientData {
  playerName: string;
  position: string;
}

export interface IProfileNamesListItemData {
  id: number;
  playerName: string;
}

export interface IComparisonUserData {
  avatar: string;
  age: number;
  feet: number;
  inches: number;
  weight: number;
  battingTopValues: IBattingTopValue[];
  pitchingTopValues: IPitchingTopValue[];
}
