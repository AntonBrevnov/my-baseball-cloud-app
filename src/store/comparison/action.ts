import { createAction } from '@reduxjs/toolkit';
import { IUploadProfileData } from '../commonTypes';
import { IUploadProfileNames } from './types';

export const fetchProfileNamesAction = createAction<IUploadProfileNames>(
  'comparison/FETCH_NAMES',
);
export const fetchProfileDataAction = createAction<IUploadProfileData>(
  'comparison/FETCH_DATA',
);
