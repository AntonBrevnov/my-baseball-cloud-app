export default function battingTypeConverter(value: string) {
  switch (value) {
    case 'exit_velocity':
      return 'Exit Velocity';
    case 'carry_distance':
      return 'Carry Distance';
      default:
        return '';
  }
}
