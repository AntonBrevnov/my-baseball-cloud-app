export default function pitchingTypeConverter(value: string) {
  switch (value) {
    case 'pitch_velocity':
      return 'Pitch Velocity';
    case 'spin_rate':
      return 'Spin Rate';
      default:
        return '';
  }
}
