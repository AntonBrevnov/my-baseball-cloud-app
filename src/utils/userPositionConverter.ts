export const userPositionConverter = (position: string) => {
  switch (position) {
    case '':
      return 'All';
    case 'first_base':
      return 'First Base';
    case 'second_base':
      return 'Second Base';
    case 'third_base':
      return 'Third Base';
    case 'catcher':
      return 'Catcher';
    case 'pitcher':
      return 'Pitcher';
    case 'outfield':
      return 'Outfield';
    case 'shortstop':
      return 'Shortstop';
    default:
      return '';
  }
};
export const userPositionReverseConverter = (position: string) => {
  switch (position) {
    case 'First Base':
      return 'first_base';
    case 'Second Base':
      return 'second_base';
    case 'Third Base':
      return 'third_base';
    case 'Catcher':
      return 'catcher';
    case 'Pitcher':
      return 'pitcher';
    case 'Outfield':
      return 'outfield';
    case 'Shortstop':
      return 'shortstop';
    default:
      return '';
  }
};
