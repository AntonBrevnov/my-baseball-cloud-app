type NameOwnerType = {
  name: string;
};

export const namesStringGenerator = (names: NameOwnerType[]): string =>
  names.map(value => value.name).join(', ');
