export default function dateConverter(date: string): string {
  switch (date) {
    case 'last_week':
      return 'Last Week';
    case 'last_month':
      return 'Last Month';
    default:
      return 'All';
  }
}
