interface IPaginationItem {
  index: number;
  isSelected: boolean;
}

export default function paginationItemsGenerator(
  currentPage: number,
  maxPagesCount: number,
): IPaginationItem[] {
  const paginationPanelItems = [];
  for (let i = 0; i <= maxPagesCount; i++) {
    if (i === currentPage - 1 || i === currentPage || i === currentPage + 1) {
      paginationPanelItems.push({
        index: i,
        isSelected: currentPage === i,
      });
    } else if (i === currentPage - 2 || i === currentPage + 2) {
      paginationPanelItems.push({
        index: -1,
        isSelected: false,
      });
    }
  }
  return paginationPanelItems;
}
