const userSchoolYearConverter = (schoolYear: string) => {
  switch (schoolYear) {
    case 'freshman':
      return 'Freshman';
    case 'sophomore':
      return 'Sophomore';
    case 'senior':
      return 'Senior';
    case 'junior':
      return 'Junior';
    case 'none':
      return 'None';
    default:
      return '';
  }
};

export default userSchoolYearConverter;
