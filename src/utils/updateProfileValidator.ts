import { IUserEditorFormData } from '../store/user/types';

export const updateProfileValidator = ({
  firstName,
  lastName,
  age,
  feet,
  weight,
}: IUserEditorFormData) => {
  if (!firstName || !lastName || !age || !feet || !weight) {
    return { message: '* Fill out the required fields' };
  }
  return undefined;
};

export const firstNameValidator = (value: string) => {
  if (!value) {
    return 'First Name Required';
  }
  return undefined;
};
export const lastNameValidator = (value: string) => {
  if (!value) {
    return 'Last Name Required';
  }
  return undefined;
};
export const ageValidator = (value: string) => {
  if (!value) {
    return 'Age Required';
  } if (Number(value) >= 30) {
    return 'Must not be older that 30';
  }
  return undefined;
};
export const feetValidator = (value: string) => {
  if (!value) {
    return 'Feet Required';
  } if (Number(value) > 7) {
    return 'Maximum height is 7';
  } if (Number(value) < 4) {
    return 'Minimal height is 4';
  }
  return undefined;
};
export const weightValidator = (value: string) => {
  if (!value) {
    return 'Weight Required';
  } if (Number(value) > 350) {
    return 'Maximum weight is 350 lbs';
  } if (Number(value) < 50) {
    return 'Minimal weight is 50 lbs';
  }
  return undefined;
};
