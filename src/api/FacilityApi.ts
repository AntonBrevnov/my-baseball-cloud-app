import { httpClient } from '.';
import { ISearchable } from '../store/commonTypes';

export async function fetchFacilitiesList({
  accessToken,
  client,
  uid,
  searchInputValue,
}: ISearchable) {
  return await httpClient
    .post(
      'graphql',
      {
        query: `query Facilities($search:String!) {
        facilities(search: $search) {
          facilities {
            id
            email
            u_name
          }
        }
      }`,
        variables: { search: searchInputValue },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data.data.facilities.facilities);
}
