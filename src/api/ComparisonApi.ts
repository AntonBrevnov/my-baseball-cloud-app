import { httpClient } from '.';
import { IUploadProfileData } from '../store/commonTypes';
import { IUploadProfileNames } from '../store/comparison/types';

export async function fetchUserNames({
  accessToken,
  client,
  uid,
  playerName,
  position,
}: IUploadProfileNames) {
  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
          query ProfileNames($input:FilterProfileNamesInput!) {
            profile_names(input: $input) {
              profile_names {
                id
                position
                first_name
                last_name
                inches
                feet
                weight
                age
              }
            }
          }`,
        variables: {
          input: {
            player_name: playerName,
            position,
          },
        },
      },
      {
        headers: {
          'access-Token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.profile_names.profile_names;
}

export async function fetchProfileData({
  accessToken,
  client,
  uid,
  profileId,
}: IUploadProfileData) {
  const data = await httpClient
    .post(
      'graphql',
      {
        query: `query Profile($id:String!) {
        profile(id: $id) {
          avatar
          feet
          inches
          weight
          age
          batting_top_values {
            pitch_type
            distance
            launch_angle
            exit_velocity
          }
          pitching_top_values {
            velocity
            spin_rate
            pitch_type
          }
        }
      }`,
        variables: { id: String(profileId) },
      },
      {
        headers: {
          'access-token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.profile;
}
