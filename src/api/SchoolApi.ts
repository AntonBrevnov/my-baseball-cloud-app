import { httpClient } from '.';
import { ISearchable } from '../store/commonTypes';

export async function fetchSchoolsList({
  accessToken,
  client,
  uid,
  searchInputValue,
}: ISearchable) {
  return await httpClient
    .post(
      'graphql',
      {
        query: `query Schools($search:String!) {
          schools(search: $search) {
            schools {
              id
              name
            }
          }
        }`,
        variables: { search: searchInputValue },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data.data.schools.schools);
}
