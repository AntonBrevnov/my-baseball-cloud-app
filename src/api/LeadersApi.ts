import { httpClient } from '.';
import {
  IUpdateFavoriteStatusProps,
  IUploadLeadersProps,
} from '../store/leaders/types';

export async function getAllBattingLeaders({
  accessToken,
  client,
  uid,
  age,
  date,
  position,
  school,
  team,
  isFavorite,
  type,
}: IUploadLeadersProps) {
  const queryInput: any = {
    type,
  };
  if (!!age && age > 0) {
    queryInput.age = age;
  }
  if (date) {
    queryInput.date = date;
  }
  if (position) {
    queryInput.position = position;
  }
  if (school) {
    queryInput.school = school;
  }
  if (team) {
    queryInput.team = team;
  }
  if (isFavorite) {
    queryInput.favorite = isFavorite;
  }

  const httpResponse = await httpClient
    .post(
      'graphql',
      {
        query: `
        query LeaderboardBatting($input:FilterLeaderboardInput!) { 
          leaderboard_batting(input: $input) { 
            leaderboard_batting {
              batter_name
              exit_velocity
              launch_angle
              distance
              batter_datraks_id
              age
              school {
                id
                name
              }
              teams {
                id
                name
              }
              favorite
            }
          }
        }`,
        variables: {
          input: queryInput,
        },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return httpResponse.data.leaderboard_batting.leaderboard_batting;
}

export async function getAllPitchingLeaders({
  accessToken,
  client,
  uid,
  age,
  date,
  position,
  school,
  team,
  isFavorite,
  type,
}: IUploadLeadersProps) {
  const queryInput: any = {
    type,
  };
  if (!!age && age > 0) {
    queryInput.age = age;
  }
  if (date) {
    queryInput.date = date;
  }
  if (position) {
    queryInput.position = position;
  }
  if (school) {
    queryInput.school = school;
  }
  if (team) {
    queryInput.team = team;
  }
  if (isFavorite) {
    queryInput.favorite = isFavorite;
  }

  const httpResponse = await httpClient
    .post(
      'graphql',
      {
        query: `
          query LeaderboardPitching($input:FilterLeaderboardInput!) {
            leaderboard_pitching(input: $input) {
              leaderboard_pitching {
                pitcher_datraks_id
                pitcher_name
                pitch_type
                velocity
                spin_rate
                vertical_break
                horizontal_break
                age
                school {
                  id
                  name
                }
                teams {
                  id
                  name
                }
                favorite
              }
            }
          }`,
        variables: {
          input: queryInput,
        },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return httpResponse.data.leaderboard_pitching.leaderboard_pitching;
}

export async function updateFavoriteStatus({
  accessToken,
  client,
  uid,
  id,
  isFavorite,
}: IUpdateFavoriteStatusProps) {
  await httpClient.post(
    'graphql',
    {
      query: `
        mutation UpdateFavoriteProfile($form:UpdateFavoriteProfileInput!) {
          update_favorite_profile(input: $form) {
            favorite
          }
        }`,
      variables: { form: { profile_id: id, favorite: isFavorite } },
    },
    {
      headers: {
        'Access-Token': accessToken,
        Client: client,
        Uid: uid,
      },
    },
  );
}
