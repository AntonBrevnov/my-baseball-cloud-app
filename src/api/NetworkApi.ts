import { httpClient } from '.';
import {
  IUpdateNetworkFavoriteStatusProps,
  IUploadUsersProps,
} from '../store/network/types';

export async function getNetworkUsers({
  accessToken,
  client,
  uid,
  playerName,
  school,
  team,
  position,
  age,
  isFavorite,
  profilesCount,
  offset,
}: IUploadUsersProps) {
  const queryInput: any = {
    profiles_count: profilesCount,
    offset,
  };
  if (playerName) {
    queryInput.player_name = playerName;
  }
  if (!!age && age > 0) {
    queryInput.age = age;
  }
  if (position) {
    queryInput.position = position;
  }
  if (school) {
    queryInput.school = school;
  }
  if (team) {
    queryInput.team = team;
  }
  if (isFavorite) {
    queryInput.favorite = Number(isFavorite);
  }

  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
        query Profiles($input:FilterProfilesInput!) {
          profiles(input: $input) {
            profiles {
              id
              first_name
              last_name
              position
              position2
              school_year
              feet
              inches
              weight
              age
              school {
                id
                name
              }
              teams {
                id
                name
              }
              favorite
            }
            total_count
          }
        }`,
        variables: { input: queryInput },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.profiles;
}

export async function updateNetworkFavoriteStatus({
  accessToken,
  client,
  uid,
  id,
  isFavorite,
}: IUpdateNetworkFavoriteStatusProps) {
  await httpClient.post(
    'graphql',
    {
      query: `
      mutation UpdateFavoriteProfile($form:UpdateFavoriteProfileInput!) {
        update_favorite_profile(input: $form) {
          favorite
        }
      }`,
      variables: { form: { profile_id: id, favorite: isFavorite } },
    },
    {
      headers: {
        'Access-Token': accessToken,
        Client: client,
        Uid: uid,
      },
    },
  );
}
