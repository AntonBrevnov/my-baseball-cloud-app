import { httpClient } from '.';
import { ISearchable } from '../store/commonTypes';

export async function fetchTeamsList({
  accessToken,
  client,
  uid,
  searchInputValue,
}: ISearchable) {
  return await httpClient
    .post(
      'graphql',
      {
        query: `query Teams($search:String!) {
          teams(search: $search) {
            teams {
              id
              name
            }
          }
        }`,
        variables: { search: searchInputValue },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data.data.teams.teams);
}
