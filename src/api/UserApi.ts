import axios from 'axios';
import { httpClient } from '.';
import {
  IClientData,
  IUploadGraph,
  IUploadLogs,
  IUploadProfileData,
} from '../store/commonTypes';
import {
  IUpdateProfileProps,
  IUploadAvatarProps,
  IUserAuthorizationProps,
  IUserRegistrationProps,
} from '../store/user/types';

export async function SignIn({ email, password }: IUserAuthorizationProps) {
  const clientData = httpClient
    .post('auth/sign_in', {
      email,
      password,
    })
    .then(response => ({
      accessToken: response.headers['access-token'],
      client: response.headers.client,
      uid: response.headers.uid,
    }));
  return clientData;
}

export async function SignUp({
  email,
  password,
  confirmPassword,
  role,
}: IUserRegistrationProps) {
  const clientData = httpClient
    .post('auth', {
      email,
      password,
      confirm_password: confirmPassword,
      role,
    })
    .then(response => ({
      accessToken: response.headers['access-token'],
      client: response.headers.client,
      uid: response.headers.uid,
    }));
  return clientData;
}

export async function SignOut({ accessToken, client, uid }: IClientData) {
  await httpClient.delete('auth/sign_out', {
    headers: {
      'access-token': accessToken,
      client,
      uid,
    },
  });
}

export async function fetchUserData({ accessToken, client, uid }: IClientData) {
  const user = httpClient
    .post(
      'graphql',
      {
        query: `{ 
        current_profile () {
          id
          first_name
          last_name
          position
          position2
          avatar
          throws_hand
          bats_hand
          biography
          school_year
          feet
          inches
          weight
          age
          school {
            id
            name
          }  
          teams {
            id
            name
          }  
          facilities {
            id
            email
            u_name
          }
        }
      }`,
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return user;
}

export async function fetchAllUserData({
  accessToken,
  client,
  uid,
  profileId,
}: IUploadProfileData) {
  const data = await httpClient
    .post(
      'graphql',
      {
        query: `query Profile($id:String!) {
          profile(id: $id) {
            id
            first_name
            last_name
            position
            position2
            school_year
            avatar
            throws_hand
            bats_hand
            biography
            feet
            inches
            weight
            age
            recent_events {
              id
              event_type
              event_name
              date
              is_pitcher
              data_rows_count
              recent_avatars {
                id
                first_name
                last_name
                avatar
              }
            }
            winsgspan
            grip_right
            grip_left
            wrist_to_elbow
            broad_jump
            grip_left
            act_score
            gpa_score
            sat_score
            batting_top_values {
              pitch_type
              distance
              launch_angle
              exit_velocity
            }
            pitching_top_values {
              velocity
              spin_rate
              pitch_type
            }
            pitcher_summary {
              velocity
              spin_rate
              horizontal_break
            }
            batter_summary {
              exit_velocity
              distance
              launch_angle
            }
            school {
              id
              name
            }
            teams {
              id
              name
            }
            facilities {
              id
              email
              u_name
            }
            favorite
            events_opened
            paid
          }
        }`,
        variables: { id: String(profileId) },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.profile;
}

export async function UploadAvatar({
  fileName,
  accessToken,
  client,
  uid,
}: IUploadAvatarProps) {
  const avatarUrl = httpClient
    .post(
      's3/signed_url',
      {
        name: fileName,
      },
      {
        headers: {
          'access-Token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => {
      const tempHttpClient = axios.create();
      tempHttpClient.interceptors.request.use(
        config => {
          config.headers = {
            'access-Token': accessToken,
            client,
            uid,
          };
          return config;
        },
        error => Promise.reject(error),
      );
      tempHttpClient.put(response.data.signedUrl);
      return `https://baseballcloud-staging-assets.s3.us-east-2.amazonaws.com/${response.data.fileKey}`;
    });
  return avatarUrl;
}

export async function UpdateProfile({
  userData,
  accessToken,
  client,
  uid,
}: IUpdateProfileProps) {
  const user = httpClient
    .post(
      'graphql',
      {
        query: `mutation UpdateProfile($form:UpdateProfileInput!) {
            update_profile (input:$form) {
              profile {
                id
                first_name
                last_name
                position
                position2
                avatar
                throws_hand
                bats_hand
                biography
                school_year
                feet
                inches
                weight
                age
                recent_events {
                  id
                  event_type
                  event_name
                  date
                  recent_avatars {
                    id
                    first_name
                    last_name
                    avatar
                  }
                }
                school {
                  id
                  name
                }
                teams {
                  id
                  name
                }
                facilities {
                  id
                  email
                  u_name
                }
              }
            }
          }`,
        variables: {
          form: {
            age: userData.age,
            avatar: userData.avatar,
            bats_hand: userData.batsHand.toLowerCase(),
            biography: userData.biography,
            facilities: userData.facilities.map(value => ({
              id: value.id,
              email: value.email,
              u_name: value.name,
            })),
            feet: userData.feet,
            first_name: userData.firstName,
            last_name: userData.lastName,
            id: userData.id,
            position: userData.firstPosition,
            position2: userData.secondPosition,
            school: userData.school,
            school_year: userData.schoolYear,
            teams: userData.teams,
            throws_hand: userData.throwsHand.toLowerCase(),
            weight: userData.weight,
          },
        },
      },
      {
        headers: {
          'access-Token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => response.data.data.update_profile.profile);
  return user;
}

export async function fetchPitchingSummary({
  accessToken,
  client,
  uid,
  profileId,
}: IUploadProfileData) {
  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
          query PitchingSummary($id:ID!) {
            pitching_summary(id: $id) {
              top_values {
                id
                velocity
                spin_rate
                pitch_type
              }
              average_values {
                id
                velocity
                spin_rate
                pitch_type
              }
            }
          }`,
        variables: { id: String(profileId) },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.pitching_summary;
}

export async function fetchPitchingGraph({
  accessToken,
  client,
  uid,
  profileId,
  pitchType,
}: IUploadGraph) {
  const queryInput: any = {
    profile_id: String(profileId),
  };
  if (!!pitchType && pitchType !== 'None') {
    queryInput.pitch_type = pitchType;
  }

  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
        query PitchingGraph($input:FilterGraphInput!) {
          pitching_graph(input: $input) {
            graph_rows
          }
        }`,
        variables: { input: queryInput },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.pitching_graph;
}

export async function fetchPitchingLogs({
  accessToken,
  client,
  uid,
  profileId,
  pitchType,
  name,
  count,
  offset,
}: IUploadLogs) {
  const inputQuery: any = {
    profile_id: profileId,
    count,
    offset,
  };
  if (name) {
    inputQuery.batter_name = name;
  }
  if (!!pitchType && pitchType !== 'None') {
    inputQuery.pitch_type = pitchType;
  }

  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
        query PitchingLog($input:FilterPitchingLogInput!) {
          pitching_log(input: $input) {
            pitching_log {
              date
              pitch_type
              pitch_call
              velocity
              spin_rate
              spin_axis
              tilt
              release_height
              release_side
              extension
              vertical_break
              horizontal_break
              height_at_plate
              batter_name
              batter_datraks_id
              batter_handedness
            }
            total_count
          }
        }`,
        variables: { input: inputQuery },
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.pitching_log;
}

export async function fetchBattingSummary({
  accessToken,
  client,
  uid,
  profileId,
}: IUploadProfileData) {
  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
            query BattingSummary($id:ID!) {
              batting_summary(id: $id) {
                top_values {
                  id
                  distance
                  pitch_type
                  launch_angle
                  exit_velocity
                }
                average_values {
                  id
                  distance
                  pitch_type
                  launch_angle
                  exit_velocity
                }
              }
            }`,
        variables: { id: String(profileId) },
      },
      {
        headers: {
          'access-token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.batting_summary;
}

export async function fetchBattingGraph({
  accessToken,
  client,
  uid,
  profileId,
  pitchType,
}: IUploadGraph) {
  const queryInput: any = {
    profile_id: String(profileId),
  };
  if (!!pitchType && pitchType !== 'None') {
    queryInput.pitch_type = pitchType;
  }

  const data = await httpClient
    .post(
      'graphql',
      {
        query: `
          query BattingGraph($input:FilterGraphInput!) {
            batting_graph(input: $input) {
              graph_rows
            }
          }`,
        variables: { input: queryInput },
      },
      {
        headers: {
          'access-token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.batting_graph;
}

export async function fetchBattingLogs({
  accessToken,
  client,
  uid,
  profileId,
  pitchType,
  name,
  count,
  offset,
}: IUploadLogs) {
  const inputQuery: any = {
    profile_id: String(profileId),
    count,
    offset,
  };
  if (name) {
    inputQuery.pitcher_name = name;
  }
  if (!!pitchType && pitchType !== 'None') {
    inputQuery.pitch_type = pitchType;
  }

  const data = await httpClient
    .post(
      'graphql',
      {
        query: `query BattingLog($input:FilterBattingLogInput!) {
          batting_log(input: $input) {
            batting_log {
              date
              pitcher_name
              pitcher_handedness
              pitch_type
              pitch_call
              exit_velocity
              launch_angle
              direction
              distance
              hit_spin_rate
              hang_time
              pitcher_datraks_id
            }
            total_count
          }
        }`,
        variables: { input: inputQuery },
      },
      {
        headers: {
          'access-token': accessToken,
          client,
          uid,
        },
      },
    )
    .then(response => response.data);
  return data.data.batting_log;
}
